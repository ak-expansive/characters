<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200423141957 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create the command log table.';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE command_log (id UUID NOT NULL, change_name VARCHAR(255) NOT NULL, status VARCHAR(100) NOT NULL, changes json NOT NULL, meta json NOT NULL, global_meta json NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX change_name_idx ON command_log (change_name)');
        $this->addSql('CREATE INDEX status_idx ON command_log (status)');
        $this->addSql('COMMENT ON COLUMN command_log.id IS \'(DC2Type:uuid)\'');

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE command_log');

    }
}
