<?php

namespace Contexts\CQRS\Exceptions;

use Contexts\Validation\Contracts\Arrayable;
use Exception;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class CommandValidationException extends Exception implements Arrayable
{
    protected string $targetClass;

    protected ConstraintViolationListInterface $violations;

    protected function __construct(
        string $class,
        ConstraintViolationListInterface $violations,
        string $message = '',
        int $code = 0,
        ?\Throwable $previous = null
    ) {
        $this->targetClass = $class;
        $this->violations = $violations;

        parent::__construct($message, $code, $previous);
    }

    public static function withViolations(string $class, ConstraintViolationListInterface $violations)
    {
        return new self(
            $class,
            $violations,
            "Instance of $class was not valid."
        );
    }

    public function errorsToArray(): array
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        $errors = [];
        foreach ($this->violations as $violation) {
            $accessor->setValue($errors, $violation->getPropertyPath(), $violation->getMessage());
        }

        return $errors;
    }

    public function toArray(): array
    {
        return [
            'message' => $this->getMessage(),
            'validationErrors' => $this->errorsToArray(),
        ];
    }
}
