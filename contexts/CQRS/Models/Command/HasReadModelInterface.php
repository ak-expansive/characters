<?php

namespace Contexts\CQRS\Models\Command;

interface HasReadModelInterface
{
    public function getReadModelClass(): string;
}
