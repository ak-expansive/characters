<?php

namespace Contexts\CQRS\Models;

use Contexts\CQRS\Tactician\Commands\LoggableCommandInterface;

interface CommandLogInterface
{
    public const SUCCESS = 'success';

    public const FAILURE = 'failure';

    public static function create(LoggableCommandInterface $command, array $globalExtras, string $status): self;
}
