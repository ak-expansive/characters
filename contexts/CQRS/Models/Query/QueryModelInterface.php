<?php

namespace Contexts\CQRS\Models\Query;

use Contexts\ElasticSearch\Documents\DocumentableModelInterface;

interface QueryModelInterface extends DocumentableModelInterface
{
    public static function createFromCommandModel($commandModel): QueryModelInterface;
}
