<?php

namespace Contexts\CQRS\Tactician\Middleware;

use Contexts\CQRS\Models\CommandLogInterface;
use Contexts\CQRS\Repository\CommandLogRepository;
use Contexts\CQRS\Tactician\Commands\LoggableCommandInterface;
use League\Tactician\Middleware;
use Psr\Log\LoggerInterface;
use Throwable;

class CommandLogWrapper implements Middleware
{
    protected LoggerInterface $logger;

    public function __construct(LoggerInterface $logger, CommandLogRepository $repo)
    {
        $this->logger = $logger;
        $this->repo = $repo;
    }

    public function execute($command, callable $next)
    {
        $shouldLog = $command instanceof LoggableCommandInterface;

        $thrownException = null;

        try {
            $returnValue = $next($command);
        } catch (Throwable $e) {
            //We'll throw this exception shortly
            $thrownException = $e;
        }

        if ($shouldLog) {
            try {
                $this->logCommand($command, $thrownException);
            } catch (Throwable $e) {
                /*
                 * Purposely don't rethrow this...
                 *
                 * Silently log it; better to miss a command log than to abort the command altogether
                 * Maybe make this a config setting though, may be useful to always throw in very strict environments
                 * Can always use the $previous in exceptions to allow both exceptions to bubble
                */
                $this->logger->error($e);
            }
        }

        if ($thrownException !== null) {
            // Re-throw the exceptiont that originally came from $next()...
            // We shouldn't interfere with the normal flow of things
            throw $thrownException;
        }

        return $returnValue;
    }

    protected function logCommand(LoggableCommandInterface $command, ?Throwable $e)
    {
        $this->repo->add(
            $command,
            $this->buildGlobalMeta($e),
            $e === null ? CommandLogInterface::SUCCESS : CommandLogInterface::FAILURE
        );
    }

    protected function buildGlobalMeta(?Throwable $e): array
    {
        $global = [
            // Probably inject a 'GlobalMetaProvider' in through the construct for this stuff
            'uri' => '...get uri from request',
            'user-id' => '... get user id',
        ];


        if ($e !== null) {
            // Initially used an interface, but there isn't a standard php one
            // This is likely to work for alot more types of exception
            $global['error'] = method_exists($e, 'toArray') ? $e->toArray() : [
                'stack' => $e->getTraceAsString(),
                'code' => $e->getCode(),
            ];
        }

        return $global;
    }
}
