<?php

namespace Contexts\CQRS\Tactician\Middleware;

use Contexts\CQRS\Tactician\Commands\RequiresTransactionInterface;
use Doctrine\ORM\EntityManagerInterface;
use League\Tactician\Middleware;
use Throwable;

class DoctrineTransactionWrapper implements Middleware
{
    protected EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function execute($command, callable $next)
    {
        if (!$command instanceof RequiresTransactionInterface) {
            return $next($command);
        }

        $connection = $this->em->getConnection();

        try {
            $connection->beginTransaction();
            $returnValue = $next($command);
            $connection->commit();
        } catch (Throwable $e) {
            $connection->rollBack();
            $connection->close();
            throw $e;
        }

        return $returnValue;
    }
}
