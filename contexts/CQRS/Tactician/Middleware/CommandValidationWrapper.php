<?php

namespace Contexts\CQRS\Tactician\Middleware;

use Contexts\CQRS\Exceptions\CommandValidationException;
use Contexts\CQRS\Tactician\Commands\ValidatableCommandInterface;
use League\Tactician\Middleware;
use Symfony\Component\Validator\Validation;

class CommandValidationWrapper implements Middleware
{
    public function execute($command, callable $next)
    {
        if (!$command instanceof ValidatableCommandInterface) {
            return $next($command);
        }

        $validator = Validation::createValidator();

        $constraintViolations = $validator->validate(
            $command->serializeForValidation(),
            $command->getValidationConstraints()
        );

        if (count($constraintViolations) > 0) {
            throw CommandValidationException::withViolations(
                static::class,
                $constraintViolations
            );
        }

        return $next($command);
    }
}
