<?php

namespace Contexts\CQRS\Tactician\Commands;

interface LoggableCommandInterface
{
    public function getChangeset(): array;

    public function getChangeMeta(): array;

    public function getPrettyCommandName(): string;
}
