<?php

namespace Contexts\CQRS\Tactician\Commands;

interface HandlerDefiningInterface
{
    public function getHandlerClass(): string;
}
