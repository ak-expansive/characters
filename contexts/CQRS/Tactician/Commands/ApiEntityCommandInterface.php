<?php

namespace Contexts\CQRS\Tactician\Commands;

use Ramsey\Uuid\Rfc4122\UuidV4;

interface ApiEntityCommandInterface
{
    public function getId(): ?UuidV4;

    public function isVersion(int $version): bool;
}
