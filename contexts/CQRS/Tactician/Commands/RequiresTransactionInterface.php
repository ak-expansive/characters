<?php

namespace Contexts\CQRS\Tactician\Commands;

/**
 * This just defines that a command should be run within a transaction for the
 * Doctrine Transaction command wrapper.
 */
interface RequiresTransactionInterface
{
}
