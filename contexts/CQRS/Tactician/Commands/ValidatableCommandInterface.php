<?php

namespace Contexts\CQRS\Tactician\Commands;

use Symfony\Component\Validator\Constraints\Collection;

interface ValidatableCommandInterface
{
    public function serializeForValidation(): array;

    public function getValidationConstraints(): Collection;
}
