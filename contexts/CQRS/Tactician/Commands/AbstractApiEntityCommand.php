<?php

namespace Contexts\CQRS\Tactician\Commands;

use Contexts\CQRS\Helpers\DateTimeHelper;
use DateTimeImmutable;
use Ramsey\Uuid\Rfc4122\UuidV4;

abstract class AbstractApiEntityCommand implements
    ApiEntityCommandInterface,
                                                   HandlerDefiningInterface,
                                                   LoggableCommandInterface,
                                                   RequiresTransactionInterface
{
    protected DateTimeImmutable $dispatchedAt;

    protected int $version;

    protected ?UuidV4 $id;

    protected array $__initial_data;

    public function __construct(array $data, int $version, ?UuidV4 $id = null)
    {
        $this->dispatchedAt = DateTimeHelper::create();
        $this->version = $version;
        $this->id = $id;
        $this->__initial_data = $data;
        $initialiseMethod = 'initialiseV' . $version;

        if (!method_exists($this, $initialiseMethod)) {
            throw new \RuntimeException("Could not initialise version $version for " . self::class);
        }

        $this->{$initialiseMethod}($data);
    }

    public static function fromArray(array $data, $version, ?UuidV4 $id)
    {
        // We may want some hooks here...
        return new self($data, $version, $id);
    }

    public function isVersion(int $version): bool
    {
        return $this->version == $version;
    }

    public function getId(): ?UuidV4
    {
        return $this->id;
    }

    public function getChangeset(): array
    {
        return $this->__initial_data;
    }

    public function getChangeMeta(): array
    {
        return [
            'method' => $this->id === null ? 'create' : 'update',
            'apiVersion' => $this->version,
            'dispatchedAt' => DateTimeHelper::formatForStorage($this->dispatchedAt),
        ];
    }

    public function getPrettyCommandName(): string
    {
        return self::class;
    }

    abstract public function getHandlerClass(): string;
}
