<?php

namespace Contexts\CQRS\Tactician\Handler\Locator;

use Contexts\CQRS\Tactician\Commands\HandlerDefiningInterface;
use Laravel\Lumen\Application;

class ContainerAwareLocator implements HandlerLocator
{
    protected Application $app;

    // Make this framework agnostic
    // Should be Psr\Container\ContainerInterface
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getHandlerForCommand(HandlerDefiningInterface $command)
    {
        return $this->app->make($command->getHandlerClass());
    }
}
