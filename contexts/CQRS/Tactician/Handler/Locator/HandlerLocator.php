<?php

namespace Contexts\CQRS\Tactician\Handler\Locator;

use Contexts\CQRS\Tactician\Commands\HandlerDefiningInterface;

interface HandlerLocator
{
    /**
     * @param HandlerDefiningInterface $commandName
     * @param HandlerDefiningInterface $command
     *
     * @return void
     */
    public function getHandlerForCommand(HandlerDefiningInterface $command);
}
