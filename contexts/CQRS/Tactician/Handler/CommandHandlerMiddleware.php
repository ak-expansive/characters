<?php

namespace Contexts\CQRS\Tactician\Handler;

use Contexts\CQRS\Tactician\Commands\HandlerDefiningInterface;
use Contexts\CQRS\Tactician\Handler\Locator\HandlerLocator;
use League\Tactician\Exception\CanNotInvokeHandlerException;
use League\Tactician\Handler\MethodNameInflector\MethodNameInflector;
use League\Tactician\Middleware;

/**
 * The "core" CommandBus. Locates the appropriate handler and executes command.
 */
class CommandHandlerMiddleware implements Middleware
{
    /**
     * @var HandlerLocator
     */
    private $handlerLocator;

    /**
     * @var MethodNameInflector
     */
    private $methodNameInflector;

    /**
     * @param HandlerLocator      $handlerLocator
     * @param MethodNameInflector $methodNameInflector
     */
    public function __construct(
        HandlerLocator $handlerLocator,
        MethodNameInflector $methodNameInflector
    ) {
        $this->handlerLocator = $handlerLocator;
        $this->methodNameInflector = $methodNameInflector;
    }

    /**
     * Executes a command and optionally returns a value
     *
     * @param object   $command
     * @param callable $next
     *
     * @throws CanNotInvokeHandlerException
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        return $this->handleCommand($command);
    }

    protected function handleCommand(HandlerDefiningInterface $command)
    {
        $handler = $this->handlerLocator->getHandlerForCommand($command);
        $methodName = $this->methodNameInflector->inflect($command, $handler);

        // is_callable is used here instead of method_exists, as method_exists
        // will fail when given a handler that relies on __call.
        if (!is_callable([$handler, $methodName])) {
            throw CanNotInvokeHandlerException::forCommand(
                $command,
                "Method '{$methodName}' does not exist on handler"
            );
        }

        return $handler->{$methodName}($command);
    }
}
