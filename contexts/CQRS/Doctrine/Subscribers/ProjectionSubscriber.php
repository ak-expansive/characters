<?php

namespace Contexts\CQRS\Doctrine\Subscribers;

use Contexts\CQRS\Models\Command\HasReadModelInterface;
use Contexts\CQRS\Models\Query\QueryModelInterface;
use Contexts\ElasticSearch\Repository\DocumentRepository;
use Contexts\ElasticSearch\Repository\Manager;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class ProjectionSubscriber implements EventSubscriber
{
    protected Manager $esRepoManager;

    public function __construct(Manager $manager)
    {
        $this->esRepoManager = $manager;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove,
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->projectModel(
            $args->getObject()
        );
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->deleteModel(
            $args->getObject()
        );
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->projectModel(
            $args->getObject()
        );
    }

    /**
     * @param mixed $entity
     *
     * @return void
     */
    protected function projectModel($entity)
    {
        if (!$entity instanceof HasReadModelInterface) {
            // Nothing to project
            return;
        }

        $readModelClass = $entity->getReadModelClass();

        if (!$this->readModelImplementsQueryModel($readModelClass)) {
            // Nothing to project
            return;
        }

        $readModel = $readModelClass::createFromCommandModel($entity);

        $repo = $this->esRepoManager->get($readModelClass);

        if ($repo instanceof DocumentRepository) {
            $repo->saveOne($readModel);
        }
    }

    protected function deleteModel($entity)
    {
        if (!$entity instanceof HasReadModelInterface) {
            // Nothing to delete
            return;
        }

        $readModelClass = $entity->getReadModelClass();

        if (!$this->readModelImplementsQueryModel($readModelClass)) {
            // Nothing to delete
            return;
        }

        $readModel = $readModelClass::createFromCommandModel($entity);

        $repo = $this->esRepoManager->get($readModelClass);

        if ($repo instanceof DocumentRepository) {
            $repo->deleteOne($readModel);
        }
    }

    protected function readModelImplementsQueryModel(string $readModelClass)
    {
        return in_array(
            QueryModelInterface::class,
            class_implements($readModelClass)
        );
    }
}
