<?php

namespace Contexts\CQRS\Doctrine\Repository;

use Contexts\CQRS\Doctrine\Models\CommandLog;
use Contexts\CQRS\Repository\CommandLogRepository as RepositoryInterface;
use Contexts\CQRS\Tactician\Commands\LoggableCommandInterface;
use Doctrine\ORM\EntityRepository;

class CommandLogRepository extends EntityRepository implements RepositoryInterface
{
    public function add(LoggableCommandInterface $command, array $globalMeta, string $status)
    {
        $commandLog = CommandLog::create($command, $globalMeta, $status);

        $this->_em->persist($commandLog);
        $this->_em->flush($commandLog);
    }
}
