<?php

namespace Contexts\CQRS\Doctrine\Models;

use Contexts\Core\Traits\GeneratesUuids;
use Contexts\CQRS\Models\CommandLogInterface;
use Contexts\CQRS\Tactician\Commands\LoggableCommandInterface;
use Ramsey\Uuid\Rfc4122\UuidV4;

class CommandLog implements CommandLogInterface
{
    use GeneratesUuids;

    protected UuidV4 $id;

    protected string $changeName;

    protected string $status;

    protected array $changes;

    protected array $meta;

    protected array $globalMeta;

    protected function __construct(UuidV4 $id, string $changeName, array $changes, array $meta, array $globalMeta, string $status)
    {
        $this->id = $id;
        $this->changeName = $changeName;
        $this->changes = $changes;
        $this->meta = $meta;
        $this->globalMeta = $globalMeta;
        $this->status = $status;
    }

    public static function create(LoggableCommandInterface $command, array $globalExtras, string $status): self
    {
        return new self(
            static::generateUuid(),
            $command->getPrettyCommandName(),
            $command->getChangeset(),
            $command->getChangeMeta(),
            $globalExtras,
            $status
        );
    }
}
