<?php

namespace Contexts\CQRS\Repository;

use Contexts\CQRS\Models\CommandLogInterface;
use Contexts\CQRS\Tactician\Commands\LoggableCommandInterface;

interface CommandLogRepository
{
    /**
     * @param CommandLogInterface      $commandLogInterface
     * @param LoggableCommandInterface $command
     * @param array                    $globalMeta
     * @param string                   $status
     *
     * @return void
     */
    public function add(LoggableCommandInterface $command, array $globalMeta, string $status);
}
