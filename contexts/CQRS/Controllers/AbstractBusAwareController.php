<?php

namespace Contexts\CQRS\Controllers;

use Contexts\CQRS\Tactician\Commands\HandlerDefiningInterface;
use League\Tactician\CommandBus;

abstract class AbstractBusAwareController
{
    protected CommandBus $bus;

    public function __construct(CommandBus $bus)
    {
        $this->bus = $bus;
    }

    protected function dispatch(HandlerDefiningInterface $command)
    {
        return $this->bus->handle($command);
    }
}
