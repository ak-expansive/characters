<?php

namespace Contexts\CQRS\Contracts;

interface Arrayable
{
    public function toArray(): array;
}
