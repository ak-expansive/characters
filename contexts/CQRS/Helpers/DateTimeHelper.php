<?php

namespace Contexts\CQRS\Helpers;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;

class DateTimeHelper
{
    public const NOW = 'now';

    public const STORAGE_TIMEZONE = 'UTC';

    public const STORAGE_FORMAT = DateTimeInterface::ATOM;

    /**
     * @param string $datetime
     * @param int    $timezone
     *
     * @return DateTimeImmutable
     */
    public static function create(string $datetime = self::NOW, $timezone = self::STORAGE_TIMEZONE): DateTimeImmutable
    {
        if (!$timezone instanceof DateTimeZone) {
            $timezone = new DateTimeZone($timezone);
        }

        return new DateTimeImmutable(
            $datetime,
            $timezone
        );
    }

    public static function formatForStorage(DateTimeImmutable $dt): string
    {
        return $dt->format(self::STORAGE_FORMAT);
    }
}
