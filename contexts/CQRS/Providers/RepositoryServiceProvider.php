<?php

namespace Contexts\CQRS\Providers;

use Contexts\CQRS\Doctrine\Models\CommandLog as DoctrineCommandLog;
use Contexts\CQRS\Doctrine\Repository\CommandLogRepository as DoctrineCommandLogRepository;
use Contexts\CQRS\Repository\CommandLogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $CQRSConfig = config('cqrs');
        $commandLogRepository = $CQRSConfig['commandLogRepository'] ?? DoctrineCommandLogRepository::class;

        if ($commandLogRepository == DoctrineCommandLogRepository::class) {
            $this->app->singleton(DoctrineCommandLogRepository::class, function ($app) {
                return $app->make(EntityManagerInterface::class)
                    ->getRepository(DoctrineCommandLog::class);
            });
        }

        $this->app->singleton(CommandLogRepository::class, function ($app) use ($commandLogRepository) {
            return $app->make($commandLogRepository);
        });
    }
}
