<?php

namespace Contexts\CQRS\Providers;

use Contexts\CQRS\Tactician\Handler\CommandHandlerMiddleware;
use Contexts\CQRS\Tactician\Handler\Locator\ContainerAwareLocator;
use Illuminate\Support\ServiceProvider;
use League\Tactician\CommandBus;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;

class TacticianServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ContainerAwareLocator::class, function ($app) {
            return new ContainerAwareLocator($app);
        });

        $this->app->bind(CommandHandlerMiddleware::class, function ($app) {
            return new CommandHandlerMiddleware(
                $app->make(ContainerAwareLocator::class),
                $app->make(HandleInflector::class)
            );
        });

        $CQRSConfig = config('cqrs');

        $this->app->singleton(CommandBus::class, function ($app) use ($CQRSConfig) {
            $middleware = array_map([$app, 'make'], $CQRSConfig['middleware'] ?? []);

            $middleware[] = $app->make(
                $CQRSConfig['primaryHandler'] ?? CommandHandlerMiddleware::class
            );

            return new CommandBus($middleware);
        });
    }
}
