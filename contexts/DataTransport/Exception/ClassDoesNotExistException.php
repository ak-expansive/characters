<?php

namespace Contexts\DataTransport\Exception;

use Exception;

class ClassDoesNotExistException extends Exception
{
    public static function forImportCommandClass(string $FQCN): self
    {
        return new self(
            "The class '$FQCN' does not exist to be used for import."
        );
    }
}
