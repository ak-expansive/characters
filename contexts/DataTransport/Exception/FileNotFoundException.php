<?php

namespace Contexts\DataTransport\Exception;

use Exception;

class FileNotFoundException extends Exception
{
    public static function forImport(string $filePath): self
    {
        return new self(
            "File not found for import: $filePath"
        );
    }
}
