<?php

namespace Contexts\DataTransport\Exception;

use Exception;

class UnexpectedDataStructureException extends Exception
{
    public static function forMissingKey(string $key): self
    {
        return new self(
            "The key '$key' was expected."
        );
    }

    public static function forBadValue(string $key, string $expectedType): self
    {
        return new self(
            "The key '$key' was expected to be of type: $expectedType."
        );
    }

    public static function forInvalidValue(string $key, $givenValue, array $expectedValues): self
    {
        if (is_scalar($givenValue) || (is_object($givenValue) && method_exists($givenValue, '__toString'))) {
            $displayValue = (string) $givenValue;
        } elseif (is_object($givenValue)) {
            $displayValue = 'object(' . get_class($givenValue) . ')';
        }

        return new self(
            "Got '$displayValue' for '$key'; expected one of: " . implode(', ', $expectedValues)
        );
    }
}
