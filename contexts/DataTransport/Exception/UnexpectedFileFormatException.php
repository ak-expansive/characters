<?php

namespace Contexts\DataTransport\Exception;

use Exception;

class UnexpectedFileFormatException extends Exception
{
    public static function shouldBeJSON(string $filePath): self
    {
        return new self(
            "The file at '$filePath' was expected to be valid JSON."
        );
    }
}
