<?php

namespace Contexts\DataTransport\Exception;

use Exception;

class CouldNotFindVersionException extends Exception
{
    public static function forImportItem(?string $identifyingValue): self
    {
        return new self(
            "No version was found for item, and no default was provided. (identifier: $identifyingValue)"
        );
    }
}
