<?php

namespace Contexts\DataTransport\Import;

interface ImporterInterface
{
    public function run(): void;

    public function getName(): string;

    public function setOutputCallback(callable $callback): void;
}
