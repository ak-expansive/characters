<?php

namespace Contexts\DataTransport\Import;

use Contexts\DataTransport\Exception\CouldNotFindVersionException;
use Contexts\DataTransport\Exception\FileNotFoundException;
use Contexts\DataTransport\Exception\UnexpectedDataStructureException;
use Contexts\DataTransport\Exception\UnexpectedFileFormatException;
use League\Tactician\CommandBus;

abstract class AbstractImporter
{
    public const RESERVED_VERSION_KEY = '__version';

    protected ?array $data;

    protected CommandBus $commandBus;

    // can't have type callable
    protected $outputCallback = null;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function setOutputCallback(callable $callback): void
    {
        $this->outputCallback = $callback;
    }

    protected function addOutput(string $message): void
    {
        if ($this->outputCallback === null) {
            return;
        }

        ($this->outputCallback)($message);
    }

    abstract protected function getAllowedVersions(): array;

    protected function getVersion(array $item, ?string $identifyingValue): ?int
    {
        $defaultVersion = $this->data[self::RESERVED_VERSION_KEY] ?? null;

        $version = $item[self::RESERVED_VERSION_KEY] ?? $defaultVersion;

        if ($version === null) {
            throw CouldNotFindVersionException::forImportItem($identifyingValue ?? 'unknown');
        }

        return $version;
    }

    protected function loadFile(string $filePath): void
    {
        $this->data = null;
        $fileExists = file_exists($filePath);

        if (!$fileExists) {
            throw FileNotFoundException::forImport($filePath);
        }

        // Shouldn't ever get an exception unless it's a dodgy file...
        // Let it bubble for now if we do
        $contents = file_get_contents($filePath);

        $data = json_decode($contents, true);

        if (!is_array($data)) {
            throw UnexpectedFileFormatException::shouldBeJSON($filePath);
        }

        if (!array_key_exists('items', $data)) {
            throw UnexpectedDataStructureException::forMissingKey('items');
        }

        if (!is_array($data['items'])) {
            throw UnexpectedDataStructureException::forBadValue('items', 'array');
        }

        $hasDefaultVersion = array_key_exists(self::RESERVED_VERSION_KEY, $data);
        $defaultVersion = $data[self::RESERVED_VERSION_KEY] ?? null;

        if ($hasDefaultVersion && !is_int($defaultVersion)) {
            throw UnexpectedDataStructureException::forBadValue(self::RESERVED_VERSION_KEY, 'integer');
        }

        if ($hasDefaultVersion && !in_array($defaultVersion, $this->getAllowedVersions())) {
            throw UnexpectedDataStructureException::forInvalidValue(
                self::RESERVED_VERSION_KEY,
                $defaultVersion,
                $this->getAllowedVersions()
            );
        }

        $this->data = $data;
    }
}
