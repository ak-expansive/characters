<?php

namespace Contexts\DataTransport\Console;

use Contexts\DataTransport\Import\ImporterInterface;
use Illuminate\Console\Command;

class ImportDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:transport:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports data via the Importers.';

    public function __construct(ImporterInterface ...$importers)
    {
        parent::__construct();
        $this->importers = $importers;
    }

    public function handle()
    {
        foreach ($this->importers as $importer) {
            $importer->setOutputCallback(function (string $message, string $type = 'info') {
                $this->{$type}("\t" . $message);
            });
            $this->info("Running: " . $importer->getName());
            $importer->run();
            $this->info("Finished: " . $importer->getName());
        }
    }
}
