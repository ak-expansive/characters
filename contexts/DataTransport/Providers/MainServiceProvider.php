<?php

namespace Contexts\DataTransport\Providers;

use Contexts\Characters\Import\CharactersImporter;
use Contexts\DataTransport\Console\ImportDataCommand;
use Illuminate\Support\ServiceProvider;

class MainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ImportDataCommand::class, function ($app) {
            return new ImportDataCommand(
                $app->make(CharactersImporter::class)
            );
        });
    }

    public function boot()
    {
        $this->commands([
            ImportDataCommand::class,
        ]);
    }
}
