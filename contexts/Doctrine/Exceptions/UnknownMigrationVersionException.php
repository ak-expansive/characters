<?php

namespace Contexts\Doctrine\Exceptions;

class UnknownMigrationVersionException extends \RuntimeException
{
}
