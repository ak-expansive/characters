<?php

namespace Contexts\Doctrine\Exceptions;

use Doctrine\ORM\EntityNotFoundException as BaseException;

class EntityNotFoundException extends BaseException
{
    public static function fromClassNameAndFieldValue(string $className, string $field, string $value): self
    {
        return new self(
            "Entity of type '$className' was not found for '$field'='$value'"
        );
    }
}
