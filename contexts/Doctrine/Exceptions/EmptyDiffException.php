<?php

namespace Contexts\Doctrine\Exceptions;

class EmptyDiffException extends \RuntimeException
{
}
