<?php

namespace Contexts\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\SchemaValidator;

class SchemaManager
{
    protected EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function showCreate(): string
    {
        return $this->implodeToLines(
            $this->getSchemaTool()->getCreateSchemaSql(
                $this->getMetadata()
            )
        );
    }

    public function create(): void
    {
        $this->getSchemaTool()
            ->createSchema($this->getMetadata());
    }

    public function showUpdate(): string
    {
        return $this->implodeToLines(
            $this->getSchemaTool()->getUpdateSchemaSql(
                $this->getMetadata()
            )
        );
    }

    public function update(): void
    {
        $this->getSchemaTool()
            ->updateSchema($this->getMetadata());
    }

    public function showDrop(): string
    {
        return $this->implodeToLines(
            $this->getSchemaTool()->getDropSchemaSQL(
                $this->getMetadata()
            )
        );
    }

    public function drop(): void
    {
        $this->getSchemaTool()->dropSchema(
            $this->getMetadata()
        );
    }

    public function validate(): array
    {
        $validator = $this->getValidator();

        $output = [];
        $mappingErrors = $validator->validateMapping();

        foreach ($mappingErrors as $className => $errorMessages) {
            $output['mapping'][$className] = $this->implodeToLines($errorMessages, 1);
        }

        if (!$this->schemaInSyncWithMetadata()) {
            $output['synchronisation'] = 'Database schema is out of synchronisation with metadata';
        }

        return $output;
    }

    public function schemaInSyncWithMetadata()
    {
        return $this->getValidator()->schemaInSyncWithMetadata();
    }

    protected function getValidator()
    {
        return new SchemaValidator($this->em);
    }

    protected function getSchemaTool(): SchemaTool
    {
        return new SchemaTool($this->em);
    }

    protected function getMetadata(): array
    {
        return $this->em->getMetadataFactory()->getAllMetadata();
    }

    protected function implodeToLines(array $lines, int $tabs = 0): string
    {
        return implode(
            str_repeat('    ', $tabs) . PHP_EOL,
            $lines
        );
    }
}
