<?php

namespace Contexts\Doctrine\Migrations;

use Contexts\Doctrine\Exceptions\UnknownMigrationVersionException;
use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Migrator;
use Doctrine\Migrations\MigratorConfiguration;

class Runner
{
    protected ConfigurationBuilder $configBuilder;

    protected Configuration $config;

    public function __construct(ConfigurationBuilder $configBuilder)
    {
        $this->configBuilder = $configBuilder;
    }

    public function show(string $version): array
    {
        return $this->getMigrator()->getSql(
            $this->getRealVersion($version)
        );
    }

    public function migrate(string $version, bool $isDryRun): array
    {
        return $this->getMigrator()->migrate(
            $this->getRealVersion($version),
            $this->buildMigratorConfig($isDryRun)
        );
    }

    protected function makeConfig()
    {
        $this->config = $this->configBuilder->make();
    }

    protected function getMigrator(): Migrator
    {
        $this->makeConfig();
        return new Migrator(
            $this->config,
            $this->config->getDependencyFactory()->getMigrationRepository(),
            $this->config->getOutputWriter(),
            $this->config->getDependencyFactory()->getStopwatch()
        );
    }

    protected function getRealVersion(string $potentialVersion): string
    {
        $version = $this->config->resolveVersionAlias($potentialVersion);

        if ($version === null) {
            throw new UnknownMigrationVersionException("Unknown migration version: $potentialVersion");
        }

        return $version;
    }

    protected function buildMigratorConfig(bool $isDryRun)
    {
        $migratorConfig = new MigratorConfiguration();
        $migratorConfig->setDryRun($isDryRun);
        $migratorConfig->setTimeAllQueries(true);
        $migratorConfig->setNoMigrationException(false);

        return $migratorConfig;
    }
}
