<?php

namespace Contexts\Doctrine\Migrations;

use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Finder\RecursiveRegexFinder;
use Doctrine\ORM\EntityManagerInterface;

class ConfigurationBuilder
{
    protected EntityManagerInterface $em;

    protected array $config;

    public function __construct(EntityManagerInterface $em, array $config)
    {
        $this->em = $em;
        $this->config = $config;
    }

    /**
     * @param array $config
     *
     * @return Configuration
     */
    public function make(): Configuration
    {
        $configuration = new Configuration($this->em->getConnection());
        $configuration->setName('Doctrine Migrations');
        $configuration->setMigrationsNamespace($this->config['namespace'] ?? 'Database\\Migrations');
        $configuration->setMigrationsTableName($this->config['table'] ?? 'migrations');

        $configuration->getConnection()->getConfiguration()->setFilterSchemaAssetsExpression(
            $this->config['schema_filter'] ?? '/^(?).*$/'
        );

        $configuration->setMigrationsFinder(new RecursiveRegexFinder);

        $directory = $this->config['directory'] ?? base_path() . '/migrations';
        $configuration->setMigrationsDirectory($directory);
        $configuration->registerMigrationsFromDirectory($directory);

        return $configuration;
    }
}
