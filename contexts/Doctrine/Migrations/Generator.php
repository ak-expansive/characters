<?php

namespace Contexts\Doctrine\Migrations;

use Contexts\Doctrine\Exceptions\EmptyDiffException;
use DateTime;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Generator\Generator as BaseGenerator;
use Doctrine\Migrations\Provider\OrmSchemaProvider;
use Doctrine\ORM\EntityManagerInterface;

class Generator
{
    protected ConfigurationBuilder $configBuilder;

    protected EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, ConfigurationBuilder $configBuilder)
    {
        $this->em = $em;
        $this->configBuilder = $configBuilder;
    }

    public function empty(): string
    {
        $generator = new BaseGenerator($this->getConfig());

        return $generator->generateMigration(
            $this->getVersion()
        );
    }

    public function diff(): string
    {
        $config = $this->getConfig();

        $fromSchema = $this->buildFromSchema($config);
        $toSchema   = $this->buildToSchema();

        $builder = new SQLBuilder;
        $up   = $builder->up($config, $fromSchema, $toSchema);
        $down = $builder->down($config, $fromSchema, $toSchema);

        if (!$up && !$down) {
            throw new EmptyDiffException("No changes were found.");
        }

        $generator = new BaseGenerator($config);
        $version = $this->getVersion();

        return $generator->generateMigration(
            $version,
            $up,
            $down
        );
    }

    protected function getConfig()
    {
        return $this->configBuilder->make();
    }

    protected function buildFromSchema(Configuration $config): Schema
    {
        $connection  = $config->getConnection();

        return $connection->getSchemaManager()->createSchema();
    }

    protected function buildToSchema(): Schema
    {
        $provider = new OrmSchemaProvider($this->em);

        return $provider->createSchema();
    }

    protected function getVersion(): string
    {
        return (new DateTime())
            ->format('YmdHis');
    }
}
