<?php

namespace Contexts\Doctrine\Console\Schema;

class DropSchemaCommand extends AbstractSchemaCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doctrine:schema:drop {--show} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drops the schema associated with doctrine mappings.';

    protected function doShow(): int
    {
        $this->comment($this->schemaManager->showDrop());

        return 0;
    }

    protected function doExecute(): int
    {
        $this->info('Dropping Schema...');
        $this->schemaManager->drop();
        $this->info('Schema Dropped.');

        return 0;
    }
}
