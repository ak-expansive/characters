<?php

namespace Contexts\Doctrine\Console\Schema;

use Contexts\Doctrine\SchemaManager;
use Illuminate\Console\Command;

class ValidateSchemaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doctrine:schema:validate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validates the schema associated with doctrine mappings.';

    /**
     * @var SchemaManager
     */
    protected SchemaManager $schemaManager;

    /**
     * Create a new command instance.
     *
     * @param SchemaManager $schemaManager
     *
     * @return void
     */
    public function __construct(SchemaManager $schemaManager)
    {
        parent::__construct();

        $this->schemaManager = $schemaManager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $errors = $this->schemaManager->validate();

        $this->showMappingErrors($errors['mappings'] ?? []);
        $this->showSynchronisationErrors($errors['synchronisation'] ?? null);
    }

    protected function showMappingErrors(array $errors): void
    {
        if (empty($errors)) {
            $this->info('Entity mappings are valid!');

            return;
        }

        foreach ($errors as $class => $errors) {
            $this->error("Mapping validation errors for: $class");
            $this->error($errors);
        }
    }

    protected function showSynchronisationErrors(?string $error): void
    {
        if ($error === null) {
            $this->info("Schema and mappings are in sync!");
            return;
        }

        $this->error($error);
    }
}
