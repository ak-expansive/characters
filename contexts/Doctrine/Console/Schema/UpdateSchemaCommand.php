<?php

namespace Contexts\Doctrine\Console\Schema;

class UpdateSchemaCommand extends AbstractSchemaCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doctrine:schema:update {--show} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the schema based on doctrine mappings.';

    protected function doShow(): int
    {
        $this->comment($this->schemaManager->showUpdate());

        return 0;
    }

    protected function doExecute(): int
    {
        if ($this->schemaManager->schemaInSyncWithMetadata()) {
            $this->info("No updates to perform!");
            return 0;
        }

        $this->info('Updating Schema...');
        $this->schemaManager->update();
        $this->info('Schema Updated.');

        return 0;
    }
}
