<?php

namespace Contexts\Doctrine\Console\Schema;

use Contexts\Doctrine\SchemaManager;
use Illuminate\Console\Command;

abstract class AbstractSchemaCommand extends Command
{
    /**
     * @var SchemaManager
     */
    protected SchemaManager $schemaManager;

    /**
     * Create a new command instance.
     *
     * @param SchemaManager $schemaManager
     *
     * @return void
     */
    public function __construct(SchemaManager $schemaManager)
    {
        parent::__construct();

        $this->schemaManager = $schemaManager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->warn("Commands in the schema namespace can produce unexpected results.");
        $this->warn("Migrations are the preferred way to manage the schema.");
        $this->warn("These commands have been left as they can be useful in dev.");
        $this->warn("*    You may experience problems with schemas when using these with postgres.");
        $this->warn("     See: https://github.com/doctrine/dbal/issues/1110");
        $this->warn("*    The migrations table may be dropped.");
        $this->line('');
        $this->line('');

        if ($this->option('show')) {
            return $this->doShow();
        }

        $shouldProceed = $this->option('force') || $this->confirm('Are you want to continue?');

        if (!$shouldProceed) {
            $this->warn("Aborting, due to lack of '--force' or correct prompt response.");
            return 0;
        }

        return $this->doExecute();
    }

    abstract protected function doShow(): int;

    abstract protected function doExecute(): int;
}
