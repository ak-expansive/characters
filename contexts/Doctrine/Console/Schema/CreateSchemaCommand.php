<?php

namespace Contexts\Doctrine\Console\Schema;

class CreateSchemaCommand extends AbstractSchemaCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doctrine:schema:create {--show} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates the schema based on doctrine mappings.';

    protected function doShow(): int
    {
        $this->comment($this->schemaManager->showCreate());

        return 0;
    }

    protected function doExecute(): int
    {
        $this->info('Creating Schema...');
        $this->schemaManager->create();
        $this->info('Schema Created.');

        return 0;
    }
}
