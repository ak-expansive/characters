<?php

namespace Contexts\Doctrine\Console\Migration;

use Contexts\Doctrine\Exceptions\EmptyDiffException;
use Contexts\Doctrine\Migrations\Generator;
use Illuminate\Console\Command;

class GenerateDiffMigrationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doctrine:migration:autogen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates a new migration based upon the differences between current db schema and entity metadata.';

    /**
     * @var Generator
     */
    protected Generator $generator;

    /**
     * Create a new command instance.
     *
     * @param Generator $generator
     *
     * @return void
     */
    public function __construct(Generator $generator)
    {
        parent::__construct();

        $this->generator = $generator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Calculating changes...");

        try {
            $outputPath = $this->generator->diff();
        } catch (EmptyDiffException $e) {
            $this->error('There were no changes to generate a migration for!');
            return;
        }

        $this->info("Generated Migration file at: $outputPath");
    }
}
