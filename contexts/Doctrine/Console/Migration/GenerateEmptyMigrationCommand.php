<?php

namespace Contexts\Doctrine\Console\Migration;

use Contexts\Doctrine\Migrations\Generator;
use Illuminate\Console\Command;

class GenerateEmptyMigrationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doctrine:migration:empty';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates an empty migration.';

    /**
     * @var Generator
     */
    protected Generator $generator;

    /**
     * Create a new command instance.
     *
     * @param Generator $generator
     *
     * @return void
     */
    public function __construct(Generator $generator)
    {
        parent::__construct();

        $this->generator = $generator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Generating migration...");

        $outputPath = $this->generator->empty();

        $this->info("Generated Migration file at: $outputPath");
    }
}
