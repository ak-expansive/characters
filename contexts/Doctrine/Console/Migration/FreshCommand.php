<?php

namespace Contexts\Doctrine\Console\Migration;

use Contexts\Doctrine\Migrations\ConfigurationBuilder;
use Contexts\Doctrine\Migrations\Generator;
use Contexts\Doctrine\Migrations\Runner;
use Illuminate\Console\Command;

class FreshCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doctrine:migration:fresh {--force} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resets all migrations.';

    /**
     * @var Runner
     */
    protected Runner $runner;

    /**
     * Create a new command instance.
     *
     * @param Generator            $generator
     * @param ConfigurationBuilder $configBuilder
     * @param Runner               $runner
     *
     * @return void
     */
    public function __construct(Runner $runner)
    {
        parent::__construct();

        $this->runner = $runner;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shouldProceed = $this->option('force') === true || $this->confirm("You will lose all local data, are you sure?");

        if (!$shouldProceed) {
            $this->warn("Aborting refresh.");
            return 0;
        }

        $this->info("Resetting database...");
        $successful = $this->runMigrator('first');

        if (!$successful) {
            $this->error("An unexpected error occurred.");
            return 1;
        }

        $this->info("Database reset!");
        $this->line('');
        $this->line('');

        $this->info("Running migrations...");
        $successful = $this->runMigrator('latest');

        if (!$successful) {
            $this->error("An unexpected error occurred.");
            return 1;
        }
        $this->info("All migrations complete.");
    }

    protected function runMigrator(string $potentialVersion): bool
    {
        try {
            $SQLOutput = $this->runner->migrate(
                $potentialVersion,
                false
            );

            $this->outputSQLResult($SQLOutput);
        } catch (\Throwable $e) {
            $this->error($e->getMessage());
            return false;
        }

        return true;
    }

    protected function outputSQLResult(array $result)
    {
        if (empty($result)) {
            $this->info("No new migrations to run.");
        }

        $SQLResult = array_map(function (array $sqlLines) {
            return implode(PHP_EOL, $sqlLines);
        }, $result);

        foreach ($SQLResult as $migrationVersion => $changes) {
            $this->info("Changes for migration: " . $migrationVersion);
            $this->line('');
            $this->comment($changes);
            $this->line('');
            $this->line('');
        }
    }
}
