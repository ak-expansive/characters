<?php

namespace Contexts\Doctrine\Console\Migration;

use Contexts\Doctrine\Migrations\ConfigurationBuilder;
use Contexts\Doctrine\Migrations\Generator;
use Contexts\Doctrine\Migrations\Runner;
use Illuminate\Console\Command;

class ResetCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doctrine:migration:reset {--show} {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resets all migrations.';

    /**
     * @var Runner
     */
    protected Runner $runner;

    /**
     * Create a new command instance.
     *
     * @param Generator            $generator
     * @param ConfigurationBuilder $configBuilder
     * @param Runner               $runner
     *
     * @return void
     */
    public function __construct(Runner $runner)
    {
        parent::__construct();

        $this->runner = $runner;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $potentialVersion = 'first';

        try {
            if ($this->option('show')) {
                $this->outputSQLResult(
                    $this->runner->show($potentialVersion)
                );
                return 0;
            }

            $SQLOutput = $this->runner->migrate(
                $potentialVersion,
                $this->option('dry-run') === true
            );

            $this->outputSQLResult($SQLOutput);
        } catch (\Throwable $e) {
            $this->error($e->getMessage());
            return 1;
        }
    }

    protected function outputSQLResult(array $result)
    {
        if (empty($result)) {
            $this->info("No new migrations to run.");
        }

        $SQLResult = array_map(function (array $sqlLines) {
            return implode(PHP_EOL, $sqlLines);
        }, $result);

        foreach ($SQLResult as $migrationVersion => $changes) {
            $this->info("Changes for migration: " . $migrationVersion);
            $this->line('');
            $this->comment($changes);
            $this->line('');
            $this->line('');
        }
    }
}
