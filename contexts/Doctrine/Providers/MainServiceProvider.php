<?php

namespace Contexts\Doctrine\Providers;

use Contexts\Doctrine\Console\Migration\FreshCommand;
use Contexts\Doctrine\Console\Migration\GenerateDiffMigrationCommand;
use Contexts\Doctrine\Console\Migration\GenerateEmptyMigrationCommand;
use Contexts\Doctrine\Console\Migration\ResetCommand;
use Contexts\Doctrine\Console\Migration\RunMigrationsCommand;
use Contexts\Doctrine\Console\Schema\CreateSchemaCommand;
use Contexts\Doctrine\Console\Schema\DropSchemaCommand;
use Contexts\Doctrine\Console\Schema\UpdateSchemaCommand;
use Contexts\Doctrine\Console\Schema\ValidateSchemaCommand;
use Contexts\Doctrine\Migrations\ConfigurationBuilder;
use Contexts\Doctrine\Subscribers\FixPostgreSQLDefaultSchema;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use Illuminate\Support\ServiceProvider;
use MartinGeorgiev\Doctrine\DBAL\Types\BigIntArray;
use MartinGeorgiev\Doctrine\DBAL\Types\IntegerArray;
use MartinGeorgiev\Doctrine\DBAL\Types\Jsonb;
use MartinGeorgiev\Doctrine\DBAL\Types\JsonbArray;
use MartinGeorgiev\Doctrine\DBAL\Types\SmallIntArray;
use MartinGeorgiev\Doctrine\DBAL\Types\TextArray;
use Ramsey\Uuid\Doctrine\UuidType;

class MainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $doctrineConfig = config('doctrine');
        $emConfig = Setup::createXMLMetadataConfiguration(
            $doctrineConfig['mappingDirectories'] ?? [],
            config('app.debug'),
            $doctrineConfig['proxyDir']
        );

        $this->app->singleton(EntityManagerInterface::class, function ($app) use ($doctrineConfig, $emConfig) {
            $em = EntityManager::create($doctrineConfig['connection'], $emConfig);

            $em->getEventManager()->addEventSubscriber(new FixPostgreSQLDefaultSchema);

            foreach ($doctrineConfig['subscribers'] as $subscriber) {
                $em->getEventManager()->addEventSubscriber($app->make($subscriber));
            }

            return $em;
        });

        $this->app->singleton(ConfigurationBuilder::class, function ($app) {
            return new ConfigurationBuilder(
                $app->make(EntityManagerInterface::class),
                config('doctrine.migrations')
            );
        });
    }

    public function boot()
    {
        Type::addType('uuid', UuidType::class);
        Type::addType('jsonb', Jsonb::class);
        Type::addType('jsonb[]', JsonbArray::class);
        Type::addType('smallint[]', SmallIntArray::class);
        Type::addType('integer[]', IntegerArray::class);
        Type::addType('bigint[]', BigIntArray::class);
        Type::addType('text[]', TextArray::class);

        $this->commands([
            CreateSchemaCommand::class,
            UpdateSchemaCommand::class,
            DropSchemaCommand::class,
            ValidateSchemaCommand::class,

            GenerateDiffMigrationCommand::class,
            GenerateEmptyMigrationCommand::class,
            RunMigrationsCommand::class,
            ResetCommand::class,
            FreshCommand::class,
        ]);
    }
}
