<?php

namespace Contexts\ApiSerialization\Transformation;

class TransformerConfig
{
    public function __construct(string $transformerClass, array $config = [])
    {
        $this->transformerClass = $transformerClass;
        $this->config = $config;
    }

    public function getClass(): string
    {
        return $this->transformerClass;
    }

    public function getConfig(): array
    {
        return $this->config;
    }
}