<?php

namespace Contexts\ApiSerialization\Transformation;

use Contexts\ApiSerialization\Exceptions\TransformerNotFoundException;
use Contexts\ApiSerialization\Exceptions\ConflictingTransformerRegistrationException;

class TransformersContainer
{
    protected array $transformers;

    public function __construct(TransformerInterface ...$transformers)
    {
        $keys = array_map('get_class', $transformers);

        $this->guardTransformerDuplicates($keys);

        $this->transformers = array_combine($keys, $transformers);
    }

    protected function guardTransformerDuplicates(array $classNames)
    {
        $counts = array_count_values($classNames);
        $nonUniqueEntries = array_filter($counts, function ($value) {
            return $value > 1;
        });

        if (count($nonUniqueEntries) > 0) {
            throw ConflictingTransformerRegistrationException::forContainerRegistration(
                ...array_keys($nonUniqueEntries)
            );
        }
    }

    public function get(string $className)
    {
        if (! isset($this->transformers[$className])) {
            throw TransformerNotFoundException::forUnregisteredClassInContainer($className);
        }

        return $this->transformers[$className];
    }
}