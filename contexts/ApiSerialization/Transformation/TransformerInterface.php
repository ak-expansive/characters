<?php

namespace Contexts\ApiSerialization\Transformation;

interface TransformerInterface
{
    public function transform(array $data, array $config): array;
}