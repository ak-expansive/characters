<?php

namespace Contexts\ApiSerialization\Contracts;

interface ApiSerializableInterface
{
    public function forApi(int $version): array;
}
