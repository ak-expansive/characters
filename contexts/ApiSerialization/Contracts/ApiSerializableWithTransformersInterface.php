<?php

namespace Contexts\ApiSerialization\Contracts;

interface ApiSerializableWithTransformersInterface extends ApiSerializableInterface
{
    public function getTransformerConfigs(): array;
}
