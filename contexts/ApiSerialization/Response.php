<?php

namespace Contexts\ApiSerialization;

use InvalidArgumentException;
use Contexts\ApiSerialization\Transformation\TransformerConfig;
use Contexts\ApiSerialization\Contracts\ApiSerializableInterface;
use Contexts\ApiSerialization\Transformation\TransformersContainer;
use Contexts\ApiSerialization\Contracts\ApiSerializableWithTransformersInterface;

class Response
{
    public const ERROR_PAGE_OUT_OF_BOUNDS = 'PAGE_OUT_OF_BOUNDS';

    public const AVAILABLE_ERROR_CODES = [
        self::ERROR_PAGE_OUT_OF_BOUNDS,
    ];

    public const EMPTY_RESPONSE_CODES = [
        202,
        204,
        404,
    ];

    public const ERROR_RESPONSE_CODES = [
        400,
    ];

    protected ?int $version;

    protected ?ApiSerializableInterface $result = null;

    protected ?array $results = null;

    protected array $pagination = [];

    protected ?int $statusCode = 200;

    protected ?string $errorCode = null;

    protected ?array $headers = [];

    public function __construct(TransformersContainer $transformersContainer)
    {
        $this->transformersContainer = $transformersContainer;
    }

    public function addHeader(string $header, string $value): self
    {
        $this->headers[$header] = $value;

        return $this;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function setStatusCode(int $statusCode)
    {
        if ($this->errorCode !== null && $statusCode < 400) {
            throw new \InvalidArgumentException('Successful status code used when an error code is set.');
        }

        $this->statusCode = $statusCode;

        return $this;
    }

    public function setSingularResult(ApiSerializableInterface $result): self
    {
        $this->result = $result;
        $this->results = null;

        return $this;
    }

    public function setResults(ApiSerializableInterface ...$result): self
    {
        $this->results = $result;
        $this->result = null;

        return $this;
    }

    public function setPage(int $page): self
    {
        $this->pagination['page'] = $page;

        return $this;
    }

    public function setPageSize(int $pageSize): self
    {
        $this->pagination['page_size'] = $pageSize;

        return $this;
    }

    public function setPageTotal(int $pageTotal): self
    {
        $this->pagination['page_total'] = $pageTotal;

        return $this;
    }

    public function setTotalPages(int $totalPages): self
    {
        $this->pagination['total_pages'] = $totalPages;

        return $this;
    }

    public function setTotalResults(int $total): self
    {
        $this->pagination['total'] = $total;

        return $this;
    }

    public function serialize(): ?array
    {
        if (in_array($this->statusCode, self::EMPTY_RESPONSE_CODES)) {
            return null;
        }

        if ($this->errorCode !== null) {
            return [
                'error_code' => $this->errorCode,
            ];
        }

        if (in_array($this->statusCode, self::ERROR_RESPONSE_CODES)) {
            return null;
        }

        $meta = [];

        if (!empty($this->pagination)) {
            $meta['pagination'] = $this->pagination;
        }

        return [
            '_meta' => $meta,
            'data' => $this->getResponseData(),
        ];
    }

    public function statusCode(): int
    {
        return $this->statusCode;
    }

    public function setErrorCode(string $errorCode)
    {
        if (!in_array($errorCode, self::AVAILABLE_ERROR_CODES)) {
            throw new InvalidArgumentException("Unknown error code: $errorCode");
        }

        $this->errorCode = $errorCode;
        $this->overrideStatusCodeOnError();

        return $this;
    }

    protected function overrideStatusCodeOnError()
    {
        // Basically anything above 400 can be an acceptable error code
        // Anything below implies a successful request
        // We'll override to 400 in these cases as we can't really be more specific
        if ($this->errorCode !== null && $this->statusCode < 400) {
            $this->statusCode = 400;
        }
    }

    public function setVersion(int $version): self
    {
        $this->version = $version;

        return $this;
    }

    protected function getResponseData()
    {
        if ($this->result === null && $this->results === null) {
            throw new \RuntimeException("There is no result set.");
        }

        if ($this->result !== null) {
            return $this->serializeResult($this->result);
        }

        return array_map([$this, 'serializeResult'], $this->results);
    }

    protected function serializeResult(ApiSerializableInterface $result): array
    {
        $data = $result->forApi($this->version);

        if ($result instanceof ApiSerializableWithTransformersInterface) {
            $data = $this->runTransformers(
                $data,
                ...$result->getTransformerConfigs(),
            );
        }

        return $data;
    }

    protected function runTransformers(array $data, TransformerConfig ...$transformerConfigs): array
    {
        foreach ($transformerConfigs as $config) {
            $transformer = $this->transformersContainer->get($config->getClass());

            $data = $transformer->transform($data, $config->getConfig());
        }

        return $data;
    }
}
