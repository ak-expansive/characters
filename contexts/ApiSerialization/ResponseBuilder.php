<?php

namespace Contexts\ApiSerialization;

use Contexts\ApiSerialization\Transformation\TransformersContainer;

class ResponseBuilder
{
    protected TransformersContainer $transformersContainer;

    public function __construct(TransformersContainer $transformersContainer)
    {
        $this->transformersContainer = $transformersContainer;
    }

    protected function buildBaseResponse(): Response
    {
        return new Response(
            $this->transformersContainer
        );
    }

    public function forVersion(int $version): Response
    {
        return $this->buildBaseResponse()
            ->setVersion($version);
    }

    public function notFound(): Response
    {
        return $this->buildBaseResponse()
            ->setStatusCode(404);
    }

    public function accepted(): Response
    {
        return $this->buildBaseResponse()
            ->setStatusCode(202);
    }

    public function noContent(): Response
    {
        return $this->buildBaseResponse()
            ->setStatusCode(204);
    }

    public function badRequest(): Response
    {
        return $this->buildBaseResponse()
            ->setStatusCode(400);
    }
}