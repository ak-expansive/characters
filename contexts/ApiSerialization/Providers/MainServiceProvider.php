<?php

namespace Contexts\ApiSerialization\Providers;

use Illuminate\Support\ServiceProvider;
use Contexts\ApiSerialization\Transformation\TransformersContainer;

class MainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TransformersContainer::class, function($app) {
            $transformers = array_map([$app, 'make'], config('transformers.registered'));

            return new TransformersContainer(...$transformers);
        });
    }
}
