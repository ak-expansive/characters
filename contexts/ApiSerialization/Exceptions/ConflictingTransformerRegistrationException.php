<?php

namespace Contexts\ApiSerialization\Exceptions;

use Exception;

class ConflictingTransformerRegistrationException extends Exception
{
    public static function forContainerRegistration(string ...$duplicatedClassNames)
    {
        $implodedClassNames = "'" . implode("', '", $duplicatedClassNames) . "'";

        return new self(
            "The classes [$implodedClassNames] appeared more than once in the Transformers Container."
        );
    }
}