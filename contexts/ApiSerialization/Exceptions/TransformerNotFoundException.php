<?php

namespace Contexts\ApiSerialization\Exceptions;

use Exception;

class TransformerNotFoundException extends Exception
{
    public static function forUnregisteredClassInContainer(string $class)
    {
        return new self(
            "The class '$class' was registered in the container."
        );
    }
}