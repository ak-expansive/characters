<?php

namespace Contexts\Imgo\Providers;

use Contexts\Imgo\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\ServiceProvider;

class MainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Client::class, function ($app) {
            $guzzleClient = new GuzzleClient([
                'base_uri' => config('imgo.host'),

                'timeout' => 30,

                'headers' => [
                    'Content-Type' => 'multipart/form-data',
                ],
            ]);

            return new Client(
                $guzzleClient
            );
        });
    }

    public function boot()
    {
    }
}
