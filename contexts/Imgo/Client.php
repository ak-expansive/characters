<?php

namespace Contexts\Imgo;

use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\File\File;
use Contexts\Core\Images\ImageUploaderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Client implements ImageUploaderInterface
{
    protected ClientInterface $httpClient;

    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function upload(UploadedFile $file): string
    {
        $response = $this->httpClient->request(
            'PUT',
            '/upload',
            [
                'multipart' => [
                    [
                        'name'     => 'image',
                        'contents' => fopen($file->getPathname(), 'r'),
                        'filename' => $file->getClientOriginalName(),
                    ]
                ],
            ]
        );

        $location = $response->getHeader('Location')[0] ?? null;

        // throw error is null

        $urlParts = parse_url($location);

        return $urlParts['path'];
    }
}