<?php

namespace Contexts\Validation;

use Symfony\Component\Validator\ConstraintViolationListInterface;

interface ValidatableModelInterface
{
    public function validate(string $stage, bool $shouldThrow = true): ConstraintViolationListInterface;
}
