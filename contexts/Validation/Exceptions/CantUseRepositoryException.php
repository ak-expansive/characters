<?php

namespace Contexts\Validation\Exceptions;

use Exception;

class CantUseRepositoryException extends Exception
{
    public static function forCountWhere(string $class)
    {
        $implementationRequirement = CountWhere::class;
        return new self("Class $class does not implement the $implementationRequirement interface.");
    }
}
