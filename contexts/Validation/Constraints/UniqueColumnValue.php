<?php

namespace Contexts\Validation\Constraints;

use Symfony\Component\Validator\Constraint;

class UniqueColumnValue extends Constraint
{
    public $field;

    public $entity;

    public $exclude = [];

    public $message = 'A resource exists with {{ field }} equal to {{ value }}.';

    public function __construcy($options = null)
    {
        parent::__construct($options);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return ['field', 'entity'];
    }
}
