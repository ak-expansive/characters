<?php

namespace Contexts\Validation\Constraints;

use Contexts\Validation\Contracts\CountsWhere;
use Contexts\Validation\Exceptions\CantUseRepositoryException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueColumnValueValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $field = $constraint->field;
        $entity = $constraint->entity;
        $exclude = $constraint->exclude ?? [];

        $repo = $this->getEntityManager()->getRepository($entity);

        if (!$repo instanceof CountsWhere) {
            throw CantUseRepositoryException::forCountWhere(get_class($repo));
        }

        $count = $repo->countWhere($field, $value, $exclude);

        if ($count > 0) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setParameter('{{ field }}', $this->formatValue($field))
                ->setCode('UNIQUE_COLUMN_VALUE_ERROR')
                ->addViolation();
        }
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return app()->make(EntityManagerInterface::class);
    }
}
