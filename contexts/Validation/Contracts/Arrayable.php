<?php

namespace Contexts\Validation\Contracts;

interface Arrayable
{
    public function toArray(): array;
}
