<?php

namespace Contexts\Validation\Contracts;

interface CountsWhere
{
    public function countWhere(string $field, $value, $exclude = []): int;
}
