<?php

namespace Contexts\Characters\Providers;

use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    public function boot()
    {
        $this->app->router->group([
            'prefix' => 'api/v1/characters',
        ], function ($router) {
            require __DIR__ . '/../routes/v1.php';
        });
    }
}
