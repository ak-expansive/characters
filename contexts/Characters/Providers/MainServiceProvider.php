<?php

namespace Contexts\Characters\Providers;

use Contexts\Characters\Import\CharactersImporter;
use Contexts\Characters\Repository\Command\CharacterRepository;
use Illuminate\Support\ServiceProvider;
use League\Tactician\CommandBus;

class MainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CharactersImporter::class, function ($app) {
            return new CharactersImporter(
                $app->make(CommandBus::class),
                $app->make(CharacterRepository::class),
                __DIR__ . '/../seeds/characters.json'
            );
        });
    }

    public function boot()
    {
    }
}
