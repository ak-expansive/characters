<?php

namespace Contexts\Characters\Providers;

use Contexts\Characters\Models\Command\Character as CommandCharacter;
use Contexts\Characters\Models\Query\Character as QueryCharacter;
use Contexts\Characters\Repository\Command\CharacterRepository as CommandCharacterRepository;
use Contexts\Characters\Repository\Query\CharacterRepository as QueryCharacterRepository;
use Contexts\ElasticSearch\Repository\Manager;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CommandCharacterRepository::class, function ($app) {
            return $app->make(EntityManagerInterface::class)
                ->getRepository(CommandCharacter::class);
        });

        $this->app->singleton(QueryCharacterRepository::class, function ($app) {
            return $app->make(Manager::class)
                ->get(QueryCharacter::class);
        });
    }

    public function boot()
    {
    }
}
