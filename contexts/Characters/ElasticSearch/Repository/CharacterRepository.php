<?php

namespace Contexts\Characters\ElasticSearch\Repository;

use Contexts\Characters\Models\Query\Character;
use Contexts\Characters\Repository\Query\CharacterRepository as RepositoryInterface;
use Contexts\ElasticSearch\Repository\DocumentRepository;
use Contexts\ElasticSearch\Repository\QueryResult;
use Ramsey\Uuid\Rfc4122\UuidV4;

class CharacterRepository extends DocumentRepository implements RepositoryInterface
{
    public function byId(UuidV4 $id): ?Character
    {
        $result = $this->client->search([
            'index' => $this->getIndexName(),
            'body' => [
                'query' => [
                    'match' => [
                        '_id' => (string) $id,
                    ],
                ],
            ],
        ]);

        $models = $this->hydrateResults($result);

        if (count($models) > 1) {
            throw new \RuntimeException('Multiple docs for same id...');
        }

        return $models[0] ?? null;
    }

    public function all(int $page, int $pageSize, ?array $filters = [], ?string $search = null): QueryResult
    {
        $query = [
            'match_all' => new \StdClass,
        ];

        if (!empty($filters) || $search !== null) {
            $query = [
                'bool' => [],
            ];
        }

        if (!empty($filters)) {
            $query['bool']['filter'] = $this->buildFilterQuery($filters);
        }

        if (!empty($search)) {
            $query['bool']['must'] = $this->buildFullSearchQuery($search);
        }

        $result = $this->client->search([
            'size' => $pageSize,
            'from' => ($page - 1) * $pageSize,
            'index' => $this->getIndexName(),
            'body' => [
                'query' => $query,
            ],
        ]);

        $models = $this->hydrateResults($result);

        return (new QueryResult())
            ->setPage($page)
            ->setPageSize($pageSize)
            ->setIndexTotal($result['hits']['total']['value'])
            ->setResultCollection(...$models);
    }

    protected function buildFilterQuery(array $filters): array
    {
        $filter = [];


        foreach ($filters as $field => $values) {
            $filter[]['terms'][$field . ".raw"] = $values;
        }

        return $filter;
    }

    protected function buildFullSearchQuery(string $search)
    {
        return [
                'multi_match' => [
                    'query' => $search,
                    'fuzziness' => "3",
                    'fields' => [
                        '*_name',
                        'slug',
                    ],
                ],

        ];
    }
}
