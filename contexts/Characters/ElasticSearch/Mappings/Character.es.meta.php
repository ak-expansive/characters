<?php

use Contexts\Characters\ElasticSearch\Repository\CharacterRepository;
use Contexts\Characters\Models\Query\Character;

return [
    'model' => Character::class,
    'repository' => CharacterRepository::class,
    'mappings' => [
        'properties' => [
            'first_name' => [
                'type' => 'text',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                    ],
                ],
            ],
            'last_name' => [
                'type' => 'text',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                    ],
                ],
            ],
            'slug' => [
                'type' => 'text',
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                    ],
                ],
            ],
            'image' => [
                'type' => 'keyword'
            ]
        ],
    ],
    'indexName' => env('CHARACTERS_ES_INDEX_NAME', 'characters'),
];
