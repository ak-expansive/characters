<?php

use Contexts\Characters\Controllers\V1Controller;

$router->get('/', [
    'as' => 'list',
    'uses' => V1Controller::class . '@list',
]);

$router->get('/{id}', [
    'as' => 'display',
    'uses' => V1Controller::class . '@display',
]);

$router->post('/', [
    'as' => 'create',
    'uses' => V1Controller::class . '@create',
]);

$router->patch('/{id}', [
    'as' => 'update',
    'uses' => V1Controller::class . '@update',
]);

$router->delete('/{id}', [
    'as' => 'delete',
    'uses' => V1Controller::class . '@delete',
]);

$router->post('/{id}/image', [
    'as' => 'add_image',
    'uses' => V1Controller::class . '@addImage',
]);
