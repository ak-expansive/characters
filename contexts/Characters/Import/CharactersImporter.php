<?php

namespace Contexts\Characters\Import;

use Contexts\Characters\Commands\UpsertCharacter;
use Contexts\Characters\Repository\Command\CharacterRepository;
use Contexts\DataTransport\Exception\UnexpectedDataStructureException;
use Contexts\DataTransport\Import\AbstractImporter;
use Contexts\DataTransport\Import\ImporterInterface;
use League\Tactician\CommandBus;

class CharactersImporter extends AbstractImporter implements ImporterInterface
{
    protected CharacterRepository $repo;

    protected string $filePath;

    public function __construct(CommandBus $commandBus, CharacterRepository $repo, string $filePath)
    {
        parent::__construct($commandBus);

        $this->repo = $repo;
        $this->filePath = $filePath;
    }

    public function getName(): string
    {
        return 'characters';
    }

    public function run(): void
    {
        $this->loadFile($this->filePath);

        foreach ($this->data['items'] as $character) {
            $this->import($character);
        }
    }

    protected function getAllowedVersions(): array
    {
        return [
            1,
        ];
    }

    protected function guardSlugField(array $character): void
    {
        if (!array_key_exists('slug', $character)) {
            throw UnexpectedDataStructureException::forMissingKey('slug');
        }

        if (!is_string($character['slug'])) {
            throw UnexpectedDataStructureException::forBadValue('slug', 'string');
        }
    }

    protected function guardVersion(array $item): void
    {
        if (array_key_exists(self::RESERVED_VERSION_KEY, $this->data)) {
            // If there is a valid default, we don't need to throw an exception here...
            // The type was already validated in loadFile
            return;
        }

        if (!array_key_exists(self::RESERVED_VERSION_KEY, $item)) {
            throw UnexpectedDataStructureException::forMissingKey('slug');
        }

        if (!is_int($item[self::RESERVED_VERSION_KEY])) {
            throw UnexpectedDataStructureException::forBadValue('slug', 'string');
        }
    }

    protected function import(array $character): void
    {
        $this->guardSlugField($character);
        $this->guardVersion($character);

        $this->addOutput("Processing: " . $character['slug']);
        $existingCharacter = $this->repo->bySlug($character['slug']);
        // Try $existingCharacter->getId() ?? null
        $id = $existingCharacter ? $existingCharacter->getId() : null;

        $this->commandBus->handle(new UpsertCharacter(
            $character,
            $this->getVersion($character, $character['slug']),
            $id
        ));
        $this->addOutput("Processed: " . $character['slug']);
        $this->addOutput("");
    }
}
