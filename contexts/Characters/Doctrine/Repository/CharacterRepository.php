<?php

namespace Contexts\Characters\Doctrine\Repository;

use Contexts\Characters\Models\Command\Character;
use Contexts\Characters\Repository\Command\CharacterRepository as RepositoryInterface;
use Contexts\Doctrine\Exceptions\EntityNotFoundException;
use Contexts\Validation\Contracts\CountsWhere;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Ramsey\Uuid\Rfc4122\UuidV4;

class CharacterRepository extends EntityRepository implements
    RepositoryInterface,
                                                              CountsWhere
{
    public function byId(UuidV4 $id, bool $shouldThrowIfNotFound = false): ?Character
    {
        $qb = $this->createQueryBuilder('c');
        $eb = $qb->expr();

        try {
            $result = $qb->select('c')
                ->where($eb->eq('c.id', ':id'))
                ->setParameter('id', (string) $id)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            if ($shouldThrowIfNotFound) {
                throw EntityNotFoundException::fromClassNameAndIdentifier(self::class, [(string) $id]);
            }
        }

        return $result ?? null;
    }

    public function bySlug(string $slug, bool $shouldThrowIfNotFound = false): ?Character
    {
        $qb = $this->createQueryBuilder('c');
        $eb = $qb->expr();

        try {
            $result = $qb->select('c')
                ->where($eb->eq('c.slug', ':slug'))
                ->setParameter('slug', $slug)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            if ($shouldThrowIfNotFound) {
                throw EntityNotFoundException::fromClassNameAndFieldValue(self::class, 'slug', $slug);
            }
        }

        return $result ?? null;
    }

    public function countWhere(string $field, $value, $exclude = []): int
    {
        $qb = $this->createQueryBuilder('c');
        $eb = $qb->expr();

        $query = $qb->select('count(1)')
            ->andWhere(
                $eb->eq('c.' . $field, ':testField')
            );

        if (!empty($exclude)) {
            $query->andWhere($eb->notIn('c.id', ':exclude'))
                ->setParameter('exclude', $exclude);
        }

        $result = $query->setParameter('testField', (string) $value)
            ->getQuery()
            ->getSingleScalarResult();

        return $result;
    }

    public function save(Character $character): ?Character
    {
        $this->_em->persist($character);
        $this->_em->flush($character);

        return $character;
    }

    public function delete(Character $character): void
    {
        $this->_em->remove($character);
        $this->_em->flush();
    }
}
