<?php

namespace Contexts\Characters\Models\Command;

use Contexts\Characters\Models\Query\Character as ReadModel;
use Contexts\Core\Traits\GeneratesUuids;
use Contexts\CQRS\Models\Command\HasReadModelInterface;
use Ramsey\Uuid\Rfc4122\UuidV4;

class Character implements HasReadModelInterface
{
    use GeneratesUuids;

    protected UuidV4 $id;

    protected string $firstName = '';

    protected string $lastName = '';

    protected string $slug = '';

    protected array $otherNames = [];

    protected ?string $image = null;

    protected function __construct(UuidV4 $id)
    {
        $this->id = $id;
    }

    public function getId(): UuidV4
    {
        return $this->id;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setImage(string $path)
    {
        $this->image = $path;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setOtherNames(array $otherNames): void
    {
        $this->otherNames = $otherNames;
    }

    public static function create()
    {
        return new self(static::generateUuid());
    }

    public function getReadModelClass(): string
    {
        return ReadModel::class;
    }
}
