<?php

namespace Contexts\Characters\Models\Query;

use Ramsey\Uuid\Rfc4122\UuidV4;
use Contexts\Core\Images\ImageUrlHelper;
use Contexts\Core\Images\UrlTransformer;
use Contexts\CQRS\Models\Query\QueryModelInterface;
use Contexts\ElasticSearch\Documents\IdentifiableDocument;
use Contexts\ApiSerialization\Transformation\TransformerConfig;
use Contexts\Characters\Models\Command\Character as CommandModel;
use Contexts\ApiSerialization\Contracts\ApiSerializableWithTransformersInterface;

class Character implements QueryModelInterface, 
                           ApiSerializableWithTransformersInterface
{
    protected UuidV4 $id;

    protected string $firstName;

    protected string $lastName;

    protected string $imageUrl;

    protected array $otherNames;

    protected function __construct(
        UuidV4 $id,
        string $firstName,
        string $lastName,
        string $slug,
        string $imageUrl
    ) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->slug = $slug;
        $this->imageUrl = $imageUrl;
    }

    public function getTransformerConfigs(): array
    {
        return [
            new TransformerConfig(UrlTransformer::class, ['keys' => ['image']]),
        ];
    }

    public static function createFromCommandModel($commandModel): QueryModelInterface
    {
        if (!$commandModel instanceof CommandModel) {
            throw new \InvalidArgumentException();
        }

        return new self(
            $commandModel->getId(),
            $commandModel->getFirstName(),
            $commandModel->getLastName(),
            $commandModel->getSlug(),
            $commandModel->getImage() ?? ImageUrlHelper::DEFAULT_URI
        );
    }

    public function forApi(int $version): array
    {
        // Version may be used later
        return [
            'id' => $this->id,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'slug' => $this->slug,
            'image' => $this->imageUrl
        ];
    }

    public function toDocument(): IdentifiableDocument
    {
        return IdentifiableDocument::withBody(
            (string) $this->id,
            [
                'first_name' => $this->firstName,
                'last_name' => $this->lastName,
                'slug' => $this->slug,
                'image' => $this->imageUrl
            ]
        );
    }

    public static function fromDocument(IdentifiableDocument $document): self
    {
        return new self(
            UuidV4::fromString($document->getId()),
            $document->get('[first_name]'),
            $document->get('[last_name]'),
            $document->get('[slug]'),
            $document->get('[image]'),
        );
    }
}
