<?php

namespace Contexts\Characters\Handlers;

use Contexts\ApiSerialization\ResponseBuilder;
use Contexts\Characters\Repository\Query\CharacterRepository;
use Contexts\Characters\Commands\DisplayCharacter as DisplayCharacterCommand;

class DisplayCharacter
{
    protected CharacterRepository $repo;
    protected ResponseBuilder $responseBuilder;

    public function __construct(CharacterRepository $repo, ResponseBuilder $responseBuilder)
    {
        $this->repo = $repo;
        $this->responseBuilder = $responseBuilder;
    }

    public function handle(DisplayCharacterCommand $command)
    {
        $character = $this->repo->byId($command->getId());

        if ($character === null) {
            return $this->responseBuilder->notFound();
        }

        $response = $this->responseBuilder
            ->forVersion($command->getVersion())
            ->setSingularResult($character);

        return $response;
    }
}
