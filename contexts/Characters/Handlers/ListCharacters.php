<?php

namespace Contexts\Characters\Handlers;

use Contexts\ApiSerialization\Response;
use Contexts\ApiSerialization\ResponseBuilder;
use Contexts\Characters\Repository\Query\CharacterRepository;
use Contexts\Characters\Commands\ListCharacters as ListCharactersCommand;

class ListCharacters
{
    protected CharacterRepository $repo;
    protected ResponseBuilder $responseBuilder;

    public function __construct(CharacterRepository $repo, ResponseBuilder $responseBuilder)
    {
        $this->repo = $repo;
        $this->responseBuilder = $responseBuilder;
    }

    public function handle(ListCharactersCommand $command)
    {
        $queryResult = $this->repo->all(
            $command->getPage(),
            $command->getPageSize(),
            $command->getFilters(),
            $command->getSearch()
        );

        $characters = $queryResult->getResultCollection();

        if (empty($characters) && $queryResult->getIndexTotal() > 0) {
            return $this->responseBuilder->badRequest("page out of bounds")
                ->setErrorCode(Response::ERROR_PAGE_OUT_OF_BOUNDS);
        }

        $response = $this->responseBuilder->forVersion($command->getVersion())
            ->setResults(...$characters)
            ->setPage($queryResult->getPage())
            ->setPageSize($queryResult->getPageSize())
            ->setPageTotal(count($characters))
            ->setTotalPages($queryResult->getTotalPages())
            ->setTotalResults($queryResult->getIndexTotal());

        return $response;
    }
}
