<?php

namespace Contexts\Characters\Handlers\V1;

use Contexts\Characters\Commands\UpsertCharacter as UpsertCharacterCommand;
use Contexts\Characters\Models\Command\Character;
use Contexts\Characters\Repository\Command\CharacterRepository;

class UpsertCharacter
{
    protected $repo;

    public function __construct(CharacterRepository $repo)
    {
        $this->repo = $repo;
    }

    public function handle(UpsertCharacterCommand $command)
    {
        $character = $this->getCharacter($command);

        if ($command->getFirstName() !== null) {
            $character->setFirstName(
                $command->getFirstName()
            );
        }

        if ($command->getLastName() !== null) {
            $character->setLastName(
                $command->getLastName()
            );
        }

        if ($command->getOtherNames() !== null) {
            $character->setOtherNames(
                $command->getOtherNames()
            );
        }

        if ($command->getSlug() !== null) {
            $character->setSlug(
                $command->getSlug()
            );
        }

        $this->repo->save($character);

        return $character;
    }

    protected function getCharacter(UpsertCharacterCommand $command)
    {
        $id = $command->getId();

        if ($id === null) {
            return Character::create();
        }

        return $this->repo->byId(
            $command->getId(),
            true
        );
    }
}
