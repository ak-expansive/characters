<?php

namespace Contexts\Characters\Handlers\V1;

use Throwable;
use Contexts\ApiSerialization\Response;
use Contexts\Core\Images\ImageUrlHelper;
use Contexts\ApiSerialization\ResponseBuilder;
use Contexts\Core\Images\ImageUploaderInterface;
use Contexts\Characters\Commands\AddImage as AddImageCommand;
use Contexts\Characters\Repository\Command\CharacterRepository;

class AddImage
{
    protected CharacterRepository $repo;

    protected ImageUploaderInterface $imageUploader;
    protected ImageUrlHelper $imageUrlHelper;
    protected ResponseBuilder $responseBuilder;

    public function __construct(
        CharacterRepository $repo,
        ImageUploaderInterface $imageUploader,
        ImageUrlHelper $imageUrlHelper,
        ResponseBuilder $responseBuilder
    ) {
        $this->repo = $repo;
        $this->imageUploader = $imageUploader;
        $this->imageUrlHelper = $imageUrlHelper;
        $this->responseBuilder = $responseBuilder;
    }

    public function handle(AddImageCommand $command)
    {
        $character = $this->repo->byId($command->getId());

        if ($character === null) {
            return $this->responseBuilder->notFound();
        }

        try {
            $imagePath = $this->imageUploader->upload($command->getImage());

            $character->setImage($imagePath);

            $this->repo->save($character);
        } catch (Throwable $e) {
            throw $e;
        }

        return $this->responseBuilder->accepted()
            ->addHeader("Location", $this->imageUrlHelper->buildUrl($imagePath));
    }
}
