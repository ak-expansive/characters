<?php

namespace Contexts\Characters\Handlers;

use Contexts\ApiSerialization\Response;
use Contexts\ApiSerialization\ResponseBuilder;
use Contexts\Characters\Repository\Command\CharacterRepository;
use Contexts\Characters\Commands\DeleteCharacter as DeleteCharacterCommand;

class DeleteCharacter
{
    protected CharacterRepository $repo;
    protected ResponseBuilder $responseBuilder;

    public function __construct(CharacterRepository $repo, ResponseBuilder $responseBuilder)
    {
        $this->repo = $repo;
        $this->responseBuilder = $responseBuilder;
    }

    public function handle(DeleteCharacterCommand $command)
    {
        $character = $this->repo->byId($command->getId());

        if ($character === null) {
            return $this->responseBuilder->notFound();
        }

        $this->repo->delete($character);

        return $this->responseBuilder->accepted();
    }
}
