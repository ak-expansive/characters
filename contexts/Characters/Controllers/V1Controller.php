<?php

namespace Contexts\Characters\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Rfc4122\UuidV4;
use Contexts\Characters\Commands\AddImage;
use Contexts\Characters\Commands\ListCharacters;
use Contexts\Characters\Commands\DeleteCharacter;
use Contexts\Characters\Commands\UpsertCharacter;
use Contexts\Characters\Commands\DisplayCharacter;
use Contexts\Core\Traits\ConvertsApiToJsonResponse;
use Contexts\CQRS\Controllers\AbstractBusAwareController;

class V1Controller extends AbstractBusAwareController
{
    use ConvertsApiToJsonResponse;

    public function list(Request $request)
    {
        return $this->toJsonResponse(
            $this->dispatch(
                new ListCharacters(1, $request->all())
            )
        );
    }

    public function display(string $id)
    {
        return $this->toJsonResponse(
            $this->dispatch(
                new DisplayCharacter(
                    UuidV4::fromString($id),
                    1
                )
            )
        );
    }

    public function create(Request $request)
    {
        $character = $this->dispatch(
            new UpsertCharacter(
                $request->get('data', []),
                1
            )
        );

        return $this->toJsonResponse(
            $this->dispatch(
                new DisplayCharacter(
                    $character->getId(),
                    1
                )
            )
        );
    }

    public function update(Request $request, string $id)
    {
        $character = $this->dispatch(
            new UpsertCharacter(
                $request->get('data', []),
                1,
                UuidV4::fromString($id)
            )
        );

        return $this->toJsonResponse(
            $this->dispatch(
                new DisplayCharacter(
                    $character->getId(),
                    1
                )
            )
        );
    }

    public function delete(string $id)
    {
        return $this->toJsonResponse(
            $this->dispatch(
                new DeleteCharacter(
                    UuidV4::fromString($id)
                )
            )
        );
    }

    public function addImage(Request $request, string $id)
    {
        return $this->toJsonResponse(
            $this->dispatch(
                new AddImage(
                    [
                        'image' => $request->file('image'),
                    ],
                    1,
                    UuidV4::fromString($id),
                )
            )
        );
    }
}
