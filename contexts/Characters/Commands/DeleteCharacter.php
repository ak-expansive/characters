<?php

namespace Contexts\Characters\Commands;

use Contexts\Characters\Handlers\DeleteCharacter as DeleteCharacterHandler;
use Contexts\CQRS\Helpers\DateTimeHelper;
use Contexts\CQRS\Tactician\Commands\HandlerDefiningInterface;
use Contexts\CQRS\Tactician\Commands\LoggableCommandInterface;
use Contexts\CQRS\Tactician\Commands\RequiresTransactionInterface;
use DateTimeImmutable;
use Ramsey\Uuid\Rfc4122\UuidV4;

class DeleteCharacter implements
    HandlerDefiningInterface,
                                 LoggableCommandInterface,
                                 RequiresTransactionInterface
{
    protected DateTimeImmutable $dispatchedAt;

    protected UuidV4 $id;

    public function __construct(UuidV4 $id)
    {
        $this->id = $id;
        $this->dispatchedAt = DateTimeHelper::create();
    }

    public function getId(): UuidV4
    {
        return $this->id;
    }

    public function getHandlerClass(): string
    {
        return DeleteCharacterHandler::class;
    }

    public function getChangeMeta(): array
    {
        return [
            'dispatchedAt' => DateTimeHelper::formatForStorage($this->dispatchedAt),
        ];
    }

    public function getChangeSet(): array
    {
        return [
            'id' => (string) $this->id,
        ];
    }

    public function getPrettyCommandName(): string
    {
        return 'character.delete';
    }
}
