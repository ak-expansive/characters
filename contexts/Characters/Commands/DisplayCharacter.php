<?php

namespace Contexts\Characters\Commands;

use Contexts\Characters\Handlers\DisplayCharacter as DisplayCharacterHandler;
use Contexts\CQRS\Tactician\Commands\HandlerDefiningInterface;
use Ramsey\Uuid\Rfc4122\UuidV4;

class DisplayCharacter implements HandlerDefiningInterface
{
    protected UuidV4 $id;

    public function __construct(UuidV4 $id, int $version)
    {
        $this->id = $id;
        $this->version = $version;
    }

    public function getId(): UuidV4
    {
        return $this->id;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function getHandlerClass(): string
    {
        return DisplayCharacterHandler::class;
    }

    public function getPrettyCommandName(): string
    {
        return 'character.display';
    }
}
