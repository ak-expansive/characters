<?php

namespace Contexts\Characters\Commands;

use Contexts\Characters\Handlers\ListCharacters as ListCharactersHandler;
use Contexts\Core\Filtering\FiltersResults;
use Contexts\Core\Pagination\PaginatesResults;
use Contexts\Core\Searching\SearchesResults;
use Contexts\Core\Util\Slug;
use Contexts\CQRS\Tactician\Commands\HandlerDefiningInterface;
use Contexts\CQRS\Tactician\Commands\ValidatableCommandInterface;
use Ramsey\Uuid\Rfc4122\UuidV4;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

class ListCharacters implements
    HandlerDefiningInterface,
                                ValidatableCommandInterface
{
    use PaginatesResults;
    use FiltersResults;
    use SearchesResults;

    protected UuidV4 $id;

    public function __construct(int $version, array $queryParams)
    {
        $this->version = $version;
        $this->setPaginationValues($queryParams);
        $this->setFilters($queryParams);
        $this->setSearch($queryParams);
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function getHandlerClass(): string
    {
        return ListCharactersHandler::class;
    }

    public function getPrettyCommandName(): string
    {
        return 'character.list';
    }

    public function serializeForValidation(): array
    {
        return array_merge(
            $this->serializePaginationForValidation(),
            $this->serializeFilterForValidation(),
            $this->serializeSearchForValidation(),
        );
    }

    public function getValidationConstraints(): Collection
    {
        $constraints = array_merge(
            $this->getPaginationValidationConstraints(),
            $this->getFilterValidationConstraints(),
            $this->getSearchValidationConstraints(),
        );

        return new Collection($constraints);
    }

    protected function getFilterFieldValidationConstraints(): array
    {
        $constraints = [];

        if (isset($this->filters['slug'])) {
            $slugConstraints = [
                new Regex(['pattern' => Slug::REGEX]),
                new Type(['type' => 'string']),
            ];

            $constraints['slug'] = [
                new All(['constraints' => $slugConstraints]),
                new Type(['type' => 'array']),
            ];
        }

        if (isset($this->filters['first_name'])) {
            $firstNameConstraints = [
                new Type(['type' => 'string']),
            ];

            $constraints['first_name'] = [
                new All(['constraints' => $firstNameConstraints]),
                new Type(['type' => 'array']),
            ];
        }

        if (isset($this->filters['last_name'])) {
            $firstNameConstraints = [
                new Type(['type' => 'string']),
            ];

            $constraints['last_name'] = [
                new All(['constraints' => $firstNameConstraints]),
                new Type(['type' => 'array']),
            ];
        }

        return $constraints;
    }

    protected function serializeFilterFieldsForValidation(): array
    {
        $fields = [];

        if (isset($this->filters['slug'])) {
            $fields['slug'] = $this->filters['slug'];
        }

        if (isset($this->filters['first_name'])) {
            $fields['first_name'] = $this->filters['first_name'];
        }

        if (isset($this->filters['last_name'])) {
            $fields['last_name'] = $this->filters['last_name'];
        }

        return $fields;
    }
}
