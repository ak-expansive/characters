<?php

namespace Contexts\Characters\Commands;

use SplFileInfo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\Collection;
use Contexts\CQRS\Tactician\Commands\AbstractApiEntityCommand;
use Contexts\CQRS\Tactician\Commands\ValidatableCommandInterface;
use Contexts\Characters\Handlers\V1\AddImage as AddImageV1Handler;
use Symfony\Component\Validator\Constraints\File as FileConstraint;

class AddImage extends AbstractApiEntityCommand implements ValidatableCommandInterface
{
    protected ?UploadedFile $image = null;

    public function getImage(): ?UploadedFile
    {
        return $this->image;
    }

    public function getHandlerClass(): string
    {
        return AddImageV1Handler::class;
    }

    public function getPrettyCommandName(): string
    {
        return 'character.images.add';
    }

    public function serializeForValidation(): array
    {
        return [
            'image' => $this->image,
        ];
    }

    public function getValidationConstraints(): Collection
    {
        $constraints = [
            'image' => [
                new NotBlank,
                new FileConstraint([
                    'maxSize' => '100M', // Whatever for now, it's dev we'll sort this later
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png',
                    ],
                    'mimeTypesMessage' => 'Must be a jpeg or png image',
                ]),
            ],
        ];

        return new Collection(
            $constraints
        );
    }

    protected function initialiseV1(array $data)
    {
        $this->image = $data['image'] ?? null;
    }
}
