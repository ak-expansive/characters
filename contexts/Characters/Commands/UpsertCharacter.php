<?php

namespace Contexts\Characters\Commands;

use Contexts\Characters\Handlers\V1\UpsertCharacter as UpsertCharacterV1Handler;
use Contexts\Characters\Models\Command\Character;
use Contexts\Core\Util\Slug;
use Contexts\CQRS\Tactician\Commands\AbstractApiEntityCommand;
use Contexts\CQRS\Tactician\Commands\ValidatableCommandInterface;
use Contexts\Validation\Constraints\UniqueColumnValue;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class UpsertCharacter extends AbstractApiEntityCommand implements ValidatableCommandInterface
{
    protected ?string $firstName;

    protected ?string $lastName;

    protected ?string $slug;

    protected ?array $otherNames;

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getOtherNames(): ?array
    {
        return $this->otherNames;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getHandlerClass(): string
    {
        return UpsertCharacterV1Handler::class;
    }

    public function getPrettyCommandName(): string
    {
        return $this->id === null ? 'character.create' : 'character.update';
    }

    public function serializeForValidation(): array
    {
        return [
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'slug' => $this->slug,
        ];
    }

    public function getValidationConstraints(): Collection
    {
        $constraints = [
            'first_name' => [
                new Length(['min' => 1, 'max' => 100]),
            ],
            'last_name' => [
                new Length(['min' => 1, 'max' => 100]),
            ],
            'slug' => [
                new Length(['min' => 1, 'max' => 250]),
                new Regex(['pattern' => Slug::REGEX]),
                new UniqueColumnValue([
                    'field' => 'slug',
                    'entity' => Character::class,
                    'exclude' => $this->id === null ? [] : [$this->id],
                ]),
            ],
        ];

        if ($this->id === null) {
            $constraints = $this->addCreationConstraints($constraints);
        }

        return new Collection(
            $constraints
        );
    }

    protected function initialiseV1(array $data)
    {
        $this->firstName = $data['first_name'] ?? null;
        $this->lastName = $data['last_name'] ?? null;
        $this->otherNames = $data['other_names'] ?? null;
        $this->slug = $data['slug'] ?? null;
    }

    protected function addCreationConstraints(array $constraints): array
    {
        $constraints['first_name'][] = new NotBlank;
        $constraints['last_name'][] = new NotBlank;
        $constraints['slug'][] = new NotBlank;

        return $constraints;
    }
}
