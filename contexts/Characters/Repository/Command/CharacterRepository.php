<?php

namespace Contexts\Characters\Repository\Command;

use Contexts\Characters\Models\Command\Character;
use Ramsey\Uuid\Rfc4122\UuidV4;

interface CharacterRepository
{
    public function byId(UuidV4 $id, bool $shouldThrowIfNotFound = false): ?Character;

    public function bySlug(string $slug, bool $shouldThrowIfNotFound = false): ?Character;

    public function save(Character $character): ?Character;

    public function delete(Character $character): void;
}
