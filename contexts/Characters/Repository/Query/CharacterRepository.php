<?php

namespace Contexts\Characters\Repository\Query;

use Contexts\Characters\Models\Query\Character;
use Contexts\ElasticSearch\Repository\QueryResult;
use Ramsey\Uuid\Rfc4122\UuidV4;

interface CharacterRepository
{
    public function byId(UuidV4 $id): ?Character;

    public function all(int $page, int $pageSize): QueryResult;
}
