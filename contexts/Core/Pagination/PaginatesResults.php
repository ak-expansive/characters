<?php

namespace Contexts\Core\Pagination;

use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Type;

trait PaginatesResults
{
    /*
     * No types here, as it would potentially cause errors when trying to set the properties.
     * The validation rules will ensure valid values are used.
     * We don't want a 500 error, if they use a string, we want a validation error.
    */
    protected $page;

    protected $pageSize;

    public function getPage(): int
    {
        return (int) $this->page;
    }

    public function getPageSize(): int
    {
        return (int) $this->pageSize;
    }

    protected function setPaginationValues(array $queryParams): void
    {
        $this->page = $queryParams['page'] ?? 1;
        $this->pageSize = $queryParams['page_size'] ?? $this->getDefaultPageSize();
    }

    protected function getPaginationValidationConstraints(): array
    {
        $constraints = [
            'page' => [
                new Positive,
                new Type(['type' => 'int']),
            ],
            'page_size' => [
                new Positive,
                new LessThanOrEqual(static::getMaxPageSize()),
                new Type(['type' => 'int']),
            ],
        ];

        return $constraints;
    }

    protected function serializePaginationForValidation(): array
    {
        return [
            'page' => is_numeric($this->page) ? (int) $this->page : $this->page,
            'page_size' => is_numeric($this->pageSize) ? (int) $this->pageSize : $this->pageSize,
        ];
    }

    protected static function getMaxPageSize(): int
    {
        return 100;
    }

    protected static function getDefaultPageSize(): int
    {
        return 20;
    }
}
