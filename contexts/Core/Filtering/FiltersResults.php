<?php

namespace Contexts\Core\Filtering;

use Symfony\Component\Validator\Constraints\Collection;

trait FiltersResults
{
    protected $filters = null;

    public function getFilters(): array
    {
        return $this->serializeFilterFieldsForValidation();
    }

    protected function setFilters(array $queryParams)
    {
        $this->filters = $queryParams['filter'] ?? null;
    }

    protected function getFilterValidationConstraints(): array
    {
        return [
            'filter' => new Collection(
                $this->getFilterFieldValidationConstraints()
            ),
        ];
    }

    protected function serializeFilterForValidation(): array
    {
        return [
            'filter' => $this->serializeFilterFieldsForValidation(),
        ];
    }

    abstract protected function getFilterFieldValidationConstraints(): array;

    abstract protected function serializeFilterFieldsForValidation(): array;
}
