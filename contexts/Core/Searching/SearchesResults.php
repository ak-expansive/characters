<?php

namespace Contexts\Core\Searching;

use Symfony\Component\Validator\Constraints\Type;

trait SearchesResults
{
    protected $search = null;

    public function getSearch(): ?string
    {
        return $this->search;
    }

    protected function setSearch(array $queryParams)
    {
        $this->search = $queryParams['search'] ?? null;
    }

    protected function getSearchValidationConstraints(): array
    {
        return [
            'search' => [
                new Type(['type' => 'string']),
            ],
        ];
    }

    protected function serializeSearchForValidation(): array
    {
        return [
            'search' => $this->search,
        ];
    }
}
