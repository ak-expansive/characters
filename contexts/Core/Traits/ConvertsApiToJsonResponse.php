<?php

namespace Contexts\Core\Traits;

use Contexts\ApiSerialization\Response;
use Illuminate\Http\JsonResponse;

trait ConvertsApiToJsonResponse
{
    protected function toJsonResponse(Response $response)
    {
        return new JsonResponse(
            $response->serialize(),
            $response->statusCode(),
            $response->getHeaders()
        );
    }
}
