<?php

namespace Contexts\Core\Traits;

use Ramsey\Uuid\Rfc4122\UuidV4;

trait GeneratesUuids
{
    protected static function generateUuid(): UuidV4
    {
        return UuidV4::uuid4();
    }
}
