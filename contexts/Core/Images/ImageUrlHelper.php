<?php

namespace Contexts\Core\Images;

class ImageUrlHelper
{
    const DEFAULT_URI = 'placeholder.jpg';

    protected $host;

    public function __construct(string $host)
    {
        $this->host = $host;
    }

    public function buildUrl(string $uri)
    {
        $uri = '/' . ltrim($uri, '/');

        return $this->host . $uri;
    }

    
}