<?php

namespace Contexts\Core\Images;

use Contexts\Core\Images\ImageUrlHelper;
use Contexts\ApiSerialization\Transformation\TransformerInterface;

class UrlTransformer implements TransformerInterface
{
    protected ImageUrlHelper $urlHelper;

    public function __construct(ImageUrlHelper $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

    public function transform(array $data, array $config): array
    {
        foreach ($config['keys'] as $key) {
            // throw if key is not found
            $data[$key] = $this->urlHelper->buildUrl($data[$key]);
        }

        return $data;
    }
}