<?php

namespace Contexts\Core\Util;

class Slug
{
    public const REGEX = '/^([a-z0-9]+-)*[a-z0-9]+$/';
}
