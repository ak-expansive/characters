<?php

namespace Contexts\Core\Providers;

use Contexts\Imgo\Client;
use Laravel\Lumen\Console\Kernel;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Support\ServiceProvider;
use Contexts\Core\Images\ImageUrlHelper;
use Illuminate\Contracts\Cache\Repository;
use Contexts\Core\Images\ImageUploaderInterface;
use Illuminate\Contracts\Console\Kernel as KernelContract;

class MainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // We don't have the default kernel in App, so we bind it back to the base
        $this->app->bind(KernelContract::class, Kernel::class);

        $this->app->bind(ImageUploaderInterface::class, Client::class);
        $this->app->bind(ImageUrlHelper::class, function() {
            return new ImageUrlHelper(config('images.host'));
        });

        $this->app->singleton(Store::class, function ($app) {
            return $app->make(Repository::class)->getStore();
        });
    }
}
