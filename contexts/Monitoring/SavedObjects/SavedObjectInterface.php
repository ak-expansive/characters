<?php

namespace Contexts\Monitoring\SavedObjects;

interface SavedObjectInterface
{
    public function toApiBody(): array;
}
