<?php

namespace Contexts\Monitoring\SavedObjects\Searches;

use Contexts\Monitoring\SavedObjects\SavedObjectInterface;

class NGiNXLogs implements SavedObjectInterface
{
    /**
     * Hardcoded to make it easier between restarts of dev env
     *
     * @TODO: Find a way to remove hardcoded ID's
     */
    public const ID = '5fccd990-bdf1-11ea-bb4b-eb695fed91c2';

    public function toApiBody(): array
    {
        return [
            "attributes"=> [
                "title"=> "Characters - NGiNX Logs",
                "description"=> "Finds all logs from nginx containers for the Characters service.",
                "hits"=> 0,
                "columns"=> [
                    "_source",
                ],
                "sort"=> [],
                "version"=> 1,
                "kibanaSavedObjectMeta"=> [
                    "searchSourceJSON"=> "{\"highlightAll\":true,\"version\":true,\"query\":{\"query\":\"kubernetes.namespace_name: " . config('monitoring.kubernetes.namespace') . " AND kubernetes.container_name: nginx\",\"language\":\"lucene\"},\"indexRefName\":\"kibanaSavedObjectMeta.searchSourceJSON.index\",\"filter\":[]}",
                ],
            ],
            "references" => [
                [
                    "type"=> "index-pattern",
                    "name"=> "kibanaSavedObjectMeta.searchSourceJSON.index",
                    "id"=> config('monitoring.kibana.indexPatternID'),
                ],
            ],
        ];
    }
}
