<?php

namespace Contexts\Monitoring\SavedObjects\Dashboards;

use Contexts\Monitoring\SavedObjects\SavedObjectInterface;
use Contexts\Monitoring\SavedObjects\Searches\NGiNXLogs;

class Overview implements SavedObjectInterface
{
    /**
     * Hardcoded to make it easier between restarts of dev env
     *
     * @TODO: Find a way to remove hardcoded ID's
     */
    public const ID = '88942680-bdf1-11ea-bb4b-eb695fed91c2';

    public const PANEL_0_ID = 'd8a65d7f-717d-40d9-9fee-5c0d3f46fee9';

    public function toApiBody(): array
    {
        return [
            "attributes" => [
                "title" => "Characters Overview",
                "description" => "Overview of the Characters service.",
                "panelsJSON" => "[{\"version\":\"7.8.0\",\"gridData\":{\"x\":0,\"y\":0,\"w\":24,\"h\":15,\"i\":\"" . self::PANEL_0_ID . "\"},\"panelIndex\":\"" . self::PANEL_0_ID . "\",\"embeddableConfig\":{},\"panelRefName\":\"panel_0\"}]",
                "optionsJSON" => "{\"useMargins\":true,\"hidePanelTitles\":false}",
                "version" => 1,
                "timeRestore" => false,
                "kibanaSavedObjectMeta" => [
                    "searchSourceJSON" => "{\"query\":{\"query\":\"\",\"language\":\"kuery\"},\"filter\":[]}",
                ],
            ],
            "references" => [
                [
                    "name" => "panel_0",
                    "type" => "search",
                    "id" => NGiNXLogs::ID,
                ],
            ],
        ];
    }
}
