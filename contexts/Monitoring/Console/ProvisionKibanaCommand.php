<?php

namespace Contexts\Monitoring\Console;

use Contexts\Monitoring\Client\Kibana\ClientInterface;
use Contexts\Monitoring\SavedObjects\Dashboards\Overview;
use Contexts\Monitoring\SavedObjects\Searches\NGiNXLogs;
use Illuminate\Console\Command;

class ProvisionKibanaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitoring:kibana:provision';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates necessary Kibana resources for viewing logs.';

    protected ClientInterface $client;

    /**
     * Create a new command instance.
     *
     * @param Generator            $generator
     * @param ConfigurationBuilder $configBuilder
     * @param Runner               $runner
     * @param ClientInterface      $client
     *
     * @return void
     */
    public function __construct(ClientInterface $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $isReady = $this->waitForReady();

        if (!$isReady) {
            $this->error('Timed out waiting for Kibana using - ' . implode(', ', $this->getUriDetails()));

            return 1;
        }

        $search = new NGiNXLogs;
        $createdSearch = $this->client->createSearch($search->toApiBody(), NGiNXLogs::ID, true);

        if ($createdSearch) {
            $this->info("Created NGiNX Logs search.");
        } else {
            $this->error("Failed to create NGiNX logs search.");
            return 1;
        }

        $dashboard = new Overview;
        $createdDashboard = $this->client->createDashboard($dashboard->toApiBody(), Overview::ID, true);

        if ($createdDashboard) {
            $this->info("Created Overview dashboard.");
        } else {
            $this->error("Failed to create Overview dashboard");
            return 1;
        }
    }

    protected function getUriDetails(): array
    {
        $uriConfig = $this->client->getConfig()['base_uri'];

        $host = $uriConfig->getHost();
        $path = $uriConfig->getPath();
        $scheme = $uriConfig->getScheme();
        $port = $uriConfig->getPort();

        $uriKV = compact('host', 'scheme', 'path', 'port');

        $uriDetails = array_map(function ($v, string $k) {
            return "$k: '" . ($v ?? 'n/a') . "'";
        }, $uriKV, array_keys($uriKV));

        return $uriDetails;
    }

    protected function waitForReady(): bool
    {
        $attemptNumber = 0;
        $limit = 5;

        while (!$this->client->isReady()) {
            $attemptNumber++;
            if ($attemptNumber >= $limit) {
                return false;
            }
            $this->warn('Failed to connect to Kibana, retrying...');
            sleep(30);
        }

        return true;
    }
}
