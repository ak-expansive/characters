<?php

namespace Contexts\Monitoring\Providers;

use Contexts\Core\Exceptions\Handler;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\ServiceProvider;
use Contexts\Monitoring\Client\Kibana\Client;
use Contexts\Monitoring\Client\Kibana\ClientInterface;
use Contexts\Monitoring\Console\ProvisionKibanaCommand;

class MainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ClientInterface::class, function ($app) {
            $guzzleClient = new GuzzleClient([
                'base_uri' => config('monitoring.kibana.host'),

                'timeout' => 30,

                'headers' => [
                    'Content-Type' => 'application/json',
                    'kbn-xsrf' => 'true',
                ],
            ]);

            return new Client(
                $guzzleClient,
                $app->make(Handler::class)
            );
        });
    }

    public function boot()
    {
        $this->commands([
            ProvisionKibanaCommand::class,
        ]);
    }
}
