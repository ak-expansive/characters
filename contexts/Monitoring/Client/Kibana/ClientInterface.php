<?php

namespace Contexts\Monitoring\Client\Kibana;

interface ClientInterface
{
    public function createDashboard(array $body, ?string $id = null, $overwrite = true): bool;

    public function createSearch(array $body, ?string $id = null, $overwrite = true): bool;

    public function isReady(): bool;

    public function getConfig(): array;
}
