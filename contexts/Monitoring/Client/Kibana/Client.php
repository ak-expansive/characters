<?php

namespace Contexts\Monitoring\Client\Kibana;

use Contexts\Core\Exceptions\Handler;
use GuzzleHttp\ClientInterface as HttpClientInterface;

class Client implements ClientInterface
{
    public const STATUS_URI = '/api/status';

    public const SAVED_OBJECT_TYPE_DASHBOARD='dashboard';

    public const SAVED_OBJECT_TYPE_SEARCH='search';

    protected HttpClientInterface $client;

    protected ?Handler $exceptionHandler = null;

    public function __construct(HttpClientInterface $client, ?Handler $exceptionHandler = null)
    {
        $this->client = $client;
        $this->exceptionHandler = $exceptionHandler;
    }

    public function createDashboard(array $body, ?string $id = null, $overwrite = true): bool
    {
        return $this->createSavedObject(self::SAVED_OBJECT_TYPE_DASHBOARD, $body, $id, $overwrite);
    }

    public function createSearch(array $body, ?string $id = null, $overwrite = true): bool
    {
        return $this->createSavedObject(self::SAVED_OBJECT_TYPE_SEARCH, $body, $id, $overwrite);
    }

    public function isReady(): bool
    {
        try {
            $response = $this->client->request('GET', self::STATUS_URI);
        } catch (\Throwable $e) {
            $this->reportException($e);
            return false;
        }

        return $response->getStatusCode() == 200;
    }

    public function getConfig(): array
    {
        return $this->client->getConfig();
    }

    protected function createSavedObject(string $type, array $body, ?string $id = null, $overwrite = true): bool
    {
        $uri = "/api/saved_objects/$type";

        if ($id !== null) {
            $uri .= "/$id";
        }

        if ($overwrite) {
            $uri .= "?overwrite=true";
        }

        try {
            $response = $this->client->request(
                'post',
                $uri,
                ['json' => $body]
            );
        } catch (\Throwable $e) {
            $this->reportException($e);

            return false;
        }

        return $response->getStatusCode() == 200;
    }

    protected function reportException(\Throwable $e)
    {
        if ($this->exceptionHandler === null) {
            return;
        }

        $this->exceptionHandler->report($e);
    }
}
