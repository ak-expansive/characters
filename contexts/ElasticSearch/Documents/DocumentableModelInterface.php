<?php

namespace Contexts\ElasticSearch\Documents;

interface DocumentableModelInterface
{
    public function toDocument(): IdentifiableDocument;

    public static function fromDocument(IdentifiableDocument $document): self;
}
