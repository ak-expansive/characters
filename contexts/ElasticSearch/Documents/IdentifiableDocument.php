<?php

namespace Contexts\ElasticSearch\Documents;

use InvalidArgumentException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class IdentifiableDocument
{
    protected PropertyAccessor $propertyAccessor;

    protected string $id;

    protected string $type;

    protected array $body;

    protected function __construct(string $id, array $body)
    {
        $this->id = $id;
        $this->body = $body;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->getPropertyAccessor();
    }

    public static function withBody(string $id, array $body = []): self
    {
        return new self($id, $body);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function get(string $propertyPath, bool $throwOnMissing = true)
    {
        if (!$this->propertyAccessor->isReadable($this->body, $propertyPath)) {
            if ($throwOnMissing) {
                throw new InvalidArgumentException("$propertyPath not found in document $this->id");
            }

            return null;
        }

        return $this->propertyAccessor->getValue($this->body, $propertyPath);
    }

    public function getBody(): array
    {
        return $this->body;
    }
}
