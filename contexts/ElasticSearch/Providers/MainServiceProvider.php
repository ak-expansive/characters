<?php

namespace Contexts\ElasticSearch\Providers;

use Contexts\ElasticSearch\Console\CreateIndicesCommand;
use Contexts\ElasticSearch\Console\DeleteIndicesCommand;
use Contexts\ElasticSearch\Console\TruncateIndicesCommand;
use Contexts\ElasticSearch\Console\UpdateIndicesMappingsCommand;
use Contexts\ElasticSearch\Repository\Meta\Finder\MetaFinderProvider;
use Contexts\ElasticSearch\Repository\Manager;
use Contexts\ElasticSearch\Repository\Meta\MetaRepository;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Illuminate\Log\Logger;
use Illuminate\Support\ServiceProvider;

class MainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $esConfig = config('elasticsearch');

        $this->app->singleton(MetaRepository::class, function () use ($esConfig) {
            $metaList = $esConfig['repositories'] ?? [];

            $provider = $esConfig['MetaFinderProvider'] ?? null;

            if ($provider === null) {
                return MetaRepository::fromArray($metaList);
            }

            if (!$provider instanceof MetaFinderProvider) {
                throw new \RuntimeException('Not an instance of finder provider');
            }

            $finder = $provider->getFinder();


            foreach ($finder as $filepath) {
                $config = require_once $filepath;
                $metaList[] = $config;
            }


            return MetaRepository::fromArray($metaList);
        });

        $this->app->singleton(Client::class, function ($app) use ($esConfig) {
            $config = $esConfig['connection'];

            if ($this->app->environment('local') && !$this->app->runningInConsole()) {
                $config['logger'] = $app->make(Logger::class);
            }

            return ClientBuilder::fromConfig($config);
        });

        $this->app->singleton(Manager::class, function ($app) {
            return new Manager(
                $app->make(Client::class),
                $app->make(MetaRepository::class)
            );
        });
    }

    public function boot()
    {
        $this->commands([
            CreateIndicesCommand::class,
            DeleteIndicesCommand::class,
            UpdateIndicesMappingsCommand::class,
            TruncateIndicesCommand::class,
        ]);
    }
}
