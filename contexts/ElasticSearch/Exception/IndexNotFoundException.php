<?php

namespace Contexts\ElasticSearch\Exception;

use RuntimeException;

class IndexNotFoundException extends RuntimeException
{
    public static function forIndex(string $name): self
    {
        return new self("Index not found: $name");
    }
}
