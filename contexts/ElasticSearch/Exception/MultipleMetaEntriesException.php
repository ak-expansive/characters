<?php

namespace Contexts\ElasticSearch\Exception;

use RuntimeException;

class MultipleMetaEntriesException extends RuntimeException
{
    protected function __construct(string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function forModels(array $classes): self
    {
        return new self(
            "Found multiple meta entries for: " . implode(', ', $classes)
        );
    }
}
