<?php

namespace Contexts\ElasticSearch\Exception;

use Contexts\ElasticSearch\Repository\Meta\Meta;
use RuntimeException;

class CouldNotBuildMetaException extends RuntimeException
{
    protected function __construct(string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function fromIncorrectValueType($value): self
    {
        $valueType = gettype($value) == 'object' ? get_class($value) : gettype($value);

        return new self(
            "Could not build " . Meta::class . " from value of type: $valueType"
        );
    }

    public static function forBadIndexName(string $message)
    {
        return new self(
            "The index name is invalid: $message"
        );
    }

    public static function forMissingArrayKeys(array $missingKeys): self
    {
        return new self(
            'The following keys must be provided: ' . implode(', ', $missingKeys)
        );
    }

    public static function forMissingImplementation(string $className, string $shouldImplement): self
    {
        return new self(
            "Class '$className' does not implement '$shouldImplement'"
        );
    }

    public static function forMissingClass(string $className): self
    {
        return new self(
            "Class '$className' does not exist."
        );
    }
}
