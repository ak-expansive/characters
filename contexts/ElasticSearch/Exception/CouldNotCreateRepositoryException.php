<?php

namespace Contexts\ElasticSearch\Exception;

use RuntimeException;

class CouldNotCreateRepositoryException extends RuntimeException
{
    protected function __construct(string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function forMissingModelImplementation(string $class, string $shouldImplement): self
    {
        return new self(
            "Model '$class' did not implement the required interface: '$shouldImplement'"
        );
    }

    public static function forMissingRepositoryImplementation(string $class, string $shouldImplement): self
    {
        return new self(
            "Repository '$class' did not implement the required interface: '$shouldImplement'"
        );
    }
}
