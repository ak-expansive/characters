<?php

namespace Contexts\ElasticSearch\Exception;

use RuntimeException;

class QueryResultException extends RuntimeException
{
    public static function forSettingCollectionResultsAfterSingular(): self
    {
        return new self(
            "Cannot set a collection of results when a singular is already set."
        );
    }

    public static function forSettingSingularResultAfterCollection(): self
    {
        return new self(
            "Cannot set a singular result when a collection is already set."
        );
    }
}
