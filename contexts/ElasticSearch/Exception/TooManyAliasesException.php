<?php

namespace Contexts\ElasticSearch\Exception;

use RuntimeException;

class TooManyAliasesException extends RuntimeException
{
    protected function __construct(string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function forModel(string $class, array $aliases): self
    {
        return new self(
            "Found multiple aliases (" . implode(', ', $aliases) . ") for model: $class"
        );
    }
}
