<?php

namespace Contexts\ElasticSearch\Repository;

use RuntimeException;
use Contexts\ElasticSearch\Exception\QueryResultException;

class QueryResult
{
    protected ?int $page = null;

    protected ?int $pageSize = null;

    protected ?int $indexTotal = null;

    protected $singularResult = null;

    protected ?array $resultCollection = null;

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getPage(): ?int
    {
        return $this->page;
    }

    public function setPageSize(int $pageSize): self
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    public function getPageSize(): ?int
    {
        return $this->pageSize;
    }

    public function setIndexTotal(int $total): self
    {
        $this->indexTotal = $total;

        return $this;
    }

    public function getTotalPages(): ?int
    {
        if ($this->indexTotal === null || $this->pageSize === null) {
            throw new RuntimeException("Total pages cannot be calculated without indexTotal and pageSize");
        }

        return ceil($this->indexTotal / $this->pageSize);
    }

    public function getIndexTotal(): ?int
    {
        return $this->indexTotal;
    }

    public function setSingularResult($result): self
    {
        if ($this->resultCollection !== null) {
            throw QueryResultException::forSettingSingularResultAfterCollection();
        }

        $this->singularResult = $result;

        return $this;
    }

    public function getSingularResult()
    {
        return $this->singularResult;
    }

    public function setResultCollection(...$result): self
    {
        if ($this->singularResult !== null) {
            throw QueryResultException::forSettingCollectionResultsAfterSingular();
        }

        $this->resultCollection = $result;

        return $this;
    }

    public function getResultCollection(): ?array
    {
        return $this->resultCollection;
    }
}
