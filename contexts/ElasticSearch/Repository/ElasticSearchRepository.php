<?php

namespace Contexts\ElasticSearch\Repository;

use Contexts\ElasticSearch\Repository\Meta\Meta;
use Elasticsearch\Client;

interface ElasticSearchRepository
{
    public function __construct(Client $client, Meta $meta);

    public function createIndex();

    public function deleteIndex();

    public function truncate();

    public function updateMappings();
}
