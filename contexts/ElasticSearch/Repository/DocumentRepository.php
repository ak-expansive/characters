<?php

namespace Contexts\ElasticSearch\Repository;

use Contexts\ElasticSearch\Documents\DocumentableModelInterface;
use Contexts\ElasticSearch\Documents\IdentifiableDocument;
use Contexts\ElasticSearch\Exception\IndexNotFoundException;
use Contexts\ElasticSearch\Repository\Meta\Meta;
use Elasticsearch\Client;

abstract class DocumentRepository implements ElasticSearchRepository
{
    protected Client $client;

    protected Meta $meta;

    public function __construct(Client $client, Meta $meta)
    {
        $this->client = $client;
        $this->meta = $meta;
    }

    public function saveOne(DocumentableModelInterface $model)
    {
        $identifiableDocument = $model->toDocument();


        $this->client->index(
            [
                'index' => $this->getIndexName(),
                'id' => $identifiableDocument->getId(),
                'body' => $identifiableDocument->getBody(),
                'refresh' => 'wait_for',
            ]
        );
    }

    public function deleteOne(DocumentableModelInterface $model)
    {
        $identifiableDocument = $model->toDocument();

        $this->client->delete(
            [
                'index' => $this->getIndexName(),
                'id' => $identifiableDocument->getId(),
            ]
        );
    }

    public function createIndex()
    {
        $indexName = $this->getIndexName();

        if ($this->client->indices()->exists(['index' => $indexName])) {
            return;
        }

        $params = [
            'index' => $indexName,
            'body' => [],
        ];

        $settings = $this->getIndexSettings();
        $mappings = $this->getIndexMappings();
        $aliases = $this->getIndexAliases();

        if ($settings !== null) {
            $params['body']['settings'] = $settings;
        }

        if ($mappings !== null) {
            $params['body']['mappings'] = $mappings;
        }

        if ($aliases !== null) {
            $params['body']['aliases'] = $aliases;
        }

        return $this->client->indices()->create($params);
    }

    public function updateMappings()
    {
        $indexName = $this->getIndexName();

        if (!$this->client->indices()->exists(['index' => $indexName])) {
            throw IndexNotFoundException::forIndex($indexName);
        }

        $params = [
            'index' => $indexName,
            'body' => [],
        ];

        $mappings = $this->getIndexMappings();

        if ($mappings !== null) {
            // Probably throw an exception if they're null
            // Or would this be a valid usecase i.e. get rid of mappings altogether
            $params['body'] = $mappings;
        }

        return $this->client->indices()->putMapping($params);
    }

    public function truncate()
    {
        $this->deleteIndex();

        return $this->createIndex();
    }

    public function deleteIndex()
    {
        $indexName = $this->getIndexName();

        if (!$this->client->indices()->exists(['index' => $indexName])) {
            throw IndexNotFoundException::forIndex($indexName);
        }

        $params = [
            'index' => $indexName,
        ];

        return $this->client->indices()->delete($params);
    }

    public function getIndexName(): string
    {
        return $this->meta->getIndexName();
    }

    public function getIndexMappings(): ?array
    {
        return $this->meta->getMappings();
    }

    public function getIndexSettings(): ?array
    {
        return $this->meta->getSettings();
    }

    public function getIndexAliases(): ?array
    {
        return $this->meta->getAliases();
    }

    protected function extractHits(array $result): array
    {
        return $result['hits']['hits'] ?? [];
    }

    protected function hydrateResults(array $result)
    {
        $hits = $this->extractHits($result);

        if (empty($hits)) {
            return [];
        }

        $documents = array_map(function ($hit) {
            return IdentifiableDocument::withBody($hit['_id'], $hit['_source']);
        }, $hits);

        return array_map([$this->meta->getModelClass(), 'fromDocument'], $documents);
    }
}
