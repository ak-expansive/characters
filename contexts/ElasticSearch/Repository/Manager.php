<?php

namespace Contexts\ElasticSearch\Repository;

use Contexts\ElasticSearch\Exception\CouldNotCreateRepositoryException;
use Contexts\ElasticSearch\Repository\Meta\MetaRepository;
use Elasticsearch\Client;

class Manager
{
    protected Client $client;

    protected array $aliases;

    protected MetaRepository $metaRepo;

    public function __construct(Client $client, MetaRepository $metaRepo)
    {
        $this->client = $client;
        $this->metaRepo = $metaRepo;
    }

    public function get(string $class): ElasticSearchRepository
    {
        $meta = $this->metaRepo->byModel($class);
        $repositoryClass = $meta->getRepositoryClass();

        $this->guardImplementsQueryRepository($repositoryClass);

        return new $repositoryClass(
            $this->client,
            $meta
        );
    }

    protected function guardImplementsQueryRepository(string $class): void
    {
        $implements = class_implements($class);

        if (!in_array(ElasticSearchRepository::class, $implements)) {
            throw CouldNotCreateRepositoryException::forMissingRepositoryImplementation(
                $class,
                ElasticSearchRepository::class
            );
        }
    }
}
