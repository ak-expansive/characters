<?php

namespace Contexts\ElasticSearch\Repository\Meta;

use Contexts\ElasticSearch\Exception\CouldNotBuildMetaException;
use Contexts\ElasticSearch\Exception\MultipleMetaEntriesException;

class MetaRepository
{
    protected array $metaList;

    /*
     * Seems a little odd but this allows me to override this property when testing
     * It seems like the nicest way around mocking static methods
    */
    protected static $metaFactoryMethod = [Meta::class, 'fromArray'];

    protected function __construct(array $metaList)
    {
        $keys = array_map(function (Meta $meta) {
            return $meta->getModelClass();
        }, $metaList);

        $this->metaList = array_combine(
            $keys,
            array_values($metaList)
        );
    }

    public static function setMetaFactoryMethod(callable $callable)
    {
        return static::$metaFactoryMethod = $callable;
    }

    public static function fromArray(array $configs): self
    {
        $metaList = array_map(function ($config) {
            if ($config instanceof Meta) {
                return $config;
            }

            if (is_array($config)) {
                return call_user_func(static::$metaFactoryMethod, $config);
            }

            throw CouldNotBuildMetaException::fromIncorrectValueType($config);
        }, $configs);

        self::guardUniqueModels($metaList);

        return new static($metaList);
    }

    public function byModel(string $modelClass): ? Meta
    {
        if (!isset($this->metaList[$modelClass])) {
            throw new \RuntimeException('Meta not found');
        }

        return $this->metaList[$modelClass];
    }

    public function all(): array
    {
        return array_values($this->metaList);
    }

    protected static function guardUniqueModels(array $metaList)
    {
        $modelClasses = array_map(function (Meta $meta) {
            return $meta->getModelClass();
        }, $metaList);

        $counts = array_count_values($modelClasses);

        $nonUniqueEntries = array_filter($counts, function ($value) {
            return $value > 1;
        });

        if (count($nonUniqueEntries) > 0) {
            throw MultipleMetaEntriesException::forModels(array_keys($nonUniqueEntries));
        }
    }
}
