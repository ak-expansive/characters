<?php

namespace Contexts\ElasticSearch\Repository\Meta\Finder;

use Symfony\Component\Finder\Finder;

class DefaultMetaFinderProvider implements MetaFinderProvider
{
    protected string $rootDir;

    protected function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;
    }

    public static function make(string $rootDir): self
    {
        return new self($rootDir);
    }

    public function getFinder(): Finder
    {
        $finder = new Finder;

        return $finder->name("*.es.meta.php")
            ->in($this->rootDir);
    }
}
