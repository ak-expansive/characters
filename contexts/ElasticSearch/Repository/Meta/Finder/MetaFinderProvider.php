<?php

namespace Contexts\ElasticSearch\Repository\Meta\Finder;

use Symfony\Component\Finder\Finder;

interface MetaFinderProvider
{
    public function getFinder(): Finder;
}
