<?php

namespace Contexts\ElasticSearch\Repository\Meta;

use Contexts\ElasticSearch\Exception\InvalidMetaException;
use Contexts\ElasticSearch\Repository\ElasticSearchRepository;
use Contexts\ElasticSearch\Exception\CouldNotBuildMetaException;

class Meta
{
    protected string $modelClass;

    protected string $repositoryClass;

    protected string $indexName;

    protected ?array $settings;

    protected ?array $aliases;

    protected ?array $mappings;

    protected ?array $extra;

    protected function __construct(
        string $modelClass,
        string $repositoryClass,
        string $indexName,
        ?array $settings = null,
        ?array $aliases = null,
        ?array $mappings = null,
        ?array $extra = null
    ) {
        $this->modelClass = $modelClass;
        $this->repositoryClass = $repositoryClass;
        $this->indexName = $indexName;
        $this->settings = $settings;
        $this->aliases = $aliases;
        $this->mappings = $mappings;
        $this->extra = $extra;
    }

    public function getModelClass(): string
    {
        return $this->modelClass;
    }

    public function getRepositoryClass(): string
    {
        return $this->repositoryClass;
    }

    public function getIndexName(): string
    {
        return $this->indexName;
    }

    public function getMappings(): ?array
    {
        return $this->mappings;
    }

    public function getAliases(): ?array
    {
        return $this->aliases;
    }

    public function getSettings(): ?array
    {
        return $this->settings;
    }

    public function getExtras(): ?array
    {
        return $this->extra;
    }

    public function getExtra(string $key, $default = null)
    {
        return $this->extra[$key] ?? $default;
    }

    public static function fromArray(array $config)
    {
        self::guardArrayConfig($config);

        return new self(
            $config['model'],
            $config['repository'],
            $config['indexName'],
            $config['settings'] ?? null,
            $config['aliases'] ?? null,
            $config['mappings'] ?? null,
            $config['extra'] ?? null
        );
    }

    protected static function guardArrayConfig(array $config)
    {
        $requiredKeys = ['model', 'repository', 'indexName'];
        $diff = array_diff($requiredKeys, array_keys($config));

        if (!empty($diff)) {
            throw CouldNotBuildMetaException::forMissingArrayKeys($diff);
        }

        self::guardIndexName($config['indexName']);
        self::guardModelClass($config['model']);
        self::guardRepositoryClass($config['repository']);
    }

    protected static function guardIndexName($indexName)
    {
        if (empty($indexName)) {
            throw CouldNotBuildMetaException::forBadIndexName(
                "no value is set"
            );
        }

        if (! is_string($indexName)) {
            throw CouldNotBuildMetaException::forBadIndexName(
                "value is not a string"
            );
        }
    }

    protected static function guardModelClass(string $modelClass)
    {
        if (! class_exists($modelClass)) {
            throw CouldNotBuildMetaException::forMissingClass(
                $modelClass
            );
        }
    }

    protected static function guardRepositoryClass(string $repositoryClass)
    {
        if (! class_exists($repositoryClass)) {
            throw CouldNotBuildMetaException::forMissingClass(
                $repositoryClass
            );
        }

        $repositoryImplements = class_implements($repositoryClass);
        $mustImplement = ElasticSearchRepository::class;

        if (!in_array($mustImplement, $repositoryImplements)) {
            throw CouldNotBuildMetaException::forMissingImplementation(
                $repositoryClass,
                $mustImplement
            );
        }
    }
}
