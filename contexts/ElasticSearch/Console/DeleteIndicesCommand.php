<?php

namespace Contexts\ElasticSearch\Console;

use Contexts\ElasticSearch\Repository\Manager;
use Contexts\ElasticSearch\Repository\Meta\MetaRepository;
use Illuminate\Console\Command;

class DeleteIndicesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'es:indices:delete {--for=} {--index=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes an index all indices.';

    protected Manager $manager;

    protected MetaRepository $metaRepo;

    /**
     * Create a new command instance.
     *
     * @param Generator            $generator
     * @param ConfigurationBuilder $configBuilder
     * @param Runner               $runner
     * @param Manager              $manager
     * @param MetaRepository       $metaRepo
     *
     * @return void
     */
    public function __construct(Manager $manager, MetaRepository $metaRepo)
    {
        parent::__construct();

        $this->manager = $manager;
        $this->metaRepo = $metaRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = $this->option('for');

        if ($model !== null) {
            $this->doDelete($model, $this->option('index'));
            return;
        }

        if ($this->option('index') !== null) {
            $this->error("The 'index' option can only be used in conjunction with 'for' option!");
            return 1;
        }

        $this->info("Deleting indices for all registered models...");

        foreach ($this->metaRepo->all() as $meta) {
            $this->doDelete($meta->getModelClass());
        }
    }

    protected function doDelete(string $model, ?string $index = null)
    {
        $this->info("Retrieving Repository for: $model");
        $repo = $this->manager->get($model);

        $this->info("Using repository: " . get_class($repo));
        $this->info(
            sprintf(
                "Deleting index%s...",
                $index !== null ? " (using name: $index)" : ''
            )
        );

        $result = $repo->deleteIndex($index);
        $this->info("Index deleted successfully.");
    }
}
