<?php

namespace Contexts\Healthz\Helpers;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ElasticSearchClientBuilder
{
    /**
     * Builds the Predis client object.
     *
     * Just abstracts away the instantiation from the Check itself.
     * Makes it alot easier to separate the External requirements from the logic.
     *
     * @param string $dsn
     * @param array  $config
     *
     * @return Client
     */
    public function build(array $config): ?Client
    {
        return ClientBuilder::fromConfig($config);
    }
}
