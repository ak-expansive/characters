<?php

namespace Contexts\Healthz\Helpers;

use Contexts\Healthz\Check\Result\ResultInterface;
use Contexts\Healthz\Exception\NotAResultException;

class ResponseBuilder
{
    /**
     * @var array
     */
    protected $response;

    /**
     * @var ResultInterface[]
     */
    protected $results;

    /**
     * @param array $results
     */
    public function __construct(array $results)
    {
        $this->guardResults($results);

        $this->results = $results;
    }

    /**
     * Build a response based upon the results in the constructor and add the metadata
     * if not empty.
     *
     * @param array $meta
     *
     * @return array
     */
    public function build(array $meta): array
    {
        $response = array_map(
            function (ResultInterface $result) {
                return ['healthy' => $result->wasSuccessful(), 'errorsDuring' => array_column($result->errors(), 'phase')];
            },
            $this->results
        );

        if (!empty($meta)) {
            $response['_meta'] = $meta;
        }

        return $response;
    }

    /**
     * Returns a boolean whether the results were all successful.
     * Should be called statically to avoid any confusion
     *
     * @param array $response
     *
     * @return bool
     */
    public function wasSuccessful(): bool
    {
        $response = $this->build([]);

        return array_reduce(
            array_column($response, 'healthy'),
            function ($carry, $next) {
                return $carry && $next;
            },
            true
        );
    }

    /**
     * If all the elements of the array supplied do not match the required interface
     * further methods within here will fail. We should throw an exception as early
     * as possible in the process.
     *
     * @param array $results
     *
     * @return void
     */
    protected function guardResults(array $results): void
    {
        foreach ($results as $result) {
            if ($result instanceof ResultInterface) {
                continue;
            }

            throw new NotAResultException(
                sprintf(
                    "Expected instance of %s, got %s.",
                    ResultInterface::class,
                    gettype($result) == "object" ? get_class($result) : gettype($result)
                )
            );
        }
    }
}
