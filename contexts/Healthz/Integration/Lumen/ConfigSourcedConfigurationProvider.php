<?php

namespace Contexts\Healthz\Integration\Lumen;

use Contexts\Healthz\Check\Configuration\ConfigurationBuilder;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Exception\InvalidConfigurationException;
use Contexts\Healthz\Integration\ConfigurationProviderInterface;

class ConfigSourcedConfigurationProvider implements ConfigurationProviderInterface
{
    /**
     * @param array $arguments
     *
     * @return ConfigurationInterface
     */
    public function retrieve(array $arguments = []): ConfigurationInterface
    {
        $configKey = $arguments['key'] ?? null;
        $configType = $arguments['type'] ?? null;

        if ($configKey === null || $configType === null) {
            throw new InvalidConfigurationException("You must supply 'key' and 'type' arguments.");
        }

        $config = config($configKey);

        if ($config === null) {
            throw new InvalidConfigurationException("There is no config entry for key: '$configKey'");
        }

        if (array_key_exists('overrides', $arguments)) {
            $config = $this->applyOverrides($config, $arguments['overrides']);
        }

        $configBuilder = new ConfigurationBuilder;

        return $configBuilder->build($configType, $config);
    }

    protected function applyOverrides($config, $overrides)
    {
        if (!is_array($config)  || !is_array($overrides)) {
            throw new InvalidConfigurationException("Can only provide overrides for array values.");
        }

        return array_replace($config, $overrides);
    }
}
