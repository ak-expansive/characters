<?php

namespace Contexts\Healthz\Integration\Lumen;

use Contexts\Healthz\Check\Configuration\ConfigurationBuilder;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Check\Configuration\RedisConfiguration;
use Contexts\Healthz\Exception\InvalidConnectionException;
use Contexts\Healthz\Integration\ConfigurationProviderInterface;

class RedisConfigurationProvider implements ConfigurationProviderInterface
{
    /*
     * Retrieve the Configuration from the laravel config.
     * Using the laravel database.php config we extract the relevant credentials
     * for connecting to the Redis database.
     *
     * There is 1 acceptable argument, 'connection' which defines which of the
     * connections configured in database.php are used for the check. The arguments
     * passed here should be defined in the healthz config.
     */
    public function retrieve(array $arguments = []): ConfigurationInterface
    {
        if ($arguments['cluster'] ?? false === true) {
            $connection = $arguments['connection'] ?? 'default';
            $index = $arguments['connectionIndex'] ?? 0;
            $redisParams = config('database.redis.clusters.' . $connection)[$index];
        } else {
            $connection = $arguments['connection'] ?? config('database.redis.default');
            $redisParams = config('database.redis.' . $connection);
        }

        if ($redisParams === null) {
            throw new InvalidConnectionException("There is no redis connection called '" . $connection . "'.");
        }

        $configBuilder = new ConfigurationBuilder;

        return $configBuilder->build(RedisConfiguration::class, $redisParams);
    }
}
