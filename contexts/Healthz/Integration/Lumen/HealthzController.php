<?php

namespace Contexts\Healthz\Integration\Lumen;

use Contexts\Healthz\CheckSuiteRunner;
use Contexts\Healthz\Helpers\ResponseBuilder;
use Laravel\Lumen\Routing\Controller;

class HealthzController extends Controller
{
    public function __construct()
    {
        if (!empty(config('healthz.endpoint.middleware'))) {
            $this->middleware(config('healthz.endpoint.middleware'));
        }
    }

    /**
     * Returns a 200 response if all configured checks are successful.
     * Returns a 500 if any check fails.
     *
     * @param CheckSuiteRunner $checkSuiteRunner
     *
     * @return mixed
     */
    public function check(CheckSuiteRunner $checkSuiteRunner)
    {
        $config = config('healthz');

        $results = $checkSuiteRunner->run($config['suites']['endpoint']);

        $responseBuilder = new ResponseBuilder($results);
        $response = $responseBuilder->build(config('healthz.endpoint.meta', []));

        return response()->json(
            $response,
            $responseBuilder->wasSuccessful() ? 200 : 500
        );
    }
}
