<?php

use Contexts\Healthz\Integration\Lumen\HealthzController;

/*
 * Healthz route
 *
 * The route URI can be defined in the config but will default to '/_healthz'.
 */
$router->get('/' . config('healthz.endpoint.URI', '_healthz'), [
    'as' => 'healthz.check',
    'uses' => HealthzController::class . '@check',
]);
