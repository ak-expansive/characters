<?php

namespace Contexts\Healthz\Integration\Lumen;

use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    public function boot()
    {
        $this->app->router->group([], function ($router) {
            require __DIR__ . '/routes.php';
        });
    }
}
