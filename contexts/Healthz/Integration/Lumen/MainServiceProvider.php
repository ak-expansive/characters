<?php

namespace Contexts\Healthz\Integration\Lumen;

use Contexts\Healthz\Check\Configuration\ConfigurationBuilder;
use Contexts\Healthz\CheckResolver;
use Contexts\Healthz\CheckSuiteRunner;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class MainServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->commands([
            WaitForSuiteCommand::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*
         * This binding is provides alot of the core functionality for this package.
         * The CheckResolver is responsible for building the Check instances.
         *
         * Args:
         *  1: ConfigurationBuilder - handles building the configurations for the checks
         *  2: BuilderInterface[] - Objects responsible for building the checks you intend to use
         *  3: ConfigurationProviderInterface[] - Objects that can supply a config for a check
         */
        $this->app->bind(CheckResolver::class, function ($app) {
            $builders = array_map([$app, 'make'], config('healthz.builders'));
            $configurationProviders = array_map([$app, 'make'], config('healthz.configurationProviders'));
            return new CheckResolver(
                new ConfigurationBuilder,
                $builders,
                $configurationProviders
            );
        });

        $this->app->bind(CheckSuiteRunner::class, function ($app) {
            return new CheckSuiteRunner(
                $app->make(CheckResolver::class),
                config('healthz.checks') ?? []
            );
        });

        if ($this->app->runningInConsole()) {
            $this->app->when(config('healthz.builders'))
                ->needs(LoggerInterface::class)
                ->give(NullLogger::class);
        }
    }
}
