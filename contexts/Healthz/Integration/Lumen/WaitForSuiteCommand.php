<?php

namespace Contexts\Healthz\Integration\Lumen;

use Contexts\Healthz\CheckSuiteBlocker;
use Contexts\Healthz\WaitConfiguration;
use Illuminate\Console\Command;

class WaitForSuiteCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'healthz:waitfor:suite {suite} {--max=3} {--between=5} {--initialdelay=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Waits for a suite to have a successful run or fails.';

    protected CheckSuiteBlocker $checkSuiteBlocker;

    /**
     * Create a new command instance.
     *
     * @param Generator            $generator
     * @param ConfigurationBuilder $configBuilder
     * @param Runner               $runner
     * @param CheckSuiteBlocker    $checkSuiteBlocker
     *
     * @return void
     */
    public function __construct(CheckSuiteBlocker $checkSuiteBlocker)
    {
        parent::__construct();

        $this->checkSuiteBlocker = $checkSuiteBlocker;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $suite = config('healthz.suites.' . $this->argument('suite'));

        $waitConfig = new WaitConfiguration(
            $this->option('between'),
            $this->option('initialdelay'),
            $this->option('max'),
        );

        $outputCallback = function ($line) {
            $this->warn($line);
        };

        $this->checkSuiteBlocker->setOutputCallback($outputCallback);
        $result = $this->checkSuiteBlocker->wait($suite, $waitConfig, $outputCallback);

        if (!$result) {
            $this->error("Suite never succeeded!");
        } else {
            $this->info('Success!');
        }

        return $result ? 0 : 1;
    }
}
