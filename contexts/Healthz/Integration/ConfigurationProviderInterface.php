<?php

namespace Contexts\Healthz\Integration;

use Contexts\Healthz\Check\Configuration\ConfigurationInterface;

interface ConfigurationProviderInterface
{
    /**
     * Responsible for retrieving configuration values from the wider system
     * and placing them in an object implementing ConfigurationInterface.
     *
     * $arguments has a variable structure allowing new Providers to be added,
     * with minimal change.
     *
     * @var array
     *
     * @param array $arguments
     *
     * @return ConfigurationInterface
     */
    public function retrieve(array $arguments = []): ConfigurationInterface;
}
