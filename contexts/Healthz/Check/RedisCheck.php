<?php

namespace Contexts\Healthz\Check;

use Contexts\Healthz\Check\Builder\RedisBuilder;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Check\Configuration\RedisConfiguration;
use Contexts\Healthz\Check\Result\ResultBuilder;
use Contexts\Healthz\Check\Result\ResultInterface;
use Contexts\Healthz\Helpers\RedisClientBuilder;
use Psr\Log\LoggerInterface;

class RedisCheck extends AbstractCheck
{
    /**
     * The parts of the config we need to establish the connection.
     */
    public const REDIS_CONNECTION_PARTS = [
        "host",
        "port",
        "password",
        "timeout",
    ];

    /**
     * Builds a Redis connection allowing us to test the connection.
     *
     * The connection needs to be built during this check to ensure we maintain
     * control. If injecting this through a container (dependency injection), some
     * frameworks may have established (or failed to establish) a connection.
     * Building here ensures we haven't tried to connect (and subsequently bombed out)
     * somewhere in the container.
     *
     * @var RedisClientBuilder
     */
    protected $redisClientBuilder;

    /**
     * Constructor...
     *
     * @param ConfigurationInterface $config
     * @param ResultBuilder          $resultBuilder
     * @param RedisClientBuilder     $redisClientBuilder
     * @param null|LoggerInterface   $logger
     */
    public function __construct(
        ConfigurationInterface $config,
        ResultBuilder $resultBuilder,
        RedisClientBuilder $redisClientBuilder,
        ?LoggerInterface $logger = null
    ) {
        parent::__construct($config, $resultBuilder, $logger);

        $this->redisClientBuilder = $redisClientBuilder;
    }

    /**
     * {@inheritdoc}
     *
     * Step 1: Build a Redis client
     * Step 2: Attempt connection
     * Step 3:
     *            Catch exceptions and return
     *      OR    Continue to Step 4
     * Step 4: Try a ping
     * Step 5:
     *            Catch exceptions and return
     *      OR    All done, return result.
     */
    public function run(): ResultInterface
    {
        $result = $this->resultBuilder->build($this->config);

        try {
            $client = $this->redisClientBuilder->build(
                $this->getConnectionFromConfig()
            );
            $client->connect();
        } catch (\Throwable $e) {
            $this->log($e);
            $result->addError($e->getMessage(), 'connection');
            return $result;
        }

        try {
            $client->ping();
        } catch (\Throwable $e) {
            $this->log($e);
            $result->addError($e->getMessage(), 'ping');
            return $result;
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public static function requiredConfiguration(): string
    {
        return RedisConfiguration::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function requiredBuilder(): string
    {
        return RedisBuilder::class;
    }

    /**
     * Extract the keys from the config required to establish a connection.
     *
     * @return array
     */
    protected function getConnectionFromConfig(): array
    {
        $connDetails = [];

        foreach (self::REDIS_CONNECTION_PARTS as $part) {
            $connDetails[$part] = $this->config->get($part);
        }

        return $connDetails;
    }
}
