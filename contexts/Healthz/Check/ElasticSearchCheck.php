<?php

namespace Contexts\Healthz\Check;

use Contexts\Healthz\Check\Builder\ElasticSearchBuilder;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Check\Configuration\ElasticSearchConfiguration;
use Contexts\Healthz\Check\Result\ResultBuilder;
use Contexts\Healthz\Check\Result\ResultInterface;
use Contexts\Healthz\Helpers\ElasticSearchClientBuilder;
use Contexts\Healthz\Helpers\PDOBuilder;
use PDO;
use Psr\Log\LoggerInterface;

class ElasticSearchCheck extends AbstractCheck
{
    /**
     * @var ElasticSearchClientBuilder
     */
    protected $esClientBuilder;

    /**
     * Constructor...
     *
     * @param ConfigurationInterface     $config
     * @param ResultBuilder              $resultBuilder
     * @param PDOBuilder                 $pdoBuilder
     * @param null|LoggerInterface       $logger
     * @param ElasticSearchClientBuilder $esClientBuilder
     */
    public function __construct(
        ConfigurationInterface $config,
        ResultBuilder $resultBuilder,
        ElasticSearchClientBuilder $esClientBuilder,
        ?LoggerInterface $logger = null
    ) {
        parent::__construct($config, $resultBuilder, $logger);

        $this->esClientBuilder = $esClientBuilder;
    }

    /**
     * {@inheritdoc}
     *
     * Basic test of MySQL connection...
     *
     * The default query is: 'SELECT 1;'
     *
     * Step 1: Build a PDO connection
     * Step 2: Connect using the MySQL creds in the config
     * Step 3:
     *            Catch any exceptions during this phase & return
     *      OR
     *            Move onto Step 4
     * Step 4: Run the configured 'query' against the connection
     * Step 5:
     *            Catch any exceptions during this phase & return
     *      OR
     *            All done, return the result.
     */
    public function run(): ResultInterface
    {
        $result = $this->resultBuilder->build($this->config);

        try {
            $client = $this->esClientBuilder->build($this->config->asArray());
        } catch (\Throwable $e) {
            $this->log($e);
            $result->addError($e->getMessage(), 'initialisation');
            return $result;
        }

        try {
            $client->ping();
        } catch (\Throwable $e) {
            $this->log($e);
            $result->addError($e->getMessage(), 'ping');
            return $result;
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public static function requiredConfiguration(): string
    {
        return ElasticSearchConfiguration::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function requiredBuilder(): string
    {
        return ElasticSearchBuilder::class;
    }
}
