<?php

namespace Contexts\Healthz\Check;

use Contexts\Healthz\Check\Builder\MysqlBuilder;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Check\Configuration\MysqlConfiguration;
use Contexts\Healthz\Check\Result\ResultBuilder;
use Contexts\Healthz\Check\Result\ResultInterface;
use Contexts\Healthz\Helpers\PDOBuilder;
use PDO;
use Psr\Log\LoggerInterface;

class MysqlCheck extends AbstractCheck
{
    /**
     * These are the connection options we'll extract for the DSN.
     *
     * @var array
     */
    public const MYSQL_DSN_PARTS = [
        'host',
        'dbname',
        'port',
        'unix_socket',
        'charset',
    ];

    /**
     * Builds a PDO connection allowing us to test the connection.
     *
     * The connection needs to be built during this check as the PDO library
     * automatically connects upon object instantiaion. What I'm saying is, we
     * can't rely on dependency injection for this; the MySQL connection would
     * already be established (or failed) before this class is executed.
     *
     * @var PDOBuilder
     */
    protected $pdoBuilder;

    /**
     * Constructor...
     *
     * @param ConfigurationInterface $config
     * @param ResultBuilder          $resultBuilder
     * @param PDOBuilder             $pdoBuilder
     * @param null|LoggerInterface   $logger
     */
    public function __construct(
        ConfigurationInterface $config,
        ResultBuilder $resultBuilder,
        PDOBuilder $pdoBuilder,
        ?LoggerInterface $logger = null
    ) {
        parent::__construct($config, $resultBuilder, $logger);

        $this->pdoBuilder = $pdoBuilder;
    }

    /**
     * {@inheritdoc}
     *
     * Basic test of MySQL connection...
     *
     * The default query is: 'SELECT 1;'
     *
     * Step 1: Build a PDO connection
     * Step 2: Connect using the MySQL creds in the config
     * Step 3:
     *            Catch any exceptions during this phase & return
     *      OR
     *            Move onto Step 4
     * Step 4: Run the configured 'query' against the connection
     * Step 5:
     *            Catch any exceptions during this phase & return
     *      OR
     *            All done, return the result.
     */
    public function run(): ResultInterface
    {
        $result = $this->resultBuilder->build($this->config);

        try {
            $pdo = $this->pdoBuilder->buildConnection(
                $this->buildDSN(),
                $this->config->get('user'),
                $this->config->get('pass'),
                [
                    PDO::ATTR_TIMEOUT => $this->config->get('timeout'),
                ]
            );
        } catch (\Throwable $e) {
            $this->log($e);
            $result->addError($e->getMessage(), 'connection');
            return $result;
        }

        try {
            $statement = $pdo->query($this->config->get('query'));
            $value = $statement->fetch();
        } catch (\Throwable $e) {
            $this->log($e);
            $result->addError($e->getMessage(), 'query');
            return $result;
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public static function requiredConfiguration(): string
    {
        return MysqlConfiguration::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function requiredBuilder(): string
    {
        return MysqlBuilder::class;
    }

    /**
     * Extracts the parts of the config strictly required for the MySQL connection.
     *
     * @return array
     */
    protected function buildConnectionDetails(): array
    {
        $connDetails = [];

        foreach (self::MYSQL_DSN_PARTS as $part) {
            $connDetails[$part] = $this->config->get($part);
        }

        return $connDetails;
    }

    /**
     * Builds a MySQL DSN connection string from the configuration.
     *
     * @return string
     */
    protected function buildDSN(): string
    {
        $connDetails = $this->buildConnectionDetails();

        $pairedKeyValues = [];
        foreach ($connDetails as $key => $value) {
            if ($value == null) {
                continue;
            }

            $pairedKeyValues[] = "$key=$value";
        }

        return "mysql:" . implode(";", $pairedKeyValues);
    }
}
