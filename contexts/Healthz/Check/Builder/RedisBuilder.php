<?php

namespace Contexts\Healthz\Check\Builder;

use Contexts\Healthz\Check\CheckInterface;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Check\RedisCheck;
use Contexts\Healthz\Check\Result\ResultBuilder;
use Contexts\Healthz\Helpers\RedisClientBuilder;
use Psr\Log\LoggerInterface;

class RedisBuilder implements BuilderInterface
{
    /**
     * ResultBuilder
     *
     * @var ResultBuilder
     */
    protected $resultBuilder;

    /**
     * @var null|LoggerInterface
     */
    protected $logger;

    public function __construct(ResultBuilder $resultBuilder, ?LoggerInterface $logger = null)
    {
        $this->resultBuilder = $resultBuilder;
        $this->logger = $logger;
    }

    public function build(ConfigurationInterface $config): CheckInterface
    {
        return new RedisCheck(
            $config,
            $this->resultBuilder,
            new RedisClientBuilder,
            $this->logger
        );
    }
}
