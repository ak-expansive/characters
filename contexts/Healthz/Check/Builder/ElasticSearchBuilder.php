<?php

namespace Contexts\Healthz\Check\Builder;

use Contexts\Healthz\Check\CheckInterface;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Check\ElasticSearchCheck;
use Contexts\Healthz\Check\Result\ResultBuilder;
use Contexts\Healthz\Helpers\ElasticSearchClientBuilder;
use Psr\Log\LoggerInterface;

class ElasticSearchBuilder implements BuilderInterface
{
    /**
     * @var ResultBuilder
     */
    protected $resultBuilder;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(ResultBuilder $resultBuilder, ?LoggerInterface $logger = null)
    {
        $this->resultBuilder = $resultBuilder;
        $this->logger = $logger;
    }

    public function build(ConfigurationInterface $config): CheckInterface
    {
        return new ElasticSearchCheck(
            $config,
            $this->resultBuilder,
            new ElasticSearchClientBuilder,
            $this->logger
        );
    }
}
