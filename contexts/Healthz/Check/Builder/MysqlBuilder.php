<?php

namespace Contexts\Healthz\Check\Builder;

use Contexts\Healthz\Check\CheckInterface;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Check\MysqlCheck;
use Contexts\Healthz\Check\Result\ResultBuilder;
use Contexts\Healthz\Helpers\PDOBuilder;
use Psr\Log\LoggerInterface;

class MysqlBuilder implements BuilderInterface
{
    /**
     * @var ResultBuilder
     */
    protected $resultBuilder;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(ResultBuilder $resultBuilder, ?LoggerInterface $logger = null)
    {
        $this->resultBuilder = $resultBuilder;
        $this->logger = $logger;
    }

    public function build(ConfigurationInterface $config): CheckInterface
    {
        return new MysqlCheck(
            $config,
            $this->resultBuilder,
            new PDOBuilder,
            $this->logger
        );
    }
}
