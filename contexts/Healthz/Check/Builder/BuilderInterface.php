<?php

namespace Contexts\Healthz\Check\Builder;

use Contexts\Healthz\Check\CheckInterface;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;

interface BuilderInterface
{
    public function build(ConfigurationInterface $config): CheckInterface;
}
