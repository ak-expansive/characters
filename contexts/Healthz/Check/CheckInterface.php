<?php

namespace Contexts\Healthz\Check;

use Contexts\Healthz\Check\Result\ResultInterface;

interface CheckInterface
{
    /**
     * This runs whatever check the object was designed to run.
     * This is the core logic for the check.
     * If this was a MySQL connection check, within here we would have code connecting to MySQL.
     * It would also catch any exceptions and add them as errors to the ResultBuilder.
     *
     * @return ResultInterface
     */
    public function run(): ResultInterface;

    /**
     * This will return a full class path to the configuration type needed for this check.
     * The Configuration should match the ConfigurationInterface class.
     * This configuration will be passed to the Check instance.
     * The Configuration itself should contain validation about what fields need to be present.
     *
     * From within this check you should have confidence that anything you need
     * was already ensured by the Configuration.
     *
     * @see Healthz\Check\Configuration\ConfigurationInterface
     *
     * @return string
     */
    public static function requiredConfiguration(): string;

    /**
     * This is the class that will build this check.
     * Any two checks are unlikely to require the same dependencies.
     * A Redis check will need a Redis Client, and a MySQL check will need something like PDO.
     * Due to this fact, we need a way to flexibly build the Check without
     * restricting dependencies.
     *
     * @see Healthz\Check\BuilderInterface
     *
     * @return string
     */
    public static function requiredBuilder(): string;
}
