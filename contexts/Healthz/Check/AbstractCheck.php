<?php

namespace Contexts\Healthz\Check;

use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Check\Result\ResultBuilder;
use Contexts\Healthz\Check\Result\ResultInterface;
use Psr\Log\LoggerInterface;

/**
 * This class defines a structure for Check objects, to make a few common things slightly easier.
 * It does not have to be used for all checks, but all checks must implement the CheckInterface.
 */
abstract class AbstractCheck implements CheckInterface
{
    /**
     * Builds the result objects.
     *
     * @var ResultBuilder
     */
    protected $resultBuilder;

    /**
     * The check configuration.
     *
     * @var ConfigurationInterface
     */
    protected $config;

    /**
     * @var null|LoggerInterface
     */
    protected $logger;

    /**
     * Constructor...
     *
     * Each check needs to created for a configuration.
     * The configuration defines how the check works, so this a useful
     * relationship for reporting/logging.
     * The ResultBuilder abstracts the building of results so that they are done
     * in a predictable manner through a common interface.
     *
     * @param ConfigurationInterface $config
     * @param ResultBuilder          $resultBuilder
     * @param null|LoggerInterface   $logger
     */
    public function __construct(ConfigurationInterface $config, ResultBuilder $resultBuilder, ?LoggerInterface $logger = null)
    {
        $this->config = $config;
        $this->resultBuilder = $resultBuilder;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    abstract public function run(): ResultInterface;

    /**
     * {@inheritdoc}
     */
    abstract public static function requiredConfiguration(): string;

    /**
     * {@inheritdoc}
     */
    abstract public static function requiredBuilder(): string;

    /**
     * Use the logger to record an exception.
     * Used to report the exceptions which shouldn't be exposed by the HTTP response.
     *
     * @param \Exception $e
     *
     * @return void
     */
    protected function log(\Throwable $e)
    {
        if ($this->logger === null) {
            return;
        }

        $this->logger->error($e);
    }
}
