<?php

namespace Contexts\Healthz\Check\Configuration;

interface ConfigurationInterface
{
    /**
     * Get the value of a conifguration key.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key);

    /**
     * Return the conifguration array in all it's glory.
     *
     * @return array
     */
    public function asArray(): array;
}
