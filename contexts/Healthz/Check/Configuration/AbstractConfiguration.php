<?php

namespace Contexts\Healthz\Check\Configuration;

use Contexts\Healthz\Exception\InvalidConfigurationException;

/*
 * This class is designed to make it easier when implementing a new configuration.
 * This provides the 'guard' and 'decorate' abstracts, as well most mehod implementations
 * for matching the interface.
 */
abstract class AbstractConfiguration implements ConfigurationInterface
{
    /**
     * The raw configuration...an associative array.
     *
     * @var array
     */
    protected $configuration = [];

    /**
     * Constructor...
     * Validate that it's not empty.$
     *
     * @param array $rawConfiguration
     * @param array $configuration
     */
    public function __construct(array $configuration)
    {
        if (empty($configuration)) {
            throw new InvalidConfigurationException("The Configuration cannot be empty.");
        }

        $this->guard($configuration);
        $decorated = $this->decorate($configuration);

        $this->configuration = $decorated;
    }

    /**
     * Get the value of a configuration key.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key)
    {
        return $this->configuration[$key] ?? null;
    }

    /**
     * Return the configuration array in all it's glory.
     *
     * @return array
     */
    public function asArray(): array
    {
        return $this->configuration;
    }

    /**
     * Used as a way to add validation for a configuration.
     * This method should throw an exception if the configuration supplied is not valid.
     *
     * I've left it to default to nothing as it may be useful too turn it off.
     * It should be very rare that a config does not have it's own implementation of this function.
     *
     *
     * @param array $config
     *
     * @return void
     */
    protected function guard(array $config)
    {
    }

    /**
     * Checks that the '$config' array contains the keys (with values) found in the '$expected' array.
     * Most configs will have a set of required keys i.e. mysql requires a host, port, user, pass.
     * This provides a convenient way to check that all keys are present; otherwise throws a useful exception.
     *
     * @param array $expected
     * @param array $config
     *
     * @return void
     */
    protected function guardRequiredKeys(array $expected, array $config): void
    {
        $configKeys = array_keys(
            array_filter(
                $config,
                // Values that are equal to false may still be required
                // i.e. 0 is a valid value for alot of config settings; would be stripped by default array_filter
                function ($item) {
                    return $item !== null && $item !== '';
                }
            )
        );

        $diff = array_diff($expected, $configKeys);
        $isMissingKeys = !empty($diff);

        if ($isMissingKeys) {
            throw new InvalidConfigurationException("Missing configuration keys: " . implode(", ", $diff));
        }
    }

    /**
     * Used to decorate the configuration array before construction is complete.
     * This is where default values can be added, or keys transformed, or any destructuring etc.
     *
     * This works in an immutable way, it returns the new value; does not modify by reference.
     *
     * @param array $config
     *
     * @return array
     */
    protected function decorate(array $config): array
    {
        return $config;
    }
}
