<?php

namespace Contexts\Healthz\Check\Configuration;

class ElasticSearchConfiguration extends AbstractConfiguration
{
    /**
     * Checks the required keys are present.
     *
     *
     * @param array $config
     *
     * @throws Healthz\Exception\InvalidConfigurationException
     *
     * @return void
     */
    protected function guard(array $config)
    {
        $this->guardRequiredKeys(
            [
                'hosts',
            ],
            $config
        );
    }

    /**
     * Add the optional config options if they are not set.
     *
     * @param array $config
     *
     * @return array
     */
    protected function decorate(array $config): array
    {
        if (!isset($config['retries'])) {
            $config['retries'] = 0;
        }

        return $config;
    }
}
