<?php

namespace Contexts\Healthz\Check\Configuration;

class PostgresConfiguration extends AbstractConfiguration
{
    /**
     * Checks the required keys are present.
     *
     *
     * @param array $config
     *
     * @throws Healthz\Exception\InvalidConfigurationException
     *
     * @return void
     */
    protected function guard(array $config)
    {
        $this->guardRequiredKeys(
            [
                'host',
                'dbname',
                'user',
                'password',
            ],
            $config
        );
    }

    /**
     * Add the optional config options if they are not set.
     *
     * @param array $config
     *
     * @return array
     */
    protected function decorate(array $config): array
    {
        if (!isset($config['query'])) {
            $config['query'] = 'SELECT 1;';
        }

        if (!isset($config['port'])) {
            // MySQL will default to 3306 anyway, so if they don't supply one, thats what we use
            // But lets leave it to the PDO library to figure our the default
            $config['port'] = null;
        }

        if (!isset($config['timeout'])) {
            // The timeout option passed to PDO
            $config['timeout'] = 3;
        }

        return $config;
    }
}
