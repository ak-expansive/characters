<?php

namespace Contexts\Healthz\Check\Result;

use Contexts\Healthz\Check\Configuration\ConfigurationInterface;

class Result implements ResultInterface
{
    /**
     * Which check was this for?
     *
     * @var ConfigurationInterface
     */
    protected $for;

    /**
     * All errors that occurred.
     *
     * @var array
     */
    protected $errors = [];

    /**
     * @param ConfigurationInterface $config
     */
    public function __construct(ConfigurationInterface $config)
    {
        $this->for = $config;
    }

    /**
     * @inheritdoc
     */
    public function wasSuccessful(): bool
    {
        return empty($this->errors);
    }

    /**
     * @inheritdoc
     */
    public function errors(): array
    {
        return $this->errors;
    }

    /**
     * @inheritdoc
     */
    public function addError(string $message, string $phase = 'n/a')
    {
        $this->errors[] = compact('message', 'phase');
    }
}
