<?php

namespace Contexts\Healthz\Check\Result;

use Contexts\Healthz\Check\Configuration\ConfigurationInterface;

class ResultBuilder
{
    /**
     * Create a new Result object.
     *
     * @param ConfigurationInterface $config
     *
     * @return ResultInterface
     */
    public function build(ConfigurationInterface $config): ?ResultInterface
    {
        return new Result($config);
    }
}
