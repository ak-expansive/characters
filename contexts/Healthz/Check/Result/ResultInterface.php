<?php

namespace Contexts\Healthz\Check\Result;

use Contexts\Healthz\Check\Configuration\ConfigurationInterface;

interface ResultInterface
{
    /**
     * Constructor...
     *
     * @param ConfigurationInterface $config
     */
    public function __construct(ConfigurationInterface $config);

    /**
     * Determine whether the check was successful.
     *
     * @return bool
     */
    public function wasSuccessful(): bool;

    /**
     * Return any errors that occured during the check.
     * Should return an empty array if no errors occurred.
     *
     * @return array
     */
    public function errors(): array;

    /**
     * Add an error to the result.
     * The phase will be added as an additional field, helps debug which part of the check failed.
     * An example for mysql may be, 'connecting' or 'query' phases
     *
     * @param string $message
     * @param string $phase
     *
     * @return void
     */
    public function addError(string $message, string $phase);
}
