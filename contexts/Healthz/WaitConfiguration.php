<?php

namespace Contexts\Healthz;

class WaitConfiguration
{
    protected int $between;

    protected int $initial;

    protected int $maxTries;

    public function __construct(int $between, int $initial, int $maxTries)
    {
        $this->between = $between;
        $this->initial = $initial;
        $this->maxTries = $maxTries;
    }

    public function interval(): int
    {
        return $this->between;
    }

    public function initialDelay(): int
    {
        return $this->initial;
    }

    public function maxTries(): int
    {
        return $this->maxTries;
    }

    public static function makeDefault(): self
    {
        return new static(5, 0, 3);
    }
}
