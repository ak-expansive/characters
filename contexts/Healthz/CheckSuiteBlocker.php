<?php

namespace Contexts\Healthz;

use Contexts\Healthz\Check\Result\Result;
use Contexts\Healthz\Helpers\ResponseBuilder;

class CheckSuiteBlocker
{
    protected CheckSuiteRunner $checkResolver;

    protected $outputCallback = null;

    /**
     * @param CheckResolver    $checkResolver
     * @param CheckSuiteRunner $checkSuiteRunner
     */
    public function __construct(CheckSuiteRunner $checkSuiteRunner)
    {
        $this->checkSuiteRunner = $checkSuiteRunner;
    }

    public function setOutputCallback(callable $callback)
    {
        $this->outputCallback = $callback;
    }

    public function wait(array $suiteDefinition, WaitConfiguration $waitConfig): bool
    {
        $this->output("Waiting for a successful result...");
        $this->output("Initial delay: " . $waitConfig->initialDelay());
        sleep($waitConfig->initialDelay());

        $results = $this->checkSuiteRunner->run($suiteDefinition);
        $responseBuilder = new ResponseBuilder($results);
        $tries = 1;

        while ($tries < $waitConfig->maxTries() && !$responseBuilder->wasSuccessful()) {
            $this->output(
                sprintf("Attempt %d/%d was unsuccessful!", $tries, $waitConfig->maxTries()),
            );

            $this->showFailedSummary($results);

            sleep($waitConfig->interval());
            $results = $this->checkSuiteRunner->run($suiteDefinition);
            $responseBuilder = new ResponseBuilder($results);
            $tries++;
        }

        $success = $responseBuilder->wasSuccessful();

        if (!$success) {
            $this->output(
                sprintf("Attempt %d/%d was unsuccessful!", $tries, $waitConfig->maxTries()),
            );

            $this->showFailedSummary($results);
        }

        return $success;
    }

    protected function output(string ...$lines)
    {
        if ($this->outputCallback === null) {
            return;
        }

        foreach ($lines as $line) {
            ($this->outputCallback)($line);
        }
    }

    protected function showFailedSummary(array $results): void
    {
        if ($this->outputCallback === null) {
            return;
        }

        $failedResults = array_filter($results, function (Result $result) {
            return !$result->wasSuccessful();
        });

        foreach ($failedResults as $check => $result) {
            $lines = array_map(function (array $error) {
                return $error['phase'] . ':' . $error['message'];
            }, $result->errors());

            $this->output(
                "Errors for $check:",
                ...$lines
            );
        }
    }
}
