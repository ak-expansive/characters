<?php

namespace Contexts\Healthz;

use Contexts\Healthz\Check\Result\Result;
use Contexts\Healthz\Exception\CheckNotRegisteredException;

class CheckSuiteRunner
{
    /**
     * @var CheckResolver
     */
    protected CheckResolver $checkResolver;

    /**
     * @var array
     */
    protected array $checkConfigs;

    /**
     * Constructor...
     *
     * @param CheckResolver $checkResolver
     * @param array         $checkConfigs
     */
    public function __construct(CheckResolver $checkResolver, array $checkConfigs = [])
    {
        $this->checkResolver = $checkResolver;
        $this->checkConfigs = $checkConfigs;
    }

    /**
     * Loop through the suite definition, find and build all checks required, run them.
     * Return results of the checks.
     *
     * @param array $suiteDefinition
     *
     * @return array
     */
    public function run(array $suiteDefinition): array
    {
        $results = [];

        foreach ($suiteDefinition as $name => $checkDefinition) {
            $checkKey = $name;

            if (is_string($checkDefinition)) {
                $checkKey = $checkDefinition;
            }

            $results[$checkKey] = $this->runCheck($checkDefinition);
        }

        return $results;
    }

    protected function runCheck($check): Result
    {
        if (is_string($check)) {
            $checkConfig = $this->checkConfigs[$check] ?? null;
        } else {
            $checkConfig = $check;
        }

        if ($checkConfig === null) {
            throw new CheckNotRegisteredException();
        }

        return $this->checkResolver->resolve($checkConfig)->run();
    }
}
