<?php

namespace Contexts\Healthz\Exception;

use Exception;

class InvalidConnectionException extends Exception
{
}
