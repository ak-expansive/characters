<?php

namespace Contexts\Healthz;

use Contexts\Healthz\Check\Builder\BuilderInterface;
use Contexts\Healthz\Check\CheckInterface;
use Contexts\Healthz\Check\Configuration\ConfigurationBuilder;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Exception\BuilderNotRegisteredException;
use Contexts\Healthz\Exception\InvalidConfigurationException;
use Contexts\Healthz\Exception\ProviderNotRegisteredException;
use Contexts\Healthz\Integration\ConfigurationProviderInterface;

class CheckResolver
{
    /**
     * @var ConfigurationBuilder
     */
    protected $configBuilder;

    /**
     * @var BuilderInterface
     */
    protected $builders;

    /**
     * @var ConfigurationProviderInterface[]
     */
    protected $configProviders = [];

    /**
     * Constructor...
     *
     * @param ConfigurationBuilder $configBuilder
     * @param array                $builders
     * @param array                $configProviders
     */
    public function __construct(ConfigurationBuilder $configBuilder, array $builders, array $configProviders)
    {
        $this->configBuilder = $configBuilder;
        $this->builders = $builders;
        $this->configProviders = $configProviders;
    }

    /**
     * Take a configuration array, and return a Check object based upon its values.
     *
     * @param array $checkDefinition
     *
     * @return CheckInterface
     */
    public function resolve(array $checkDefinition): CheckInterface
    {
        $checkConfig = $this->buildConfig($checkDefinition);
        $builder = $this->findBuilder($checkDefinition);

        return $builder->build($checkConfig);
    }

    /**
     * We will loop through all builders given to this object at construction.
     * When the class of the builder matches the configuration value for class;
     * then we've found our builder. If none match, then you will get an exception.
     *
     * Any new Builders must be provided in the constructor, otherwise they will
     * not be known to the system.
     *
     *
     * @param array $checkDefinition
     *
     * @throws BuilderNotRegisteredException
     *                                       if the builder was not supplied to this class at construction
     *
     * @return BuilderInterface
     */
    protected function findBuilder(array $checkDefinition): BuilderInterface
    {
        $builderClass = $checkDefinition['class']::requiredBuilder();

        foreach ($this->builders as $builder) {
            if (get_class($builder) == $builderClass) {
                return $builder;
            }
        }

        throw new BuilderNotRegisteredException("Builder '" . $builderClass . "' not found for check '" . $checkDefinition['class'] . "'");
    }

    /**
     * Build the configuration object based upon the check configuration.
     *
     *
     * @param array $checkDefinition
     *
     * @throws InvalidConfigurationException
     *                                       if the 'configuration' or 'configurationProvider' section is not present
     *
     * @return ConfigurationInterface
     */
    protected function buildConfig(array $checkDefinition): ConfigurationInterface
    {
        if (isset($checkDefinition['configurationProvider'])) {
            return $this->getConfigFromProvider($checkDefinition['configurationProvider']);
        }

        if (isset($checkDefinition['configuration']) && !empty($checkDefinition['configuration'])) {
            return $this->configBuilder->build(
                $checkDefinition['class']::requiredConfiguration(),
                $checkDefinition['configuration']
            );
        }

        throw new InvalidConfigurationException(
            "A check must contain a 'configuration' or 'configurationProvider' section."
        );
    }

    /**
     * If the 'configurationProvider' block was used, then build the configuration
     * provider and get the configuration from it.
     *
     *
     * @param array $configProviderDefinition
     *
     * @throws InvalidConfigurationException
     *                                       if the 'class' is not defined
     *
     * @return ConfigurationInterface
     */
    protected function getConfigFromProvider(array $configProviderDefinition): ConfigurationInterface
    {
        if (!isset($configProviderDefinition['class'])) {
            throw new InvalidConfigurationException(
                "When using a configuration provider, you must supply the class path. 'configurationProvider.class'"
            );
        }

        $configProvider = $this->findConfigProvider($configProviderDefinition['class']);
        return $configProvider->retrieve($configProviderDefinition['arguments'] ?? []);
    }

    /**
     * Based upon the class supplied find the relevant configuration provider
     * if it is registered.
     *
     *
     * @param string $class
     *
     * @throws ProviderNotRegisteredException
     *                                        if the provider was not given during construction of this object
     *
     * @return ConfigurationProviderInterface
     */
    protected function findConfigProvider(string $class): ConfigurationProviderInterface
    {
        foreach ($this->configProviders as $provider) {
            if (get_class($provider) == $class) {
                return $provider;
            }
        }

        throw new ProviderNotRegisteredException(
            "Configuration Provider '" . $class . "' was not registered, please ensure it is injected to " . self::class
        );
    }
}
