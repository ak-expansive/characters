#!/bin/bash

touch ./seed.json

echo "" > ./seed.json

while read line
do
	FIRST_NAME=$(echo "$line" | awk '{print $1}')
	LAST_NAME=$(echo "$line" | awk '{print $2}')
	SLUG=$(echo "$FIRST_NAME-$LAST_NAME" | tr '[:upper:]' '[:lower:]')
	CHARACTER="{\n\"first_name\": \"$FIRST_NAME\",\n\"last_name\": \"$LAST_NAME\",\n\"slug\": \"$SLUG\"\n},"

	echo -e "$CHARACTER" >> ./seed.json
done < seed.txt