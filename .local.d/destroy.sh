#!/bin/bash

SRC_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. $SRC_DIR/.common.sh

function delete_ns() {
	kubectl get ns $NAMESPACE &> /dev/null
	_GET_NS_RESULT="$?"

	if [ "$_GET_NS_RESULT" == "1" ]; then
		_info "Nothing to do..."
		return 0
	fi

	kubectl delete ns $NAMESPACE
}

function main() {
    _section_header "Set Context to Minikube"
    kubectl config use-context minikube
    _section_footer "Set Context to Minikube"

	_section_header "Destroying"
	delete_ns
	_section_footer "Destroying"
}

main "$@"
