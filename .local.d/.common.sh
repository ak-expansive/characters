#!/bin/bash

NAMESPACE=${NAMESPACE:-expansive--characters}
PROJECT_ROOT=$(git rev-parse --show-toplevel)

if [ -z "$DEV_ENV_LOCATION" ]; then
	DEV_ENV_LOCATION=$PROJECT_ROOT/../dev-env
fi

if [ ! -d "$DEV_ENV_LOCATION" ]; then
	echo "The Dev env repository was not found at: $DEV_ENV_LOCATION"
	echo "Ensure it exists at that location or export the DEV_ENV_LOCATION variable"
	exit 1
fi

. $DEV_ENV_LOCATION/utils.d/output.sh