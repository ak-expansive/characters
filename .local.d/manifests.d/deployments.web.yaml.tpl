
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    name: web
  name: web
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      name: web
  template:
    metadata:
      labels:
        name: web
      name: web
    spec:
      imagePullSecrets:
      - name: gitlabpullsecret
      volumes:
      - name: app-volume
        hostPath:
          path: {{ .host_path }}
      containers:
      - image: "registry.gitlab.com/ak-expansive/nginx:beta-1.0.1"
        imagePullPolicy: IfNotPresent
        name: nginx
        workingDir: "/opt/app-root"
        volumeMounts:
        - name: app-volume
          mountPath: "/opt/app-root"
        ports:
        - containerPort: 8080
        - containerPort: 8888
        envFrom:
        - configMapRef:
            name: nginx-env
        resources: ~
      - image: "registry.gitlab.com/ak-expansive/php-fpm/dev:beta-1.0.1"
        imagePullPolicy: IfNotPresent
        workingDir: "/opt/app-root"
        name: php-fpm
        volumeMounts:
        - name: app-volume
          mountPath: "/opt/app-root"
        ports:
        - containerPort: 9000
        env:
        - name: OVERRIDE_WWW_DATA_UID
          value: "1000"
        - name: OVERRIDE_WWW_DATA_GID
          value: "1000"
        - name: KUBERNETES_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        envFrom:
        - configMapRef:
            name: php-fpm-env
        resources: ~
