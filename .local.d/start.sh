#!/bin/bash

SRC_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. $SRC_DIR/.common.sh

function create_namespace() {
	kubectl get ns $NAMESPACE &> /dev/null
	_GET_NS_RESULT="$?"

	if [ "$_GET_NS_RESULT" == "0" ]; then
		_info "Nothing to do..."
		return 0
	fi

	kubectl create ns $NAMESPACE
}

function create_pull_secrets() {
	NAMESPACE=$NAMESPACE $DEV_ENV_LOCATION/utils.d/create-pull-secrets.sh
}

# We need to mount local directory into the correct location inthe container
# For this we need to render the template with a host_path variable
# The render.sh script in the dev-env repo runs this rendering in a docker container
# This avoids having to install the render binary on the host; also controls version of render
function render_deployment() {
	APP_ROOT=$(cd $SRC_DIR && cd ../ && pwd)
	RELATIVE_APP_PATH=$(basename $APP_ROOT)
	ABSOLUTE_APP_PATH=$($DEV_ENV_LOCATION/utils.d/hostpaths.sh $APP_ROOT)

	$DEV_ENV_LOCATION/utils.d/render.sh $RELATIVE_APP_PATH/.local.d/manifests.d/deployments.web.yaml.tpl "-s --set \"host_path=$ABSOLUTE_APP_PATH\""
}

function create_app() {
	kubectl apply -n $NAMESPACE -f $SRC_DIR/manifests.d/configmaps.yaml
	render_deployment | kubectl apply -n $NAMESPACE -f -
	kubectl apply -n $NAMESPACE -f $SRC_DIR/manifests.d/services.yaml
	kubectl apply -n $NAMESPACE -f $SRC_DIR/manifests.d/ingresses.yaml
}

function provision_app() {
	kubectl rollout status -n $NAMESPACE deployments/web

	POD_NAME=$(kubectl get pods -n $NAMESPACE -l name=web -o jsonpath='{.items[0].metadata.name}')

	kubectl exec -n $NAMESPACE $POD_NAME -c php-fpm -- composer install

	# Provision Kibana for this app
	kubectl exec -n $NAMESPACE $POD_NAME -c php-fpm -- php artisan m:k:p

	# Wait for app dependencies to be ready
	kubectl exec -n $NAMESPACE $POD_NAME -c php-fpm -- php artisan h:w:s ready-to-provision --max=20

	# Run migrations
	kubectl exec -n $NAMESPACE $POD_NAME -c php-fpm -- php artisan d:m:run

	# Create Elastic Indices
	kubectl exec -n $NAMESPACE $POD_NAME -c php-fpm -- php artisan e:i:c

	# Import seed data
	kubectl exec -n $NAMESPACE $POD_NAME -c php-fpm -- php artisan d:t:i
}

function update_dns() {
	$DEV_ENV_LOCATION/utils.d/hosts.sh
}

function _section() {
	_section_header "$1"
	$2
	_section_footer "$1"
}

function main() {
	_section "Set Context to Minikube" "kubectl config use-context minikube"

	if [ ! -z "$1" ]; then
		$1
		exit "$?"
	fi

	_section "Initialise Namespace" create_namespace

	_section "Creating Pull Secrets" create_pull_secrets

	_section "Starting App" create_app

	_section "Set DNS records" update_dns

	_section "Provision App" provision_app

}

main "$@"
