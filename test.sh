#!/bin/bash

SRC_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

$SRC_DIR/kube ssh web php-fpm ./vendor/bin/phpunit "$@"