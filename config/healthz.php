<?php

use Contexts\Healthz\Check\MysqlCheck;
use Contexts\Healthz\Check\RedisCheck;
use Contexts\Healthz\Check\PostgresCheck;
use Contexts\Healthz\Check\ElasticSearchCheck;
use Contexts\Healthz\Check\Builder\RedisBuilder;
use Contexts\Healthz\Check\Builder\PostgresBuilder;
use Contexts\Healthz\Check\Builder\ElasticSearchBuilder;
use Contexts\Healthz\Check\Configuration\PostgresConfiguration;
use Contexts\Healthz\Integration\Lumen\RedisConfigurationProvider;
use Contexts\Healthz\Check\Configuration\ElasticSearchConfiguration;
use Contexts\Healthz\Integration\Lumen\ConfigSourcedConfigurationProvider;

return [
    // An array of all the builder classes to register with the package
    'builders' => [
        RedisBuilder::class,
        PostgresBuilder::class,
        ElasticSearchBuilder::class,
    ],

    // An array of all the configuration providers to register with the package
    'configurationProviders' => [
        RedisConfigurationProvider::class,
        ConfigSourcedConfigurationProvider::class,
    ],

    'checks' => [

        // Example: Relation Database, MySQL.
        'rdb' => [

            'class' => PostgresCheck::class,

            'configurationProvider' => [
                'class' => ConfigSourcedConfigurationProvider::class,
                'arguments' => [
                    'key' => 'doctrine.connection',
                    'type' => PostgresConfiguration::class,
                ]
            ],
        ],

        'docstore' => [

            'class' => ElasticSearchCheck::class,

            'configurationProvider' => [
                'class' => ConfigSourcedConfigurationProvider::class,
                'arguments' => [
                    'key' => 'elasticsearch.connection',
                    'type' => ElasticSearchConfiguration::class,
                    'overrides' => [
                        'retries' => 1,
                    ]
                ]
            ],
        ],

        // Example: Cache DB, Redis.
        'kv' => [
            'class' => RedisCheck::class,

            'configurationProvider' => [
                'class' => RedisConfigurationProvider::class,
                'arguments' => [
                    'cluster' => true,
                    'connection' => 'default',
                    'connectionIndex' => 0,
                ],
            ],
        ],

    ],

    'suites' => [
        'endpoint' => [
            'rdb',
            'kv',
            'docstore',
        ],

        'ready-to-provision' => [
            'rdb',
            'kv',
            'docstore',
        ],

        'ready' => [
            'rdb',
            'kv',
            'docstore',
        ]
    ],

    // Endpoint is the HTTP health check config
    'endpoint' => [

        // URI to use for the route
        'URI' => '_healthz',

        // Middleware groups to apply to the route
        'middleware' => [

        ],
    ],

];
