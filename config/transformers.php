<?php

use Contexts\Core\Images\UrlTransformer;

return [
    'registered' => [
        UrlTransformer::class,
    ]
];