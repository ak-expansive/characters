<?php

return [
    'kibana' => [
        'creationEnabled' => env('KIBANA_CREATION_ENABLED', true),
        'host' => env('KIBANA_HOST', null),
        'indexPatternID' => env('KIBANA_INDEX_PATTERN_ID', 'logstash'),
    ],

    'kubernetes' => [
        'namespace' => env('KUBERNETES_NAMESPACE', 'expansive--characters'),
    ]
];
