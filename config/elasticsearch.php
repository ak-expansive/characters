<?php

use Contexts\ElasticSearch\Repository\Meta\Finder\DefaultMetaFinderProvider;

return [
    'connection' => [
        'hosts' => explode(',', env('ELASTICSEARCH_HOSTS', '')),
        'retries' => 1,
    ],



    'respositories' => [
        /*
        [
            'model' => /Fully/Qualified/Class
            'repository' => /Fully/Qualified/Class
        ]
        */
    ],

    'MetaFinderProvider' => DefaultMetaFinderProvider::make(base_path() . '/contexts')
];
