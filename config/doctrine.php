<?php

use Contexts\CQRS\Doctrine\Subscribers\ProjectionSubscriber;

return [
    'proxyDir' => storage_path() . '/doctrine-proxies',

    'connection' => [
        'driver'   => 'pdo_pgsql',
        'host'     => env('DB_HOST', 'localhost'),
        'port'     => env('DB_PORT', 5432),
        'dbname'   => env('DB_DATABASE', 'characters'),
        'user'     => env('DB_USERNAME', 'characters'),
        'password' => env('DB_PASSWORD', ''),
        'charset'  => env('DB_CHARSET', 'utf8'),
        'prefix'   => env('DB_PREFIX', ''),
        'schema'   => env('DB_SCHEMA', 'public'),
    ],

    'migrations' => [
        'namespace' => 'Migrations',
        'table' => 'migrations',
        'directory' => base_path() . '/db/migrations',
    ],

    'subscribers' => [
        ProjectionSubscriber::class,
    ],

    'mappingDirectories' => [
        base_path() . '/contexts/CQRS/Doctrine/Mappings',
        base_path() . '/contexts/Characters/Doctrine/Mappings',
    ]
];
