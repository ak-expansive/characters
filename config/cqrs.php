<?php

use League\Tactician\Plugins\LockingMiddleware;
use Contexts\CQRS\Tactician\Middleware\CommandLogWrapper;
use Contexts\CQRS\Tactician\Middleware\CommandValidationWrapper;
use Contexts\CQRS\Tactician\Middleware\DoctrineTransactionWrapper;

return [
    'primaryHandler' => null,

    'commandLogRepository' => null,

    'middleware' => [
        LockingMiddleware::class,
        CommandValidationWrapper::class,
        CommandLogWrapper::class,
        DoctrineTransactionWrapper::class,
    ],
];
