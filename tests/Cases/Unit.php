<?php

namespace Tests\Cases;

use ReflectionClass;
use PHPUnit\Framework\TestCase;

/**
 * Empty for now, but we may wanna provide convenience methods.
 * Better to start off with everything extending this.
 */
abstract class Unit extends TestCase
{
    protected function callProtectedMethodOnSubject($subject, $method, ...$args)
    {
        $class = new ReflectionClass($subject);
        $method = $class->getMethod($method);
        $method->setAccessible(true);
        return $method->invokeArgs($subject, $args);
    }

    protected function getProtectedValueOfSubject($subject, $property)
    {
        $class = new ReflectionClass($subject);
        $prop = $class->getProperty($property);
        $prop->setAccessible(true);

        return $prop->getValue($subject);
    }
}
