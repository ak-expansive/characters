<?php

namespace Tests\Unit\Healthz\Helpers;

use Predis\Client;
use Tests\Cases\Unit;
use Contexts\Healthz\Helpers\RedisClientBuilder;

class RedisClientBuilderTest extends Unit
{
    public function testSuccessfulConstruction()
    {
        $target = new RedisClientBuilder;
        $this->assertInstanceOf(RedisClientBuilder::class, $target);
    }

    public function testBuiild()
    {
        $target = new RedisClientBuilder;
        $this->assertInstanceOf(RedisClientBuilder::class, $target);
        // This is a unit test; no connection should actually be established
        $result = $target->build([
            'host' => 'localhost',
            'port' => '6379',
            'password' => 'testy',
            'database' => 0,
        ]);

        $this->assertInstanceOf(Client::class, $result);
    }
}
