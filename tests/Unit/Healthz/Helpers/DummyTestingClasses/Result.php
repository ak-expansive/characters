<?php

namespace Tests\Unit\Healthz\Helpers\DummyTestingClasses;

use Contexts\Healthz\Check\Result\ResultInterface;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;

class Result implements ResultInterface
{
    public function __construct(ConfigurationInterface $config)
    {

    }

    public function wasSuccessful(): bool
    {
        return true;
    }

    public function errors(): array
    {
        return [];
    }

    public function addError(string $message, string $phase)
    {
        
    }
}