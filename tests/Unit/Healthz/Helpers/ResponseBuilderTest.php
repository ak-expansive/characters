<?php

namespace Tests\Unit\Healthz\Helpers;

use Tests\Cases\Unit;
use Contexts\Healthz\Helpers\ResponseBuilder;
use Contexts\Healthz\Check\Result\ResultInterface;
use Contexts\Healthz\Exception\NotAResultException;
use Tests\Unit\Healthz\Helpers\DummyTestingClasses\Result;

class ResponseBuilderTest extends Unit
{
    public function testSuccessfulConstruction()
    {
        $mockResult = $this->createMock(Result::class);
        $target = new ResponseBuilder([$mockResult]);
        $this->assertInstanceOf(ResponseBuilder::class, $target);
    }

    public function testUnuccessfulConstructionWithScalar()
    {
        $this->expectException(NotAResultException::class);
        $this->expectExceptionMessage("Expected instance of " . ResultInterface::class . ", got string.");
        $target = new ResponseBuilder(['a string']);
    }

    public function testUnuccessfulConstructionWithObject()
    {
        $this->expectException(NotAResultException::class);
        $this->expectExceptionMessage("Expected instance of " . ResultInterface::class . ", got DateTime.");
        $target = new ResponseBuilder([new \DateTime]);
    }

    public function testBuildWithSingleResultNoMeta()
    {
        $mockResult = $this->createMock(Result::class);
        $mockResult->expects($this->exactly(2))
            ->method('wasSuccessful')
            ->willReturn(true);

        $mockResult->expects($this->exactly(2))
            ->method('errors')
            ->willReturn([]);

        $target = new ResponseBuilder(['test1' => $mockResult]);
        $this->assertInstanceOf(ResponseBuilder::class, $target);

        $expectedResponse = [
            'test1' => [
                'healthy' => true,
                'errorsDuring' => [],
            ],
        ];
        $response = $target->build([]);

        $this->assertSame($expectedResponse, $response);
        $this->assertTrue($target->wasSuccessful());
    }

    public function testBuildWithSingleResultWithMeta()
    {
        $mockResult = $this->createMock(Result::class);
        $mockResult->expects($this->exactly(2))
            ->method('wasSuccessful')
            ->willReturn(true);

        $mockResult->expects($this->exactly(2))
            ->method('errors')
            ->willReturn([]);

        $target = new ResponseBuilder(['test1' => $mockResult]);
        $this->assertInstanceOf(ResponseBuilder::class, $target);

        $expectedResponse = [
            'test1' => [
                'healthy' => true,
                'errorsDuring' => [],
            ],
            '_meta' => [
                'somekey' => 'somevalue'
            ],
        ];
        $response = $target->build(['somekey' => 'somevalue']);

        $this->assertSame($expectedResponse, $response);
        $this->assertTrue($target->wasSuccessful());
    }

    public function testBuildWithMultiResultNoMeta()
    {
        $mockResult = $this->createMock(Result::class);
        $mockResult->expects($this->exactly(2))
            ->method('wasSuccessful')
            ->willReturn(true);

        $mockResult->expects($this->exactly(2))
            ->method('errors')
            ->willReturn([]);

        $mockResult2 = $this->createMock(Result::class);
        $mockResult2->expects($this->exactly(2))
            ->method('wasSuccessful')
            ->willReturn(true);

        $mockResult2->expects($this->exactly(2))
            ->method('errors')
            ->willReturn([]);

        $target = new ResponseBuilder(['test1' => $mockResult, 'test2' => $mockResult2]);
        $this->assertInstanceOf(ResponseBuilder::class, $target);

        $expectedResponse = [
            'test1' => [
                'healthy' => true,
                'errorsDuring' => [],
            ],
            'test2' => [
                'healthy' => true,
                'errorsDuring' => [],
            ],
        ];
        $response = $target->build([]);

        $this->assertSame($expectedResponse, $response);
        $this->assertTrue($target->wasSuccessful());
    }

    public function testBuildWithMultiResultPartialFailureNoMeta()
    {
        $mockResult = $this->createMock(Result::class);
        $mockResult->expects($this->exactly(2))
            ->method('wasSuccessful')
            ->willReturn(true);

        $mockResult->expects($this->exactly(2))
            ->method('errors')
            ->willReturn([]);

        $mockResult2 = $this->createMock(Result::class);
        $mockResult2->expects($this->exactly(2))
            ->method('wasSuccessful')
            ->willReturn(false);

        $mockResult2->expects($this->exactly(2))
            ->method('errors')
            ->willReturn([['phase' => 'connection'], ['phase' => 'query']]);

        $target = new ResponseBuilder(['test1' => $mockResult, 'test2' => $mockResult2]);
        $this->assertInstanceOf(ResponseBuilder::class, $target);

        $expectedResponse = [
            'test1' => [
                'healthy' => true,
                'errorsDuring' => [],
            ],
            'test2' => [
                'healthy' => false,
                'errorsDuring' => [
                    'connection',
                    'query'
                ],
            ],
        ];
        $response = $target->build([]);

        $this->assertSame($expectedResponse, $response);
        $this->assertFalse($target->wasSuccessful());
    }

    public function testBuildWithMultiResultFullFailureNoMeta()
    {
        $mockResult = $this->createMock(Result::class);
        $mockResult->expects($this->exactly(2))
            ->method('wasSuccessful')
            ->willReturn(false);

        $mockResult->expects($this->exactly(2))
            ->method('errors')
            ->willReturn([['phase' => 'connection']]);

        $mockResult2 = $this->createMock(Result::class);
        $mockResult2->expects($this->exactly(2))
            ->method('wasSuccessful')
            ->willReturn(false);

        $mockResult2->expects($this->exactly(2))
            ->method('errors')
            ->willReturn([['phase' => 'connection'], ['phase' => 'query']]);

        $target = new ResponseBuilder(['test1' => $mockResult, 'test2' => $mockResult2]);
        $this->assertInstanceOf(ResponseBuilder::class, $target);

        $expectedResponse = [
            'test1' => [
                'healthy' => false,
                'errorsDuring' => [
                    'connection'
                ],
            ],
            'test2' => [
                'healthy' => false,
                'errorsDuring' => [
                    'connection',
                    'query'
                ],
            ],
        ];
        $response = $target->build([]);

        $this->assertSame($expectedResponse, $response);
        $this->assertFalse($target->wasSuccessful());
    }
}
