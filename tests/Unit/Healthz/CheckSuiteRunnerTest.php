<?php

namespace Tests\Unit\Healthz;

use Contexts\Healthz\CheckResolver;
use Tests\Cases\Unit;
use Contexts\Healthz\CheckSuiteRunner;
use Contexts\Healthz\Check\AbstractCheck;
use Contexts\Healthz\Check\Result\Result;

class CheckSuiteRunnerTest extends Unit
{
    public function testConstruction()
    {
        $mockResolver = $this->createMock(CheckResolver::class);
        
        $target = new CheckSuiteRunner($mockResolver);
        $this->assertInstanceOf(CheckSuiteRunner::class, $target);
    }

    public function testAllChecksAreExecuted()
    {
        $input = [
            'test1' => [
                'this-is-test1'
            ],
            'test2' => [
                'this-is-test2'
            ],
        ];

        $mockResult1 = $this->createMock(Result::class);
        $mockResult2 = $this->createMock(Result::class);

        $expected = [
            'test1' => $mockResult1,
            'test2' => $mockResult2,
        ];
        $mockCheck1 = $this->createMock(AbstractCheck::class);
        $mockCheck1->expects($this->exactly(1))
            ->method('run')
            ->willReturn($mockResult1);

        $mockCheck2 = $this->createMock(AbstractCheck::class);
        $mockCheck2->expects($this->exactly(1))
            ->method('run')
            ->willReturn($mockResult2);

        $mockResolver = $this->createMock(CheckResolver::class);
        $mockResolver->expects($this->exactly(2))
            ->method('resolve')
            ->withConsecutive([['this-is-test1']], [['this-is-test2']])
            ->willReturnOnConsecutiveCalls($mockCheck1, $mockCheck2);
        
        $target = new CheckSuiteRunner($mockResolver);
        $this->assertInstanceOf(CheckSuiteRunner::class, $target);

        $this->assertSame($expected, $target->run($input));
    }
}
