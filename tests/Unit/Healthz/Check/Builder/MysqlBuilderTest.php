<?php

namespace Tests\Unit\Healthz\Check\Builder;

use Psr\Log\LoggerInterface;
use Tests\Cases\Unit;
use Contexts\Healthz\Check\MysqlCheck;
use Contexts\Healthz\Check\Builder\MysqlBuilder;
use Contexts\Healthz\Check\Result\ResultBuilder;
use Contexts\Healthz\Check\Configuration\AbstractConfiguration;

class MysqlBuilderTest extends Unit
{
    public function testConstruction()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $target = new MysqlBuilder($mockResultBuilder, $mockLogger);
        $this->assertInstanceOf(MysqlBuilder::class, $target);
    }

    public function testBuild()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $target = new MysqlBuilder($mockResultBuilder, $mockLogger);
        $this->assertInstanceOf(MysqlBuilder::class, $target);
        $this->assertInstanceOf(MysqlCheck::class, $target->build($mockConfig));
    }
}
