<?php

namespace Tests\Unit\Healthz\Check\Builder;

use Psr\Log\LoggerInterface;
use Tests\Cases\Unit;
use Contexts\Healthz\Check\RedisCheck;
use Contexts\Healthz\Check\Builder\RedisBuilder;
use Contexts\Healthz\Check\Result\ResultBuilder;
use Contexts\Healthz\Check\Configuration\AbstractConfiguration;

class RedisBuilderTest extends Unit
{
    public function testConstruction()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $target = new RedisBuilder($mockResultBuilder, $mockLogger);
        $this->assertInstanceOf(RedisBuilder::class, $target);
    }

    public function testBuild()
    {
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $target = new RedisBuilder($mockResultBuilder, $mockLogger);
        $this->assertInstanceOf(RedisBuilder::class, $target);
        $this->assertInstanceOf(RedisCheck::class, $target->build($mockConfig));
    }
}
