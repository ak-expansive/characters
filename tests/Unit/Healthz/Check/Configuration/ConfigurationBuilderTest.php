<?php

namespace Tests\Unit\Healthz\Check\Configuration;

use Tests\Cases\Unit;
use Contexts\Healthz\Exception\InvalidBindingException;
use Contexts\Healthz\Check\Configuration\MysqlConfiguration;
use Contexts\Healthz\Check\Configuration\ConfigurationBuilder;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;

class ConfigurationBuilderTest extends Unit
{
    public function testConstruction()
    {
        $target = new ConfigurationBuilder;
        $this->assertInstanceOf(ConfigurationBuilder::class, $target);
    }

    public function testBuildValid()
    {
        $target = new ConfigurationBuilder;
        $result = $target->build(
            MysqlConfiguration::class, 
            [
                'host' => 'hosty',
                'port' => 3306,
                'user' => 'test',
                'pass' => 'pass',
                'dbname' => 'dbname',
            ]
        );
        $this->assertInstanceOf(MysqlConfiguration::class, $result);
    }

    public function testBuildInvalid()
    {
        $target = new ConfigurationBuilder;
       
        $this->expectException(InvalidBindingException::class);
        $this->expectExceptionMessage("doesnotexist does not implement " . ConfigurationInterface::class);

        $result = $target->build(
            'doesnotexist', 
            [
                'blah' => 'meh'
            ]
        );
    }
}
