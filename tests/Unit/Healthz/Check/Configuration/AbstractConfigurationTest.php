<?php

namespace Tests\Unit\Healthz\Check\Configuration;

use Tests\Cases\Unit;
use Contexts\Healthz\Exception\InvalidConfigurationException;
use Contexts\Healthz\Check\Configuration\AbstractConfiguration;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Tests\Unit\Healthz\Check\Configuration\DummyTestingClasses\AbstractConfigurationTester;
use Tests\Unit\Healthz\Check\Configuration\DummyTestingClasses\AbstractConfigurationTesterDecorator;
use Tests\Unit\Healthz\Check\Configuration\DummyTestingClasses\AbstractConfigurationTesterWithOkayGuard;
use Tests\Unit\Healthz\Check\Configuration\DummyTestingClasses\AbstractConfigurationTesterWithBadGuard;

class AbstractConfigurationTest extends Unit
{
    public function testSuccessfulConstructon()
    {
        $config = new AbstractConfigurationTester([
            'some_key' => 'meh',
            'some_other_key' => 'blah',
        ]);

        $this->assertInstanceOf(AbstractConfigurationTester::class, $config);
        $this->assertInstanceOf(AbstractConfiguration::class, $config);
        $this->assertInstanceOf(ConfigurationInterface::class, $config);
    }

    public function testInvalidConstructon()
    {
        $this->expectException(InvalidConfigurationException::class);
        // An empty array should fail

        new AbstractConfigurationTester([]);
    }

    public function testRawConfigurationWasSetSuccessfully()
    {
        $raw = [
            'some_key' => 'meh',
            'some_other_key' => 'blah',
        ];
        $config = new AbstractConfigurationTester($raw);
        $this->assertEquals($raw, $config->asArray());
        $this->assertEquals('meh', $config->get('some_key'));
        $this->assertEquals('blah', $config->get('some_other_key'));
    }

    public function testBadGuardThrowsException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("every time");
        $raw = [
            'some_key' => 'meh',
            'some_other_key' => 'blah',
        ];

        new AbstractConfigurationTesterWithBadGuard($raw);
    }

    public function testOkayGuardThrowsExceptionWithBadData()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("bad data");
        $raw = [
            'some_key' => 'meh',
            'some_other_key' => 'blah',
        ];

        new AbstractConfigurationTesterWithOkayGuard($raw);
    }

    public function testOkayGuardConstructsWithCorrectData()
    {
        $raw = [
            'im_required' => 'meh',
        ];

        $config = new AbstractConfigurationTesterWithOkayGuard($raw);
        $this->assertInstanceOf(AbstractConfigurationTesterWithOkayGuard::class, $config);
        $this->assertEquals($raw, $config->asArray());
    }

    public function testDecoratorIsUsed()
    {
        $raw = [
            'im_required' => 'meh',
        ];

        $config = new AbstractConfigurationTesterDecorator($raw);
        $this->assertInstanceOf(AbstractConfigurationTesterDecorator::class, $config);
        $this->assertEquals('decorated', $config->get('__decorate'));
    }
}
