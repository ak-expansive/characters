<?php

namespace Tests\Unit\Healthz\Check\Configuration;

use Tests\Cases\Unit;
use Contexts\Healthz\Check\Configuration\MysqlConfiguration;
use Contexts\Healthz\Check\Configuration\AbstractConfiguration;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Exception\InvalidConfigurationException;

class MysqlConfigurationTest extends Unit
{
    public function testSuccessfulConstructon()
    {
        $config = new MysqlConfiguration([
            'host' => 'hosty',
            'dbname' => 'dbdbdb',
            'user' => 'robotnik',
            'pass' => 'OHMYGEEEERD',
        ]);

        $this->assertInstanceOf(MysqlConfiguration::class, $config);
        $this->assertInstanceOf(AbstractConfiguration::class, $config);
        $this->assertInstanceOf(ConfigurationInterface::class, $config);
    }

    public function testEmptyHostValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: host");
        new MysqlConfiguration([
            'host' => '',
            'dbname' => 'dbdbdb',
            'user' => 'robotnik',
            'pass' => 'OHMYGEEEERD',
        ]);
    }

    public function testMissingHostValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: host");
        new MysqlConfiguration([
            'dbname' => 'dbdbdb',
            'user' => 'robotnik',
            'pass' => 'OHMYGEEEERD',
        ]);
    }

    public function testEmptyDBValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: dbname");
        new MysqlConfiguration([
            'host' => 'hosty',
            'dbname' => '',
            'user' => 'robotnik',
            'pass' => 'OHMYGEEEERD',
        ]);
    }

    public function testMissingDBValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: dbname");
        new MysqlConfiguration([
            'host' => 'hosty',
            'user' => 'robotnik',
            'pass' => 'OHMYGEEEERD',
        ]);
    }

    public function testEmptyUserValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: user");
        new MysqlConfiguration([
            'host' => 'hosty',
            'dbname' => 'dbdbdb',
            'user' => '',
            'pass' => 'OHMYGEEEERD',
        ]);
    }

    public function testMissingUserValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: user");
        new MysqlConfiguration([
            'host' => 'hosty',
            'dbname' => 'dbdbdb',
            'pass' => 'OHMYGEEEERD',
        ]);
    }

    public function testEmptyPassValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: pass");
        new MysqlConfiguration([
            'host' => 'hosty',
            'dbname' => 'dbdbdb',
            'user' => 'robotnik',
            'pass' => '',
        ]);
    }

    public function testMissingPassValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: pass");
        new MysqlConfiguration([
            'host' => 'hosty',
            'dbname' => 'dbdbdb',
            'user' => 'robotnik',
        ]);
    }

    public function testMultipleMissingValues()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: dbname, user, pass");
        new MysqlConfiguration([
            'host' => 'hosty',
        ]);
    }

    public function testItReturnsValuesByKey()
    {
        $config = new MysqlConfiguration([
            'host' => 'hosty',
            'dbname' => 'dbdbdb',
            'user' => 'robotnik',
            'pass' => 'OHMYGEEEERD',
            'port' => '3306',
            'query' => 'SELECT 1 FROM NOWEHERE;'
        ]);

        $this->assertEquals('hosty', $config->get('host'));
        $this->assertEquals('dbdbdb', $config->get('dbname'));
        $this->assertEquals('robotnik', $config->get('user'));
        $this->assertEquals('OHMYGEEEERD', $config->get('pass'));
        $this->assertEquals('3306', $config->get('port'));
        $this->assertEquals('SELECT 1 FROM NOWEHERE;', $config->get('query'));
    }

    public function testItReturnsTheFullConfigurationAsArray()
    {
        $raw = [
            'host' => 'hosty',
            'dbname' => 'dbdbdb',
            'user' => 'robotnik',
            'pass' => 'OHMYGEEEERD',
            'port' => '3306',
            'query' => 'SELECT 1 FROM NOWEHERE;',
            'timeout' => 3,
        ];

        $config = new MysqlConfiguration($raw);

        $this->assertEquals($raw, $config->asArray());
    }

    /*
     * Both of the below tests validate that the decorator allows the values to be overridden from config
     * @see testItReturnsValuesByKey
     * @see testItReturnsTheFullConfigurationAsArray
     */
    public function testDecorator()
    {
        $input = [
            'host' => 'hosty',
            'dbname' => 'dbdbdb',
            'user' => 'robotnik',
            'pass' => 'OHMYGEEEERD',
        ];

        $expected = array_merge($input, [
            'port' => null,
            'query' => 'SELECT 1;',
            'timeout' => 3,
        ]);

        $config = new MysqlConfiguration($input);

        $this->assertEquals($expected, $config->asArray());
    }
}
