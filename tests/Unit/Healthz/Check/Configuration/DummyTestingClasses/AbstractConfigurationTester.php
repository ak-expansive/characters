<?php

namespace Tests\Unit\Healthz\Check\Configuration\DummyTestingClasses;

use Contexts\Healthz\Check\Configuration\AbstractConfiguration;

class AbstractConfigurationTester extends AbstractConfiguration
{
    /**
     * Returns the type of the check.
     *
     * @return string
     */
    public static function type(): string
    {
        return 'config-tester';
    }
}