<?php

namespace Tests\Unit\Healthz\Check\Configuration\DummyTestingClasses;

use Tests\Unit\Healthz\Check\Configuration\DummyTestingClasses\AbstractConfigurationTester;

class AbstractConfigurationTesterWithBadGuard extends AbstractConfigurationTester
{
    /**
     * Purposely just throw an exception to test it is used in the constructor.
     *
     * @return void
     */
    protected function guard(array $config)
    {
        throw new \InvalidArgumentException("every time");
    }
}