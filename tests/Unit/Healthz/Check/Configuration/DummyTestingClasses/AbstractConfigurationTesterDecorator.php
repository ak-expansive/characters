<?php

namespace Tests\Unit\Healthz\Check\Configuration\DummyTestingClasses;

use Tests\Unit\Healthz\Check\Configuration\DummyTestingClasses\AbstractConfigurationTester;

class AbstractConfigurationTesterDecorator extends AbstractConfigurationTester
{
    /**
     * Add a key to the config, just to check the decorator is run.
     *
     * @return void
     */
    protected function decorate(array $config): array
    {
        $config['__decorate'] = 'decorated';
        return $config;
    }
}