<?php

namespace Tests\Unit\Healthz\Check\Configuration;

use Tests\Cases\Unit;
use Contexts\Healthz\Check\Configuration\RedisConfiguration;
use Contexts\Healthz\Check\Configuration\AbstractConfiguration;
use Contexts\Healthz\Check\Configuration\ConfigurationInterface;
use Contexts\Healthz\Exception\InvalidConfigurationException;

class RedisConfigurationTest extends Unit
{
    public function testSuccessfulConstructon()
    {
        $config = new RedisConfiguration([
            'host' => 'localhost',
            'port' => '6379',
            'password' => 'testy',
            'database' => 0,
        ]);

        $this->assertInstanceOf(RedisConfiguration::class, $config);
        $this->assertInstanceOf(AbstractConfiguration::class, $config);
        $this->assertInstanceOf(ConfigurationInterface::class, $config);
    }

    public function testEmptyHostValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: host");
        new RedisConfiguration([
            'host' => '',
            'port' => '6379',
            'password' => 'testy',
            'database' => 0,
        ]);
    }

    public function testMissingHostValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: host");
        new RedisConfiguration([
            'port' => '6379',
            'password' => 'testy',
            'database' => 0,
        ]);
    }

    public function testEmptyPortValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: port");
        new RedisConfiguration([
            'host' => 'localhost',
            'port' => '',
            'password' => 'testy',
            'database' => 0,
        ]);
    }

    public function testMissingPortValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: port");
        new RedisConfiguration([
            'host' => 'localhost',
            'password' => 'testy',
            'database' => 0,
        ]);
    }

    public function testEmptyPasswordValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: password");
        new RedisConfiguration([
            'host' => 'hosty',
            'port' => '6379',
            'password' => '',
            'database' => 0,
        ]);
    }

    public function testMissingPasswordValue()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: password");
        new RedisConfiguration([
            'host' => 'hosty',
            'port' => '6379',
            'database' => 0,
        ]);
    }

    public function testMultipleMissingValues()
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage("Missing configuration keys: port, password");
        new RedisConfiguration([
            'host' => 'hosty',
            'database' => 0,
        ]);
    }

    public function testItReturnsValuesByKey()
    {
        $config = new RedisConfiguration([
            'host' => 'localhost',
            'port' => '6379',
            'password' => 'OHMYGEEEERD',
            'database' => 0,
        ]);

        $this->assertEquals('localhost', $config->get('host'));
        $this->assertEquals('6379', $config->get('port'));
        $this->assertEquals('OHMYGEEEERD', $config->get('password'));
        $this->assertEquals(3.0, $config->get('timeout'));
        $this->assertEquals(0, $config->get('database'));
    }

    public function testItReturnsTheFullConfigurationAsArray()
    {
        $raw = [
            'host' => 'localhost',
            'port' => '6379',
            'password' => 'testy',
            'timeout' => 2.0,
            'database' => 0,
        ];

        $config = new RedisConfiguration($raw);

        $this->assertEquals($raw, $config->asArray());
    }

    /*
     * Both of the below tests validate that the decorator allows the values to be overridden from config
     * @see testItReturnsValuesByKey
     * @see testItReturnsTheFullConfigurationAsArray
     */
    public function testDecorator()
    {
        $input = [
            'host' => 'localhost',
            'port' => '6379',
            'password' => 'testy',
            'database' => 0,
        ];

        $expected = array_merge($input, [
            'timeout' => 3.0,
        ]);

        $config = new RedisConfiguration($input);

        $this->assertEquals($expected, $config->asArray());
    }
}
