<?php

namespace Tests\Unit\Healthz\Check;

use Tests\Cases\Unit;
use Contexts\Healthz\Check\AbstractCheck;
use Contexts\Healthz\Check\Result\Result;
use Contexts\Healthz\Check\Result\ResultBuilder;
use Contexts\Healthz\Check\Result\ResultInterface;
use Contexts\Healthz\Check\Configuration\AbstractConfiguration;

class AbstractCheckTester extends AbstractCheck
{
    public function run(): ResultInterface
    {
        return $this->resultBuilder->build($this->config);
    }

    public static function requiredConfiguration(): string
    {
        return 'config-path';
    }

    public static function requiredBuilder(): string
    {
        return 'builder-path';
    }
}

class AbstractCheckTest extends Unit
{
    public function testSuccessfulConstructon()
    {
        $mockResult = $this->createMock(Result::class);
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(0))
            ->method('build')
            ->willReturn(null);

        $check = new AbstractCheckTester($mockConfig, $mockResultBuilder);
    }

    public function testItReturnsTheCorrectResult()
    {
        $mockResult = $this->createMock(Result::class);
        $mockConfig = $this->createMock(AbstractConfiguration::class);
        $mockResultBuilder = $this->createMock(ResultBuilder::class);
        $mockResultBuilder->expects($this->exactly(1))
            ->method('build')
            ->with($mockConfig)
            ->willReturn($mockResult);

        $check = new AbstractCheckTester($mockConfig, $mockResultBuilder);
        $this->assertEquals($mockResult, $check->run());
    }
}
