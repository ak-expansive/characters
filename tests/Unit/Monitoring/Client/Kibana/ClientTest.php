<?php

namespace Tests\Unit\Monitoring\Client\Kibana;

use RuntimeException;
use Tests\Cases\Unit;
use Contexts\Core\Exceptions\Handler;
use Psr\Http\Message\ResponseInterface;
use Contexts\Monitoring\Client\Kibana\Client;
use GuzzleHttp\ClientInterface as HttpClientInterface;

class ClientTest extends Unit
{
    public function testTheConfigCanBeRetrievedFromTheHTTPClient()
    {
        $config = ['key' => 'value'];
        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('getConfig')
            ->willReturn($config);

        $subj = new Client($mockHTTPClient);
        $this->assertSame($config, $subj->getConfig());
    }

    public function testIsReadyReturnsTrueWhenTheHealthcheckEndpointResponds200()
    {
        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(200);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('GET', Client::STATUS_URI)
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertTrue($subj->isReady());
    }

    public function testIsReadyReturnsFalseWhenTheHealthcheckEndpointRespondsNon200()
    {
        // Just test some examples
        $responseCodes = [
            201,
            300,
            400,
            500
        ];
        foreach ($responseCodes as $responseCode) {
            $mockResponse = $this->createMock(ResponseInterface::class);
            $mockResponse->expects($this->exactly(1))
                ->method('getStatusCode')
                ->willReturn($responseCode);
    
            $mockHTTPClient = $this->createMock(HttpClientInterface::class);
            $mockHTTPClient->expects($this->exactly(1))
                ->method('request')
                ->with('GET', Client::STATUS_URI)
                ->willReturn($mockResponse);
    
            $subj = new Client($mockHTTPClient);
            $this->assertFalse($subj->isReady());
        }
    }

    public function testIsReadyReturnsFalseWhenHTTPClientThrowsException()
    {
        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('GET', Client::STATUS_URI)
            ->willThrowException(new RuntimeException('Bad request.'));

        $subj = new Client($mockHTTPClient);
        $this->assertFalse($subj->isReady());
    }

    public function testIsReadyExceptionIsReportedWhenHTTPClientThrowsException()
    {
        $exception = new RuntimeException('Bad request.');

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('GET', Client::STATUS_URI)
            ->willThrowException($exception);

        $mockExceptionHandler = $this->createMock(Handler::class);
        $mockExceptionHandler->expects($this->exactly(1))
            ->method('report')
            ->with($exception);

        $subj = new Client($mockHTTPClient, $mockExceptionHandler);
        $this->assertFalse($subj->isReady());
    }

    public function testSuccessfulDashboardOverwrite()
    {
        $id = '8222c0c1-2215-4108-b7bc-e118d7bf4581';
        $body = ['some_key' => 'some_val'];

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(200);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/dashboard/$id?overwrite=true", ['json' => $body])
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertTrue($subj->createDashboard(
            $body,
            $id, 
            true
        ));
    }

    public function testSuccessfulDashboardSaveWithoutOverwrite()
    {
        $id = '8222c0c1-2215-4108-b7bc-e118d7bf4581';
        $body = ['some_key' => 'some_val'];

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(200);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/dashboard/$id", ['json' => $body])
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertTrue($subj->createDashboard(
            $body,
            $id,
            false
        ));
    }

    public function testSuccessfulDashboardSaveWithoutIDButWithOverwrite()
    {
        $body = ['some_key' => 'some_val'];

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(200);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/dashboard?overwrite=true", ['json' => $body])
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertTrue($subj->createDashboard(
            $body,
            null,
            true
        ));
    }

    public function testSuccessfulDashboardSaveWithoutIDOrOverwrite()
    {
        $body = ['some_key' => 'some_val'];

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(200);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/dashboard", ['json' => $body])
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertTrue($subj->createDashboard(
            $body,
            null,
            false
        ));
    }

    public function testDashboardOverwriteThatThrowsException()
    {
        $exception = new RuntimeException('Bad request.');
        $id = '8222c0c1-2215-4108-b7bc-e118d7bf4581';
        $body = ['some_key' => 'some_val'];

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/dashboard/$id?overwrite=true", ['json' => $body])
            ->willThrowException($exception);

        $subj = new Client($mockHTTPClient);
        $this->assertFalse($subj->createDashboard(
            $body,
            $id, 
            true
        ));
    }

    public function testDashboardOverwriteWithNon200Response()
    {
        $id = '8222c0c1-2215-4108-b7bc-e118d7bf4581';
        $body = ['some_key' => 'some_val'];

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(400);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/dashboard/$id?overwrite=true", ['json' => $body])
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertFalse($subj->createDashboard(
            $body,
            $id, 
            true
        ));
    }

    public function testDashboardOverwriteThatThrowsExceptionIsReported()
    {
        $exception = new RuntimeException('Bad request.');
        $id = '8222c0c1-2215-4108-b7bc-e118d7bf4581';
        $body = ['some_key' => 'some_val'];

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/dashboard/$id?overwrite=true", ['json' => $body])
            ->willThrowException($exception);


        $mockExceptionHandler = $this->createMock(Handler::class);
        $mockExceptionHandler->expects($this->exactly(1))
            ->method('report')
            ->with($exception);

        $subj = new Client($mockHTTPClient, $mockExceptionHandler);
        $this->assertFalse($subj->createDashboard(
            $body,
            $id, 
            true
        ));
    }

    public function testSuccessfulSearchOverwrite()
    {
        $id = '8222c0c1-2215-4108-b7bc-e118d7bf4581';
        $body = ['some_key' => 'some_val'];

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(200);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/search/$id?overwrite=true", ['json' => $body])
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertTrue($subj->createSearch(
            $body,
            $id, 
            true
        ));
    }

    public function testSuccessfulSearchSaveWithoutOverwrite()
    {
        $id = '8222c0c1-2215-4108-b7bc-e118d7bf4581';
        $body = ['some_key' => 'some_val'];

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(200);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/search/$id", ['json' => $body])
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertTrue($subj->createSearch(
            $body,
            $id,
            false
        ));
    }

    public function testSuccessfulSearchSaveWithoutIDButWithOverwrite()
    {
        $body = ['some_key' => 'some_val'];

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(200);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/search?overwrite=true", ['json' => $body])
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertTrue($subj->createSearch(
            $body,
            null,
            true
        ));
    }

    public function testSuccessfulSearchSaveWithoutIDOrOverwrite()
    {
        $body = ['some_key' => 'some_val'];

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(200);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/search", ['json' => $body])
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertTrue($subj->createSearch(
            $body,
            null,
            false
        ));
    }

    public function testSearchOverwriteThatThrowsException()
    {
        $exception = new RuntimeException('Bad request.');
        $id = '8222c0c1-2215-4108-b7bc-e118d7bf4581';
        $body = ['some_key' => 'some_val'];

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/search/$id?overwrite=true", ['json' => $body])
            ->willThrowException($exception);

        $subj = new Client($mockHTTPClient);
        $this->assertFalse($subj->createSearch(
            $body,
            $id, 
            true
        ));
    }

    public function testSearchOverwriteWithNon200Response()
    {
        $id = '8222c0c1-2215-4108-b7bc-e118d7bf4581';
        $body = ['some_key' => 'some_val'];

        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockResponse->expects($this->exactly(1))
            ->method('getStatusCode')
            ->willReturn(400);

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/search/$id?overwrite=true", ['json' => $body])
            ->willReturn($mockResponse);

        $subj = new Client($mockHTTPClient);
        $this->assertFalse($subj->createSearch(
            $body,
            $id, 
            true
        ));
    }

    public function testSearchOverwriteThatThrowsExceptionIsReported()
    {
        $exception = new RuntimeException('Bad request.');
        $id = '8222c0c1-2215-4108-b7bc-e118d7bf4581';
        $body = ['some_key' => 'some_val'];

        $mockHTTPClient = $this->createMock(HttpClientInterface::class);
        $mockHTTPClient->expects($this->exactly(1))
            ->method('request')
            ->with('post', "/api/saved_objects/search/$id?overwrite=true", ['json' => $body])
            ->willThrowException($exception);


        $mockExceptionHandler = $this->createMock(Handler::class);
        $mockExceptionHandler->expects($this->exactly(1))
            ->method('report')
            ->with($exception);

        $subj = new Client($mockHTTPClient, $mockExceptionHandler);
        $this->assertFalse($subj->createSearch(
            $body,
            $id, 
            true
        ));
    }
}