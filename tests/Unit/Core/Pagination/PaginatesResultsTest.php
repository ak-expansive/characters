<?php

namespace Tests\Unit\Core\Pagination;

use Tests\Cases\Unit;
use Contexts\Core\Pagination\PaginatesResults;

class PaginatesResultsTest extends Unit
{
    public function testValuesCanBeSetFromArray()
    {
        $queryParams = [
            'page' => 4,
            'page_size' => 6,
        ];
        $subj = $this->getMockForTrait(PaginatesResults::class);
        $this->callProtectedMethodOnSubject($subj, 'setPaginationValues', $queryParams);

        $this->assertEquals($queryParams['page'], $subj->getPage());
        $this->assertEquals($queryParams['page_size'], $subj->getPageSize());
    }

    public function testDefaultsAreSetCorrectly()
    {
        $subj = $this->getMockForTrait(PaginatesResults::class);
        $this->callProtectedMethodOnSubject($subj, 'setPaginationValues', []);
        $defaultPageSize = $this->callProtectedMethodOnSubject($subj, 'getDefaultPageSize');

        $this->assertEquals(1, $subj->getPage());
        $this->assertEquals($defaultPageSize, $subj->getPageSize());
    }

    public function testSerializationTypecastingForNumericStrings()
    {
        $queryParams = [
            'page' => '4',
            'page_size' => '6',
        ];
        $subj = $this->getMockForTrait(PaginatesResults::class);
        $this->callProtectedMethodOnSubject($subj, 'setPaginationValues', $queryParams);
        $serialized = $this->callProtectedMethodOnSubject($subj, 'serializePaginationForValidation');

        $this->assertIsInt($serialized['page']);
        $this->assertEquals($serialized['page'], 4);
        $this->assertIsInt($serialized['page_size']);
        $this->assertEquals($serialized['page_size'], 6);
    }

    /**
     * We wanna rely on the validation to display the problems to the user.
     * We don't wanna throw any errors on setting, but just keep the values.
     * If they're numeric however, we can help them out.
     * Usually these values come query params and will always be strings that are numeric.
     *
     * @return void
     */
    public function testSerializationIsNotTypecastForNonNumericStrings()
    {
        $queryParams = [
            'page' => 'blah',
            'page_size' => 'meh',
        ];
        $subj = $this->getMockForTrait(PaginatesResults::class);
        $this->callProtectedMethodOnSubject($subj, 'setPaginationValues', $queryParams);
        $serialized = $this->callProtectedMethodOnSubject($subj, 'serializePaginationForValidation');

        $this->assertIsString($serialized['page']);
        $this->assertEquals($serialized['page'], $queryParams['page']);
        $this->assertIsString($serialized['page_size']);
        $this->assertEquals($serialized['page_size'], $queryParams['page_size']);
    }

    /**
     * The key for the value in the result of serialization needs to be the same as the key of the rules in the validation constraints.
     *
     * @return void
     */
    public function testThatConstraintsAndSerializationCoalesce()
    {
        $queryParams = [
            'page' => '4',
            'page_size' => '6',
        ];
        $subj = $this->getMockForTrait(PaginatesResults::class);
        $this->callProtectedMethodOnSubject($subj, 'setPaginationValues', $queryParams);
        $constraints = $this->callProtectedMethodOnSubject($subj, 'getPaginationValidationConstraints');
        $serialized = $this->callProtectedMethodOnSubject($subj, 'serializePaginationForValidation');

        $this->assertSame([
            'page' => 4,
            'page_size' => 6,
        ], $serialized);
        $this->assertArrayHasKey('page', $constraints);
        $this->assertArrayHasKey('page_size', $constraints);
    }

    public function testThatTheMaxPageSizeIsGreaterThanTheDefaultPageSize()
    {
        $subj = $this->getMockForTrait(PaginatesResults::class);
        $max = $this->callProtectedMethodOnSubject($subj, 'getMaxPageSize');
        $default = $this->callProtectedMethodOnSubject($subj, 'getDefaultPageSize');

        $this->assertGreaterThanOrEqual($default, $max);
    }
}