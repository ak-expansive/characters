<?php

namespace Tests\Unit\Core\Util;

use Tests\Cases\Unit;
use Contexts\Core\Util\Slug;

class SlugTest extends Unit
{
    public function testValidRegexMatches()
    {
        $shouldMatch = [
            'blah-meh-hey',
            'wonfewnf',
            'wo-bol',
            '9-37493-bdj',
        ];

        foreach ($shouldMatch as $arg) {
            $this->assertRegExp(Slug::REGEX, $arg);
        }
    }

    public function testInvalidRegexMatches()
    {
        $shouldMatch = [
            '-wf-wef',
            'wonfewnf-',
            'WJM-WFN',
            'wenf-nFc',
            'wefin--fwef',
            'dh$',
            'fwin*woknw-wfi',
            'wifn_weno'
        ];

        foreach ($shouldMatch as $arg) {
            $this->assertNotRegExp(Slug::REGEX, $arg);
        }
    }
}