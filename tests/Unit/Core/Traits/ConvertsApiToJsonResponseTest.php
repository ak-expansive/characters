<?php

namespace Tests\Unit\Core\Traits;

use Tests\Cases\Unit;
use Illuminate\Http\JsonResponse;
use Contexts\ApiSerialization\Response;
use Contexts\Core\Traits\ConvertsApiToJsonResponse;

class ConvertsApiToJsonResponseTest extends Unit
{
    public function testItConvertsApiResponseToJsonResponse()
    {
        $data = ['some_key' => 'some_value'];
        $mockResponse = $this->createMock(Response::class);
        $mockResponse->expects($this->exactly(1))
            ->method('serialize')
            ->willReturn($data);
        $mockResponse->expects($this->exactly(1))
            ->method('statusCode')
            ->willReturn(200);
        $subj = $this->getMockForTrait(ConvertsApiToJsonResponse::class);

        $output = $this->callProtectedMethodOnSubject(
            $subj,
            'toJsonResponse',
            $mockResponse
        );

        $this->assertInstanceOf(JsonResponse::class, $output);
        $this->assertSame($output->getData(true), $data);
        $this->assertSame($output->getStatusCode(), 200);
    }
}