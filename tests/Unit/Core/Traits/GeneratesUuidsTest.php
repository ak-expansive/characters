<?php

namespace Tests\Unit\Core\Traits;

use Tests\Cases\Unit;
use Ramsey\Uuid\Rfc4122\UuidV4;
use Contexts\Core\Traits\GeneratesUuids;

class GeneratesUuidsTest extends Unit
{
    public function testItGeneratesAUuidV4()
    {
        $subj = $this->getMockForTrait(GeneratesUuids::class);

        $this->assertInstanceOf(
            UuidV4::class, 
            $this->callProtectedMethodOnSubject(
                $subj,
                'generateUuid'
            )
        );
    }

    public function testItGeneratesAStringableObject()
    {
        $subj = $this->getMockForTrait(GeneratesUuids::class);

        $uuid = $this->callProtectedMethodOnSubject(
            $subj,
            'generateUuid'
        );

        $this->assertIsString((string) $uuid);
    }

    public function testWhenStringifiedItGivesAValidUuidV4()
    {
        $subj = $this->getMockForTrait(GeneratesUuids::class);

        $uuid = (string) $this->callProtectedMethodOnSubject(
            $subj,
            'generateUuid'
        );

        $this->assertRegexp('/^[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}$/i', $uuid);
    }
}