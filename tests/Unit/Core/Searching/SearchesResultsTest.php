<?php

namespace Tests\Unit\Core\Searching;

use Tests\Cases\Unit;
use Contexts\Core\Searching\SearchesResults;

class SearchesResultsTest extends Unit
{
    public function testSearchCanBeSetFromArray()
    {
        $searchString = 'looking for something';
        $subj = $this->getMockForTrait(SearchesResults::class);
        $this->callProtectedMethodOnSubject($subj, 'setSearch', ['search' => $searchString]);

        $this->assertEquals($searchString, $subj->getSearch());
    }

    public function testIsSearchIsNotInArrayItWillBeNull()
    {
        $subj = $this->getMockForTrait(SearchesResults::class);
        $this->callProtectedMethodOnSubject($subj, 'setSearch', ['blah' => 'meh']);

        $this->assertNull($subj->getSearch());
    }

    /**
     * The key for the value in the result of serialization needs to be the same as the key of the rules in the validation constraints.
     *
     * @return void
     */
    public function testThatConstraintsAndSerializationCoalesce()
    {
        $searchString = 'meh';
        $subj = $this->getMockForTrait(SearchesResults::class);
        $this->callProtectedMethodOnSubject($subj, 'setSearch', ['search' => $searchString]);
        $constraints = $this->callProtectedMethodOnSubject($subj, 'getSearchValidationConstraints');
        $serialized = $this->callProtectedMethodOnSubject($subj, 'serializeSearchForValidation');

        $this->assertSame(['search' => $searchString], $serialized);
        $this->assertArrayHasKey('search', $constraints);
    }
}