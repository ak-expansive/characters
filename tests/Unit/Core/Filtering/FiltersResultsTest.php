<?php

namespace Tests\Unit\Core\Filtering;

use Tests\Cases\Unit;
use Contexts\Core\Filtering\FiltersResults;
use Symfony\Component\Validator\Constraints\Collection;

class FiltersResultsTest extends Unit
{
    public function testFilterCanBeSetFromArrayAndAreSerializedByUsingClass()
    {
        $queryParams = [
            'filter' => [
                'field' => 'value',
            ],
        ];
        $serializedFields = ['serialized_field' => 'serialized_value'];
        $subj = $this->getMockForTrait(FiltersResults::class);
        $subj->expects($this->exactly(1))
            ->method('serializeFilterFieldsForValidation')
            ->willReturn($serializedFields);

        $this->callProtectedMethodOnSubject($subj, 'setFilters', $queryParams);

        $this->assertEquals($serializedFields, $subj->getFilters());
    }

    public function testThatConstraintsAndSerializationCoalsesce()
    {
        $queryParams = [
            'filter' => [
                'field' => 'value',
            ],
        ];

        $validationConstraints = [
            'serialized_field' => [],
        ];

        $serializedFields = [
            'serialized_field' => 'serialized_value'
        ];

        $subj = $this->getMockForTrait(FiltersResults::class);
        $subj->expects($this->exactly(1))
            ->method('serializeFilterFieldsForValidation')
            ->willReturn($serializedFields);

        $subj->expects($this->exactly(1))
            ->method('getFilterFieldValidationConstraints')
            ->willReturn($validationConstraints);

        $this->callProtectedMethodOnSubject($subj, 'setFilters', $queryParams);
        $constraints = $this->callProtectedMethodOnSubject($subj, 'getFilterValidationConstraints');
        $serialized = $this->callProtectedMethodOnSubject($subj, 'serializeFilterForValidation');

        $this->assertEquals($serialized, [
            'filter' => $serializedFields
        ]);
        $this->assertArrayHasKey('filter', $constraints);
        $this->assertInstanceOf(Collection::class, $constraints['filter']);
        $this->assertArrayHasKey('serialized_field', $constraints['filter']->fields);
    }
}