<?php

namespace Tests\Unit\ElasticSearch\Documents;

use Tests\Cases\Unit;
use InvalidArgumentException;
use Contexts\ElasticSearch\Documents\IdentifiableDocument;

class IdentifiableDocumentTest extends Unit
{
    public function testTheSameIdIsReturnedThatItWasCreatedWith()
    {
        $id = '32cda6ce-235a-446f-81d3-f42e4a469951';
        $subj = IdentifiableDocument::withBody($id);
        $this->assertEquals($id, $subj->getId());
    }

    public function testTheSameBodyIsReturnedThatItWasCreatedWith()
    {
        $id = '32cda6ce-235a-446f-81d3-f42e4a469951';
        $body = ['some_key' => 'some_value'];
        $subj = IdentifiableDocument::withBody($id, $body);
        $this->assertSame($body, $subj->getBody());
    }

    public function testAccessingProperty()
    {
        $id = '32cda6ce-235a-446f-81d3-f42e4a469951';
        $body = ['some_key' => 'some_value'];
        $subj = IdentifiableDocument::withBody($id, $body);
        $this->assertEquals('some_value', $subj->get('[some_key]'));
    }

    public function testAccessingMissingProperty()
    {
        $id = '32cda6ce-235a-446f-81d3-f42e4a469951';
        $body = ['some_key' => 'some_value'];
        $subj = IdentifiableDocument::withBody($id, $body);

        $this->expectException(InvalidArgumentException::class);
        $subj->get('[blah]');
    }

    public function testAccessingMissingPropertyWithoutThrowing()
    {
        $id = '32cda6ce-235a-446f-81d3-f42e4a469951';
        $body = ['some_key' => 'some_value'];
        $subj = IdentifiableDocument::withBody($id, $body);

        $this->assertNull($subj->get('[blah]', false));
    }

    public function testAccessingNestedProperty()
    {
        $id = '32cda6ce-235a-446f-81d3-f42e4a469951';
        $body = [
            'first' => [
                'second' => 'some_value',
            ],
        ];
        $subj = IdentifiableDocument::withBody($id, $body);

        $this->assertEquals('some_value', $subj->get('[first][second]'));
    }
}