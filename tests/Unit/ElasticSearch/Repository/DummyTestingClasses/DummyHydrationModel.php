<?php

namespace Tests\Unit\ElasticSearch\Repository\DummyTestingClasses;

use Contexts\ElasticSearch\Documents\IdentifiableDocument;
use Contexts\ElasticSearch\Documents\DocumentableModelInterface;

class DummyHydrationModel implements DocumentableModelInterface
{
    public $id;
    public $field1;
    public $field2;

    public function toDocument(): IdentifiableDocument
    {
        return IdentifiableDocument::withBody('id', []);
    }

    public static function fromDocument(IdentifiableDocument $document): self
    {
        $new = new self();
        $new->id = $document->getId();
        $new->field1 = $document->get('[field_1]');
        $new->field2 = $document->get('[field_2]');

        return $new;
    }
}