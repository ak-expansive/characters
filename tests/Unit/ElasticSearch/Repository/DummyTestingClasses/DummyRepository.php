<?php

namespace Tests\Unit\ElasticSearch\Repository\DummyTestingClasses;

use Elasticsearch\Client;
use Contexts\ElasticSearch\Repository\Meta\Meta;
use Contexts\ElasticSearch\Repository\ElasticSearchRepository;

class DummyRepository implements ElasticSearchRepository
{
    public function __construct(Client $client, Meta $meta)
    {

    }

    public function createIndex(?string $indexName = null)
    {

    }

    public function deleteIndex(?string $indexName = null)
    {

    }

    public function truncate(?string $indexName = null)
    {

    }

    public function updateMappings(?string $indexName = null)
    {
        
    }
}