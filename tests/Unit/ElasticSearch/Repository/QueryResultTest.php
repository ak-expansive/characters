<?php

namespace Tests\Unit\ElasticSearch\Repository;

use DateTime;
use RuntimeException;
use Tests\Cases\Unit;
use Contexts\ElasticSearch\Repository\QueryResult;
use Contexts\ElasticSearch\Exception\QueryResultException;

class QueryResultTest extends Unit
{
    public function testPageCanBeSetAndGet()
    {
        $subj = new QueryResult;

        $this->assertEquals(null, $subj->getPage());
        $subj->setPage(3);
        $this->assertEquals(3, $subj->getPage());
    }

    public function testPageSizeCanBeSetAndGet()
    {
        $subj = new QueryResult;

        $this->assertEquals(null, $subj->getPageSize());
        $subj->setPageSize(3);
        $this->assertEquals(3, $subj->getPageSize());
    }

    public function testIndexTotalCanBeSetAndGet()
    {
        $subj = new QueryResult;

        $this->assertEquals(null, $subj->getIndexTotal());
        $subj->setIndexTotal(3);
        $this->assertEquals(3, $subj->getIndexTotal());
    }

    public function testTotalPagesCanBeCalculated()
    {
        $subj = new QueryResult;

        $subj->setIndexTotal(13);
        $subj->setPageSize(5);
        $this->assertEquals(3, $subj->getTotalPages());

        $subj->setIndexTotal(15);
        $subj->setPageSize(5);
        $this->assertEquals(3, $subj->getTotalPages());

        $subj->setIndexTotal(16);
        $subj->setPageSize(5);
        $this->assertEquals(4, $subj->getTotalPages());
    }

    public function testTotalPagesCalculationThrowsExceptionIfPageSizeNotSet()
    {
        $subj = new QueryResult;
        $subj->setIndexTotal(13);

        $this->expectException(RuntimeException::class);
        $subj->getTotalPages();
    }

    public function testTotalPagesCalculationThrowsExceptionIfIndexTotalNotSet()
    {
        $subj = new QueryResult;
        $subj->setPageSize(13);

        $this->expectException(RuntimeException::class);
        $subj->getTotalPages();
    }

    public function testSingularResultCanBeSetAndGet()
    {
        $subj = new QueryResult;

        $this->assertNull($subj->getSingularResult());

        $dt = new DateTime;
        $subj->setSingularResult($dt); // The class itself should be irrelevant
        $this->assertSame($dt, $subj->getSingularResult());
    }

    public function testResultCollectionCanBeSetAndGet()
    {
        $subj = new QueryResult;

        $dt = new DateTime;
        $dt2 = new DateTime;
        $dt3 = new DateTime;
        $subj->setResultCollection($dt, $dt2, $dt3); // The class itself should be irrelevant
        $this->assertSame([$dt, $dt2, $dt3], $subj->getResultCollection());
    }

    public function testExceptionIsThrownWhenSettingCollectionAfterSingular()
    {
        $subj = new QueryResult;

        $dt = new DateTime;
        $subj->setSingularResult($dt);

        $dt1 = new DateTime;
        $dt2 = new DateTime;
        $dt3 = new DateTime;
        
        $this->expectException(QueryResultException::class);
        $subj->setResultCollection($dt1, $dt2, $dt3); // The class itself should be irrelevant
        
    }

    public function testExceptionIsThrownWhenSettingSingularAfterCollection()
    {
        $subj = new QueryResult;

        $dt1 = new DateTime;
        $dt2 = new DateTime;
        $dt3 = new DateTime;        
        $subj->setResultCollection($dt1, $dt2, $dt3); // The class itself should be irrelevant

        $dt = new DateTime;
        $this->expectException(QueryResultException::class);
        $subj->setSingularResult($dt);
    }

    public function testAllSettersReturnSelf()
    {
        $subj = new QueryResult;
        $this->assertSame($subj, $subj->setPage(1));
        $this->assertSame($subj, $subj->setPageSize(1));
        $this->assertSame($subj, $subj->setIndexTotal(1));
        $this->assertSame($subj, $subj->setSingularResult(['some_key' => 'some_value']));

        // The test above shows we will throw an exception if calling setSingularResult and setResultCollection on the same object
        $subj = new QueryResult;
        $this->assertSame($subj, $subj->setResultCollection(['some_key' => 'some_value'], ['other_key' => 'other_value']));
    }
}