<?php

namespace Tests\Unit\ElasticSearch\Repository\Meta\Finder;

use Tests\Cases\Unit;
use Symfony\Component\Finder\Finder;
use Contexts\ElasticSearch\Repository\Meta\Finder\MetaFinderProvider;
use Contexts\ElasticSearch\Repository\Meta\Finder\DefaultMetaFinderProvider;

class DefaultMetaFinderProviderTest extends Unit
{
    public function testItCanBeMadeAndImplementsRequiredInterface()
    {
        $subj = DefaultMetaFinderProvider::make(__DIR__ . '/search_dir');
        $this->assertInstanceOf(DefaultMetaFinderProvider::class, $subj);
        $this->assertInstanceOf(MetaFinderProvider::class, $subj);
    }

    public function testItCanBeMadeAndReturnsAFinder()
    {
        $subj = DefaultMetaFinderProvider::make(__DIR__ . '/search_dir');
        $this->assertInstanceOf(DefaultMetaFinderProvider::class, $subj);

        $this->assertInstanceOf(Finder::class, $subj->getFinder());
    }

    public function testTheFinderFoundTheRightFiles()
    {
        $subj = DefaultMetaFinderProvider::make(__DIR__ . '/search_dir');
        $this->assertInstanceOf(DefaultMetaFinderProvider::class, $subj);

        $this->assertInstanceOf(Finder::class, $subj->getFinder());

        $finder = $subj->getFinder();

        $foundFiles = [];

        foreach ($finder as $filepath) {
            $foundFiles[] = str_replace(__DIR__, '', $filepath->getPath()) . $filepath->getFilename();
        }

        $this->assertCount(3, $foundFiles);
        $this->assertContains(
            "/search_dir/subdir/subsubdirsubsubdir-model-mapping.es.meta.php",
            $foundFiles
        );
        $this->assertContains(
            "/search_dirsome-model-mapping.es.meta.php",
            $foundFiles
        );
        $this->assertContains(
            "/search_dirother-model-mapping.es.meta.php",
            $foundFiles
        );
    }
}