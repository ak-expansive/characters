<?php

namespace Tests\Unit\ElasticSearch\Repository\Meta;

use DateTime;
use Tests\Cases\Unit;
use Contexts\ElasticSearch\Repository\Meta\Meta;
use Contexts\ElasticSearch\Repository\ElasticSearchRepository;
use Contexts\ElasticSearch\Exception\CouldNotBuildMetaException;

class MetaTest extends Unit
{
    public function testThatRepositoryKeyIsRequiredForConstruct()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        $this->expectExceptionMessage('The following keys must be provided: repository');
        Meta::fromArray([
            'model' => '\Some\Fake\ClassPath',
            'indexName' => 'meh',
        ]);
    }

    public function testThatModelKeyIsRequiredForConstruct()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        $this->expectExceptionMessage('The following keys must be provided: model');
        Meta::fromArray([
            'repository' => '\Some\Fake\ClassPath',
            'indexName' => 'meh',
        ]);
    }

    public function testThatIndexNameKeyIsRequiredForConstruct()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        $this->expectExceptionMessage('The following keys must be provided: indexName');
        Meta::fromArray([
            'repository' => '\Some\Fake\ClassPath',
            'model' => '\Some\Fake\ClassPath',
        ]);
    }

    public function testThatAllMissingKeysAreReported()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        $this->expectExceptionMessage('The following keys must be provided: model, indexName');
        Meta::fromArray([
            'repository' => '\Some\Fake\ClassPath',
        ]);
    }

    public function testThatModelClassMustExist()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        $this->expectExceptionMessage("Class '\Some\Fake\ClassPath' does not exist.");
        Meta::fromArray([
            'model' => '\Some\Fake\ClassPath',
            'repository' => '\Some\Fake\ClassPath',
            'indexName' => 'meh',
        ]);
    }

    public function testThatRepositoryClassMustExist()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        $this->expectExceptionMessage("Class '\Some\Fake\ClassPath' does not exist.");
        Meta::fromArray([
            'model' => DateTime::class,
            'repository' => '\Some\Fake\ClassPath',
            'indexName' => 'meh',
        ]);
    }

    public function testThatIndexNameMustoNotBeEmpty()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        $this->expectExceptionMessage("The index name is invalid: no value is set");
        Meta::fromArray([
            'model' => DateTime::class,
            'repository' => '\Some\Fake\ClassPath',
            'indexName' => '',
        ]);
    }

    public function testThatIndexNameMustBeValidString()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        $this->expectExceptionMessage("The index name is invalid: value is not a string");
        Meta::fromArray([
            'model' => DateTime::class,
            'repository' => '\Some\Fake\ClassPath',
            'indexName' => new DateTime,
        ]);
    }

    public function testThatRepositoryClassMustImplementElastcSearchRepository()
    {
        $expectedImplementation = ElasticSearchRepository::class;
        $this->expectException(CouldNotBuildMetaException::class);
        $this->expectExceptionMessage("Class 'DateTime' does not implement '$expectedImplementation'");

        Meta::fromArray([
            'model' => DateTime::class,
            'repository' => DateTime::class,
            'indexName' => 'meh',
        ]);
    }

    public function testThatSuccessfulConstruction()
    {
        $expectedImplementation = ElasticSearchRepository::class;
        $mockRepo = $this->createMock($expectedImplementation);

        $subj = Meta::fromArray([
            'model' => DateTime::class,
            'repository' => get_class($mockRepo),
            'indexName' => 'meh',
        ]);

        $this->assertInstanceOf(Meta::class, $subj);
    }

    public function testThatRequiredPropertiesAreRetrievable()
    {
        $expectedImplementation = ElasticSearchRepository::class;
        $mockRepo = $this->createMock($expectedImplementation);

        $subj = Meta::fromArray([
            'model' => DateTime::class,
            'repository' => get_class($mockRepo),
            'indexName' => 'meh',
        ]);

        $this->assertInstanceOf(Meta::class, $subj);
        $this->assertEquals(DateTime::class, $subj->getModelClass());
        $this->assertEquals(get_class($mockRepo), $subj->getRepositoryClass());
        $this->assertEquals('meh', $subj->getIndexName());
    }

    public function testThatSettingsPropertyIsRetrievable()
    {
        $expectedImplementation = ElasticSearchRepository::class;
        $mockRepo = $this->createMock($expectedImplementation);

        $settings = [
            'key' => 'value',
            'other' => 'meh',
        ];
        $subj = Meta::fromArray([
            'model' => DateTime::class,
            'repository' => get_class($mockRepo),
            'indexName' => 'meh',
            'settings' => $settings
        ]);

        $this->assertInstanceOf(Meta::class, $subj);
        $this->assertSame($settings, $subj->getSettings());
    }

    public function testThatAlaisesPropertyIsRetrievable()
    {
        $expectedImplementation = ElasticSearchRepository::class;
        $mockRepo = $this->createMock($expectedImplementation);

        $aliases = [
            'key' => 'value',
            'other' => 'meh',
        ];
        $subj = Meta::fromArray([
            'model' => DateTime::class,
            'repository' => get_class($mockRepo),
            'indexName' => 'meh',
            'aliases' => $aliases
        ]);

        $this->assertInstanceOf(Meta::class, $subj);
        $this->assertSame($aliases, $subj->getAliases());
    }

    public function testThatExtraPropertyIsRetrievable()
    {
        $expectedImplementation = ElasticSearchRepository::class;
        $mockRepo = $this->createMock($expectedImplementation);

        $extras = [
            'key' => 'value',
            'other' => 'meh',
        ];
        $subj = Meta::fromArray([
            'model' => DateTime::class,
            'repository' => get_class($mockRepo),
            'indexName' => 'meh',
            'extra' => $extras
        ]);

        $this->assertInstanceOf(Meta::class, $subj);
        $this->assertSame($extras, $subj->getExtras());
    }

    public function testThatIndividualPropertyIsRetrievableFromExtra()
    {
        $expectedImplementation = ElasticSearchRepository::class;
        $mockRepo = $this->createMock($expectedImplementation);

        $extras = [
            'key' => 'value',
            'other' => 'meh',
        ];
        $subj = Meta::fromArray([
            'model' => DateTime::class,
            'repository' => get_class($mockRepo),
            'indexName' => 'meh',
            'extra' => $extras
        ]);

        $this->assertInstanceOf(Meta::class, $subj);
        $this->assertSame($extras, $subj->getExtras());
        $this->assertSame($extras['key'], $subj->getExtra('key'));
        $this->assertSame($extras['other'], $subj->getExtra('other'));
    }

    public function testThatGettingExtraPropertyReturnsProvidedDefaultOrNull()
    {
        $expectedImplementation = ElasticSearchRepository::class;
        $mockRepo = $this->createMock($expectedImplementation);

        $extras = [
            'key' => 'value',
            'other' => 'meh',
        ];
        $subj = Meta::fromArray([
            'model' => DateTime::class,
            'repository' => get_class($mockRepo),
            'indexName' => 'meh',
            'extra' => $extras
        ]);

        $this->assertInstanceOf(Meta::class, $subj);
        $this->assertSame($extras, $subj->getExtras());
        $this->assertNull($subj->getExtra('blah'));
        $this->assertSame('blah', $subj->getExtra('blah', 'blah'));
    }

    public function testThatMappingPropertyIsRetrievable()
    {
        $expectedImplementation = ElasticSearchRepository::class;
        $mockRepo = $this->createMock($expectedImplementation);

        $mapping = [
            'key' => 'value',
            'other' => 'meh',
        ];
        $subj = Meta::fromArray([
            'model' => DateTime::class,
            'repository' => get_class($mockRepo),
            'indexName' => 'meh',
            'mappings' => $mapping
        ]);

        $this->assertInstanceOf(Meta::class, $subj);
        $this->assertSame($mapping, $subj->getMappings());
    }
}