<?php

namespace Tests\Unit\ElasticSearch\Repository\Meta;

use DateTime;
use RuntimeException;
use Tests\Cases\Unit;
use Contexts\ElasticSearch\Repository\Meta\Meta;
use Contexts\ElasticSearch\Repository\Meta\MetaRepository;
use Contexts\ElasticSearch\Exception\CouldNotBuildMetaException;
use Contexts\ElasticSearch\Exception\MultipleMetaEntriesException;

class MetaRepositoryTest extends Unit
{
    public function setUp(): void
    {
        MetaRepository::setMetaFactoryMethod([$this, 'mockMeta']);
    }

    public function mockMeta(array $config)
    {
        $mock = $this->createMock(Meta::class);

        if (! empty($config['shouldThrow'])) {
            $mock->expects($this->exactly(1))
                ->method('getModelClass')
                ->willThrowException(new RuntimeException("error constructing meta"));

            return $mock;
        }

        $mock->expects($this->exactly($config['modelClassCallCount'] ?? 0))
            ->method('getModelClass')
            ->willReturn($config['modelClassReturn'] ?? '\Some\Class');

        return $mock;
    }

    public function testConstructionWithMetaObjects()
    {
        $meta1 = $this->createMock(Meta::class);
        $meta1->expects($this->exactly(2))
            ->method('getModelClass')
            ->willReturn('\Some\Class');

        $meta2 = $this->createMock(Meta::class);
        $meta2->expects($this->exactly(2))
            ->method('getModelClass')
            ->willReturn('\Some\Other\Class');

        $subj = MetaRepository::fromArray([
            $meta1,
            $meta2,
        ]);

        $this->assertInstanceOf(MetaRepository::class, $subj);
    }

    public function testConstructionWithMetaObjectsWithSameModelClass()
    {
        $meta1 = $this->createMock(Meta::class);
        $meta1->expects($this->exactly(1))
            ->method('getModelClass')
            ->willReturn('\Some\Class');

        $meta2 = $this->createMock(Meta::class);
        $meta2->expects($this->exactly(1))
            ->method('getModelClass')
            ->willReturn('\Some\Class');

        $this->expectException(MultipleMetaEntriesException::class);
        MetaRepository::fromArray([
            $meta1,
            $meta2,
        ]);
    }

    public function testConstructionFromArrayConfigs()
    {
        $meta1Config = [
            'modelClassCallCount' => 2,
            'modelClassReturn' => '\Some\Class',
        ];

        $meta2Config = [
            'modelClassCallCount' => 2,
            'modelClassReturn' => '\Some\Other\Class',
        ];
        

        $subj = MetaRepository::fromArray([
            $meta1Config,
            $meta2Config,
        ]);

        $this->assertInstanceOf(MetaRepository::class, $subj);
    }

    public function testConstructionFromArrayConfigsWithSameModelClass()
    {
        $meta1Config = [
            'modelClassCallCount' => 1,
        ];

        $meta2Config = [
            'modelClassCallCount' => 1,
        ];
        

        $this->expectException(MultipleMetaEntriesException::class);
        MetaRepository::fromArray([
            $meta1Config,
            $meta2Config,
        ]);
    }

    public function testConstructionWithObjectArrayMix()
    {
        $meta1 = $this->createMock(Meta::class);
        $meta1->expects($this->exactly(2))
            ->method('getModelClass')
            ->willReturn('\Some\Other\Class');

        $meta2Config = [
            'modelClassCallCount' => 2,
        ];
        

        $subj = MetaRepository::fromArray([
            $meta1,
            $meta2Config,
        ]);

        $this->assertInstanceOf(MetaRepository::class, $subj);
    }

    public function testExceptionInMetaConstructionBubblesUp()
    {
        $meta1Config = [
            'shouldThrow' => true,
        ];

        $this->expectException(RuntimeException::class);
        MetaRepository::fromArray([
            $meta1Config,
        ]);
    }

    public function testDateTimeThrowsException()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        MetaRepository::fromArray([
            new DateTime,
        ]);
    }

    public function testStringThrowsException()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        MetaRepository::fromArray([
            'shouldnt work',
        ]);
    }

    public function testBoolThrowsException()
    {
        $this->expectException(CouldNotBuildMetaException::class);
        MetaRepository::fromArray([
            false,
        ]);
    }

    public function testMetaIsReturnedAsIndexedArray()
    {
        $meta1 = $this->createMock(Meta::class);
        $meta1->expects($this->exactly(2))
            ->method('getModelClass')
            ->willReturn('\Some\Class');

        $meta2 = $this->createMock(Meta::class);
        $meta2->expects($this->exactly(2))
            ->method('getModelClass')
            ->willReturn('\Some\Other\Class');

        $subj = MetaRepository::fromArray([
            $meta1,
            $meta2,
        ]);

        $expected = [
            $meta1,
            $meta2,
        ];

        $this->assertInstanceOf(MetaRepository::class, $subj);
        $this->assertSame($expected, $subj->all());
    }

    public function testMetaCanBeFoundByModelClass()
    {
        $meta1 = $this->createMock(Meta::class);
        $meta1->expects($this->exactly(2))
            ->method('getModelClass')
            ->willReturn('\Some\Class');

        $meta2 = $this->createMock(Meta::class);
        $meta2->expects($this->exactly(2))
            ->method('getModelClass')
            ->willReturn('\Some\Other\Class');

        $subj = MetaRepository::fromArray([
            $meta1,
            $meta2,
        ]);


        $this->assertInstanceOf(MetaRepository::class, $subj);
        $this->assertSame($meta1, $subj->byModel('\Some\Class'));
        $this->assertSame($meta2, $subj->byModel('\Some\Other\Class'));
    }

    public function testExceptionIsThrownWhenModelClassIsNotRegistered()
    {
        $meta1 = $this->createMock(Meta::class);
        $meta1->expects($this->exactly(2))
            ->method('getModelClass')
            ->willReturn('\Some\Class');

        $meta2 = $this->createMock(Meta::class);
        $meta2->expects($this->exactly(2))
            ->method('getModelClass')
            ->willReturn('\Some\Other\Class');

        $subj = MetaRepository::fromArray([
            $meta1,
            $meta2,
        ]);


        $this->assertInstanceOf(MetaRepository::class, $subj);

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Meta not found');
        $this->assertSame($meta1, $subj->byModel('DateTime'));
    }
}
