<?php

namespace Tests\Unit\ElasticSearch\Repository;

use DateTime;
use Exception;
use Tests\Cases\Unit;
use Elasticsearch\Client;
use Contexts\ElasticSearch\Repository\Manager;
use Contexts\ElasticSearch\Repository\Meta\Meta;
use Contexts\ElasticSearch\Repository\Meta\MetaRepository;
use Contexts\ElasticSearch\Repository\ElasticSearchRepository;
use Contexts\ElasticSearch\Exception\CouldNotCreateRepositoryException;
use Tests\Unit\ElasticSearch\Repository\DummyTestingClasses\DummyRepository;

class ManagerTest extends Unit
{
    public function testItReturnsARegisteredRepository()
    {
        $mockClient = $this->createMock(Client::class);

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getRepositoryClass')
            ->willReturn(DummyRepository::class);

        $mockMetaRepo = $this->createMock(MetaRepository::class);
        $mockMetaRepo->expects($this->exactly(1))
            ->method('byModel')
            ->with('somefakeclass')
            ->willReturn($mockMeta);

        $subj = new Manager($mockClient, $mockMetaRepo);
        $repo = $subj->get('somefakeclass');

        $this->assertInstanceOf(DummyRepository::class, $repo);
        $this->assertInstanceOf(ElasticSearchRepository::class, $repo);
    }

    public function testItThrowsExceptionWhenNotValidRepository()
    {
        $mockClient = $this->createMock(Client::class);

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getRepositoryClass')
            ->willReturn(DateTime::class); // doesnt implement the right interface

        $mockMetaRepo = $this->createMock(MetaRepository::class);
        $mockMetaRepo->expects($this->exactly(1))
            ->method('byModel')
            ->with('somefakeclass')
            ->willReturn($mockMeta);

        $subj = new Manager($mockClient, $mockMetaRepo);
        $this->expectException(CouldNotCreateRepositoryException::class);
        $subj->get('somefakeclass');
    }

    public function testExceptionsBubbleUpFromMetaRepo()
    {
        $mockClient = $this->createMock(Client::class);

        $mockMetaRepo = $this->createMock(MetaRepository::class);
        $mockMetaRepo->expects($this->exactly(1))
            ->method('byModel')
            ->with('somefakeclass')
            ->willThrowException(new Exception("no mapping."));

        $subj = new Manager($mockClient, $mockMetaRepo);
        $this->expectException(Exception::class);
        $repo = $subj->get('somefakeclass');
    }
}