<?php

namespace Tests\Unit\ElasticSearch\Repository;

use Tests\Cases\Unit;
use Elasticsearch\Client;
use Elasticsearch\Namespaces\IndicesNamespace;
use Contexts\ElasticSearch\Repository\Meta\Meta;
use Contexts\ElasticSearch\Repository\DocumentRepository;
use Contexts\ElasticSearch\Documents\IdentifiableDocument;
use Contexts\ElasticSearch\Exception\IndexNotFoundException;
use Contexts\ElasticSearch\Documents\DocumentableModelInterface;
use Tests\Unit\ElasticSearch\Repository\DummyTestingClasses\DummyHydrationModel;
use Tests\Unit\ElasticSearch\Repository\DummyTestingClasses\DocumentRepositoryWithAliases;
use Tests\Unit\ElasticSearch\Repository\DummyTestingClasses\DocumentRepositoryWithFullBody;
use Tests\Unit\ElasticSearch\Repository\DummyTestingClasses\DocumentRepositoryWithMappings;
use Tests\Unit\ElasticSearch\Repository\DummyTestingClasses\DocumentRepositoryWithSettings;

class DocumentRepositoryTest extends Unit
{
    const SETTINGS = [
        'setting_1' => [
            'do_this',
            'do_that'
        ],
    ];

    const MAPPINGS = [
        'mapping_1' => [
            'has_this',
            'has_that'
        ],
    ];

    const ALIASES = [
        'alias_1',
        'alias_2'
    ];

    public function testModelIsIndexedByClientOnSaveOne()
    {
        $indexName = 'someindex';
        $id = '314aaf84-968e-4e2e-b0dc-c300e3808da7';
        $modelBody = [
            'prop_1' => 'value',
        ];
        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(1))
            ->method('index')
            ->with([
                'index' => $indexName,
                'id' => $id,
                'body' => $modelBody,
                'refresh' => 'wait_for',
            ]);

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockDocument = $this->createMock(IdentifiableDocument::class);
        $mockDocument->expects($this->exactly(1))
            ->method('getId')
            ->willReturn($id);
        $mockDocument->expects($this->exactly(1))
            ->method('getBody')
            ->willReturn($modelBody);

        $mockModel = $this->createMock(DocumentableModelInterface::class);
        $mockModel->expects($this->exactly(1))
            ->method('toDocument')
            ->willReturn($mockDocument);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->saveOne($mockModel);
    }

    public function testModelIsDeletedByClientOnDeleteOne()
    {
        $indexName = 'someindex';
        $id = '314aaf84-968e-4e2e-b0dc-c300e3808da7';
        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(1))
            ->method('delete')
            ->with([
                'index' => $indexName,
                'id' => $id,
            ]);

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockDocument = $this->createMock(IdentifiableDocument::class);
        $mockDocument->expects($this->exactly(1))
            ->method('getId')
            ->willReturn($id);

        $mockModel = $this->createMock(DocumentableModelInterface::class);
        $mockModel->expects($this->exactly(1))
            ->method('toDocument')
            ->willReturn($mockDocument);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->deleteOne($mockModel);
    }

    public function testIndexIsNotCreatedIfItExists()
    {
        $indexName = 'someindex';

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(true);

        $mockIndices->expects($this->exactly(0))
            ->method('create');

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(1))
            ->method('indices')
            ->willReturn($mockIndices);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->createIndex();
    }

    public function testBlankIndexCreationDefaultIndexName()
    {
        $indexName = 'someindex';

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockMeta->expects($this->exactly(1))
            ->method('getSettings')
            ->willReturn(null);

        $mockMeta->expects($this->exactly(1))
            ->method('getAliases')
            ->willReturn(null);

        $mockMeta->expects($this->exactly(1))
            ->method('getMappings')
            ->willReturn(null);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(false);

        $mockIndices->expects($this->exactly(1))
            ->method('create')
            ->with(['index' => $indexName, 'body' => []]);

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(2))
            ->method('indices')
            ->willReturn($mockIndices);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->createIndex();
    }

    public function testBlankIndexCreationWithCustomIndexName()
    {
        $indexName = 'someindex';

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockMeta->expects($this->exactly(1))
            ->method('getSettings')
            ->willReturn(null);

        $mockMeta->expects($this->exactly(1))
            ->method('getAliases')
            ->willReturn(null);

        $mockMeta->expects($this->exactly(1))
            ->method('getMappings')
            ->willReturn(null);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(false);

        $mockIndices->expects($this->exactly(1))
            ->method('create')
            ->with(['index' => $indexName, 'body' => []]);

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(2))
            ->method('indices')
            ->willReturn($mockIndices);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->createIndex($indexName);
    }

    public function testIndexCreationWithSettings()
    {
        $indexName = 'someindex';
        $settings = self::SETTINGS;

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockMeta->expects($this->exactly(1))
            ->method('getSettings')
            ->willReturn($settings);

        $mockMeta->expects($this->exactly(1))
            ->method('getAliases')
            ->willReturn(null);

        $mockMeta->expects($this->exactly(1))
            ->method('getMappings')
            ->willReturn(null);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(false);

        $mockIndices->expects($this->exactly(1))
            ->method('create')
            ->with([
                'index' => $indexName,
                'body' => [
                    'settings' => $settings,
                ]
            ]);

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(2))
            ->method('indices')
            ->willReturn($mockIndices);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->createIndex();
    }

    public function testIndexCreationWithMappings()
    {
        $indexName = 'someindex';
        $mappings = self::MAPPINGS;

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockMeta->expects($this->exactly(1))
            ->method('getSettings')
            ->willReturn(null);

        $mockMeta->expects($this->exactly(1))
            ->method('getAliases')
            ->willReturn(null);

        $mockMeta->expects($this->exactly(1))
            ->method('getMappings')
            ->willReturn($mappings);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(false);

        $mockIndices->expects($this->exactly(1))
            ->method('create')
            ->with([
                'index' => $indexName,
                'body' => [
                    'mappings' => $mappings,
                ]
            ]);

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(2))
            ->method('indices')
            ->willReturn($mockIndices);


        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->createIndex();
    }

    public function testIndexCreationWithAliases()
    {
        $indexName = 'someindex';
        $aliases = self::ALIASES;

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockMeta->expects($this->exactly(1))
            ->method('getSettings')
            ->willReturn(null);

        $mockMeta->expects($this->exactly(1))
            ->method('getAliases')
            ->willReturn($aliases);

        $mockMeta->expects($this->exactly(1))
            ->method('getMappings')
            ->willReturn(null);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(false);

        $mockIndices->expects($this->exactly(1))
            ->method('create')
            ->with([
                'index' => $indexName,
                'body' => [
                    'aliases' => $aliases,
                ]
            ]);

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(2))
            ->method('indices')
            ->willReturn($mockIndices);


        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->createIndex();
    }

    public function testIndexCreationWithFullBody()
    {
        $indexName = 'someindex';
        $aliases = self::ALIASES;
        $settings = self::SETTINGS;
        $mappings = self::MAPPINGS;

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockMeta->expects($this->exactly(1))
            ->method('getSettings')
            ->willReturn($settings);

        $mockMeta->expects($this->exactly(1))
            ->method('getAliases')
            ->willReturn($aliases);

        $mockMeta->expects($this->exactly(1))
            ->method('getMappings')
            ->willReturn($mappings);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(false);

        $mockIndices->expects($this->exactly(1))
            ->method('create')
            ->with([
                'index' => $indexName,
                'body' => [
                    'settings' => $settings,
                    'mappings' => $mappings,
                    'aliases' => $aliases,
                ]
            ]);

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(2))
            ->method('indices')
            ->willReturn($mockIndices);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->createIndex();
    }

    public function testUpdateMappings()
    {
        $indexName = 'someindex';
        $mappings = self::MAPPINGS;

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockMeta->expects($this->exactly(1))
            ->method('getMappings')
            ->willReturn($mappings);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(true);

        $mockIndices->expects($this->exactly(1))
            ->method('putMapping')
            ->with([
                'index' => $indexName,
                'body' => $mappings,
            ]);

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(2))
            ->method('indices')
            ->willReturn($mockIndices);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->updateMappings();
    }

    public function testUpdateMappingsWhenIndexNotFound()
    {
        $indexName = 'someindex';

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(false);

        $mockIndices->expects($this->exactly(0))
            ->method('putMapping');

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(1))
            ->method('indices')
            ->willReturn($mockIndices);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $this->expectException(IndexNotFoundException::class);
        $subj->updateMappings();
    }

    public function testDeleteIndex()
    {
        $indexName = 'someindex';

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(true);

        $mockIndices->expects($this->exactly(1))
            ->method('delete')
            ->with(['index' => $indexName]);

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(2))
            ->method('indices')
            ->willReturn($mockIndices);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->deleteIndex();
    }

    public function testDeleteIndexThrowsExceptionIfItDoesNotExist()
    {
        $indexName = 'someindex';

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(1))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturn(false);

        $mockIndices->expects($this->exactly(0))
            ->method('delete');

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(1))
            ->method('indices')
            ->willReturn($mockIndices);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $this->expectException(IndexNotFoundException::class);
        $subj->deleteIndex();
    }

    public function testTruncateIndex()
    {
        $indexName = 'someindex';

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(2))
            ->method('getIndexName')
            ->willReturn($indexName);

        $mockIndices = $this->createMock(IndicesNamespace::class);
        $mockIndices->expects($this->exactly(2))
            ->method('exists')
            ->with(['index' => $indexName])
            ->willReturnOnConsecutiveCalls(true, false);

        $mockIndices->expects($this->exactly(1))
            ->method('delete')
            ->with(['index' => $indexName]);

        $mockIndices->expects($this->exactly(1))
            ->method('create')
            ->with(['index' => $indexName, 'body' => []]);

        $mockClient = $this->createMock(Client::class);
        $mockClient->expects($this->exactly(4))
            ->method('indices')
            ->willReturn($mockIndices);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $subj->truncate();
    }

    public function testItExtractsHits()
    {
        $actualHits =  [
            'hit_1',
            'hit_2'
        ];

        $mockMeta = $this->createMock(Meta::class);
        $mockClient = $this->createMock(Client::class);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $output = $this->callProtectedMethodOnSubject($subj, 'extractHits', [
            'hits' => [
                'hits' => $actualHits,
            ],
        ]);

        $this->assertSame($actualHits, $output);
    }

    public function testItExtractsHitsDefaultsToEmptyArray()
    {
        $actualHits =  [
            'hit_1',
            'hit_2'
        ];

        $mockMeta = $this->createMock(Meta::class);
        $mockClient = $this->createMock(Client::class);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $output = $this->callProtectedMethodOnSubject($subj, 'extractHits', ['blah' => 'meh']);

        $this->assertEmpty($output);
    }

    public function testResultHydration()
    {
        $response =  [
            'hits' => [
                'hits' => [
                    [
                        '_id' => 'id1',
                        '_source' => [
                            'field_1' => 'value_1',
                            'field_2' => 'value_2'
                        ],
                    ],
                    [
                        '_id' => 'id2',
                        '_source' => [
                            'field_1' => 'value_3',
                            'field_2' => 'value_4'
                        ],
                    ],
                ],
            ],
        ];

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(1))
            ->method('getModelClass')
            ->willReturn(DummyHydrationModel::class);

        $mockClient = $this->createMock(Client::class);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $output = $this->callProtectedMethodOnSubject(
            $subj,
            'hydrateResults',
            $response
        );

        $this->assertIsArray($output);

        $this->assertInstanceOf(DummyHydrationModel::class, $output[0]);
        $this->assertInstanceOf(DummyHydrationModel::class, $output[1]);

        $this->assertEquals($response['hits']['hits'][0]['_id'], $output[0]->id);
        $this->assertEquals($response['hits']['hits'][0]['_source']['field_1'], $output[0]->field1);
        $this->assertEquals($response['hits']['hits'][0]['_source']['field_2'], $output[0]->field2);

        $this->assertEquals($response['hits']['hits'][1]['_id'], $output[1]->id);
        $this->assertEquals($response['hits']['hits'][1]['_source']['field_1'], $output[1]->field1);
        $this->assertEquals($response['hits']['hits'][1]['_source']['field_2'], $output[1]->field2);
    }

    public function testResultHydrationReturnsEmptyArrayStraightAway()
    {
        $response =  [
            'hits' => [
                'hits' => [],
            ],
        ];

        $mockMeta = $this->createMock(Meta::class);
        $mockMeta->expects($this->exactly(0))
            ->method('getModelClass');

        $mockClient = $this->createMock(Client::class);

        $subj = $this->getMockForAbstractClass(
            DocumentRepository::class,
            [$mockClient, $mockMeta]
        );

        $output = $this->callProtectedMethodOnSubject(
            $subj,
            'hydrateResults',
            $response
        );

        $this->assertIsArray($output);
        $this->assertEmpty($output);
    }
}
