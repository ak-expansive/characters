<?php

namespace Tests\Unit\ApiSerialization;

use Tests\Cases\Unit;
use Contexts\ApiSerialization\Response;
use Contexts\ApiSerialization\ResponseBuilder;
use Contexts\ApiSerialization\Transformation\TransformersContainer;

class ResponseBuilderTest extends Unit
{
    public function testItSetsA404StatusCodeForNotFound()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        
        $subj = new ResponseBuilder($mockTransformersContainer);
        $response = $subj->notFound();

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(404, $response->statusCode());
    }

    public function testItSetsA202StatusCodeForAccepted()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        
        $subj = new ResponseBuilder($mockTransformersContainer);
        $response = $subj->accepted();

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(202, $response->statusCode());
    }

    public function testItSetsA204StatusCodeForNoContent()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        
        $subj = new ResponseBuilder($mockTransformersContainer);
        $response = $subj->noContent();

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(204, $response->statusCode());
    }

    public function testItSetsA400StatusCodeForBadRequest()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        
        $subj = new ResponseBuilder($mockTransformersContainer);
        $response = $subj->badRequest();

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(400, $response->statusCode());
    }

    public function testItSetsTheGivenVersion()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        
        $subj = new ResponseBuilder($mockTransformersContainer);
        $response = $subj->forVersion(1);

        $this->assertInstanceOf(Response::class, $response);

        // Feels a bit wrong
        // Shouldn't be testing internals of the class here
        // although this feels like the only way to test this
        $this->assertEquals(1, $this->getProtectedValueOfSubject($response, 'version'));
    }
}