<?php

namespace Tests\Unit\ApiSerialization\Transformation;

use Tests\Cases\Unit;
use Contexts\ApiSerialization\Transformation\TransformerConfig;

class TransformerConfigTest extends Unit
{
    public function testTheConfigAndClassAreSetCorrectlyOnConstruct()
    {
        $testClass = 'TestClass';
        $testConfig = ['some_key' => 'some_value'];

        $subj = new TransformerConfig($testClass, $testConfig);

        $this->assertEquals($testClass, $subj->getClass());
        $this->assertEquals($testConfig, $subj->getConfig());
    }
}