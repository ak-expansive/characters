<?php

namespace Tests\Unit\ApiSerialization\Transformation\DummyTransformers;

use Contexts\ApiSerialization\Transformation\TransformerInterface;

/**
 * This is only here to be mocked.
 * We need to test that the TransformersContainer rerturns the correct object when given a path.
 * If we only create mocks of the TransformerInterface they have the objects have the same class.
 * This means we can't test that the Container returns the expected transformer.
 */
class DummyTransformer1 implements TransformerInterface
{
    public function transform(array $data, array $config): array
    {
        return $data;
    }
}