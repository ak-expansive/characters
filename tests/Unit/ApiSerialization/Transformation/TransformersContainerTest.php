<?php

namespace Tests\Unit\ApiSerialization\Transformation;

use DateTime;
use Tests\Cases\Unit;
use Contexts\ApiSerialization\Transformation\TransformersContainer;
use Contexts\ApiSerialization\Exceptions\TransformerNotFoundException;
use Tests\Unit\ApiSerialization\Transformation\DummyTransformers\DummyTransformer1;
use Tests\Unit\ApiSerialization\Transformation\DummyTransformers\DummyTransformer2;
use Contexts\ApiSerialization\Exceptions\ConflictingTransformerRegistrationException;

class TransformersContainerTest extends Unit
{
    public function testTheGivenObjectsAreKeyedByValue()
    {
        $mockTransformer1 = $this->createMock(DummyTransformer1::class);
        $mockTransformer2 = $this->createMock(DummyTransformer2::class);

        $subj = new TransformersContainer($mockTransformer1, $mockTransformer2);

        $this->assertSame($mockTransformer1, $subj->get(get_class($mockTransformer1)));
        $this->assertSame($mockTransformer2, $subj->get(get_class($mockTransformer2)));
    }

    public function testDuplicateInstancesThrowsExceptionOnConstruct()
    {
        $mockTransformer1 = $this->createMock(DummyTransformer1::class);
        $mockTransformer2 = $this->createMock(DummyTransformer1::class);

        $this->expectException(ConflictingTransformerRegistrationException::class);
        new TransformersContainer($mockTransformer1, $mockTransformer2);
    }

    public function testAskingForUnknownClassThrowsException()
    {
        // TODO: fix this
        $mockTransformer1 = $this->createMock(DummyTransformer1::class);
        $mockTransformer2 = $this->createMock(DummyTransformer2::class);

        $subj = new TransformersContainer($mockTransformer1, $mockTransformer2);

        $this->expectException(TransformerNotFoundException::class);
        $subj->get(DateTime::class);
    }
}