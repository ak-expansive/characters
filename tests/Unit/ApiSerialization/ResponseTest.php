<?php

namespace Tests\Unit\ApiSerialization;

use TypeError;
use RuntimeException;
use Tests\Cases\Unit;
use InvalidArgumentException;
use Contexts\ApiSerialization\Response;
use Contexts\ApiSerialization\Transformation\TransformerConfig;
use Contexts\ApiSerialization\Contracts\ApiSerializableInterface;
use Contexts\ApiSerialization\Transformation\TransformerInterface;
use Contexts\ApiSerialization\Transformation\TransformersContainer;
use Contexts\ApiSerialization\Contracts\ApiSerializableWithTransformersInterface;

class ResponseTest extends Unit
{
    public function testConstructionWithVersion1()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = (new Response($mockTransformersContainer))->setVersion(1);
        $this->assertinstanceOf(Response::class, $subj);
    }

    public function testStatusCodeCanBeSetAndRestrieved()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = new Response($mockTransformersContainer);
        $subj->setStatusCode(200);
        $this->assertinstanceOf(Response::class, $subj);
        $this->assertEquals(200, $subj->statusCode());
    }

    public function testConstructionWithVersionStringThrowsTypeError()
    {
        $this->expectException(TypeError::class);
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = (new Response($mockTransformersContainer))->setVersion("SomeVersion");
        $this->assertinstanceOf(Response::class, $subj);
    }

    public function testThatEmptyResponsesSerializeToNull()
    {
        foreach (Response::EMPTY_RESPONSE_CODES as $emptyCode) {
            $mockTransformersContainer = $this->createMock(TransformersContainer::class);
            $subj = new Response($mockTransformersContainer);
            $subj->setStatusCode($emptyCode);
            $this->assertinstanceOf(Response::class, $subj);
            $this->assertEquals($emptyCode, $subj->statusCode());
            $this->assertNull($subj->serialize());
        }
    }

    public function testThatEmptyResponsesSerializeToNullWhenResultIsAdded()
    {
        foreach (Response::EMPTY_RESPONSE_CODES as $emptyCode) {
            $mockTransformersContainer = $this->createMock(TransformersContainer::class);
            $subj = new Response($mockTransformersContainer);
            $subj->setStatusCode($emptyCode);
            $subj->setSingularResult(
                $this->createMock(ApiSerializableInterface::class)
            );
            $this->assertinstanceOf(Response::class, $subj);
            $this->assertEquals($emptyCode, $subj->statusCode());
            $this->assertNull($subj->serialize());
        }
    }

    public function testThatEmptyResponsesSerializeToNullWhenResultsAreAdded()
    {
        foreach (Response::EMPTY_RESPONSE_CODES as $emptyCode) {
            $mockTransformersContainer = $this->createMock(TransformersContainer::class);
            $subj = new Response($mockTransformersContainer);
            $subj->setStatusCode($emptyCode);
            $subj->setResults(
                $this->createMock(ApiSerializableInterface::class),
                $this->createMock(ApiSerializableInterface::class)
            );
            $this->assertinstanceOf(Response::class, $subj);
            $this->assertEquals($emptyCode, $subj->statusCode());
            $this->assertNull($subj->serialize());
        }
    }

    public function testThatErrorResponsesAreSerializedCorrectly()
    {
        foreach (Response::AVAILABLE_ERROR_CODES as $errorCode) {
            $mockTransformersContainer = $this->createMock(TransformersContainer::class);
            $subj = new Response($mockTransformersContainer);
            $subj->setErrorCode($errorCode);
            $this->assertinstanceOf(Response::class, $subj);
            $this->assertSame([
                'error_code' => $errorCode,
            ], $subj->serialize());
        }
    }

    public function testThatOnlyAvailableErrorCodesCanBeUsed()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = new Response($mockTransformersContainer);
        $this->expectException(InvalidArgumentException::class);
        $subj->setErrorCode('some_garbage_error');
    }

    public function testThatAnErrorCodeOverridesStatusCode()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = new Response($mockTransformersContainer);
        $subj->setStatusCode(200);
        $subj->setErrorCode(Response::ERROR_PAGE_OUT_OF_BOUNDS);

        $this->assertinstanceOf(Response::class, $subj);
        $this->assertEquals(400, $subj->statusCode());
    }

    public function testThatAnExceptionIsThrownWhenSettingSuccessStatusCodeAndErrorCode()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = new Response($mockTransformersContainer);
        $subj->setErrorCode(Response::ERROR_PAGE_OUT_OF_BOUNDS);
        $this->expectException(InvalidArgumentException::class);
        $subj->setStatusCode(200);
    }

    public function testThatAnErrorCodeOverridesDefaultResponseSerialization()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = (new Response($mockTransformersContainer))->setStatusCode(200);
        $subj->setResults(
            $this->createMock(ApiSerializableInterface::class),
            $this->createMock(ApiSerializableInterface::class),
            $this->createMock(ApiSerializableInterface::class)
        );
        $subj->setErrorCode(Response::ERROR_PAGE_OUT_OF_BOUNDS);
        $this->assertinstanceOf(Response::class, $subj);
        $this->assertEquals(400, $subj->statusCode());
        $this->assertSame([
            'error_code' => Response::ERROR_PAGE_OUT_OF_BOUNDS,
        ], $subj->serialize());
    }

    public function testSingularResultResponseIsSerializedCorrectly()
    {
        $version = 1;
        $serializedResult = [
            'some_key' => 'some_val',
        ];
        $mockResult = $this->createMock(ApiSerializableInterface::class);
        $mockResult->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult);

        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = (new Response($mockTransformersContainer))->setVersion(1)
            ->setSingularResult($mockResult);

        $expected = [
            '_meta' => [],
            'data' => $serializedResult
        ];

        $this->assertinstanceOf(Response::class, $subj);
        $this->assertSame($expected, $subj->serialize());
    }

    public function testMultiResultResponseIsSerializedCorrectly()
    {
        $version = 1;
        $serializedResult1 = [
            'some_key' => 'some_val',
        ];
        $serializedResult2 = [
            'other_key' => 'other_val',
        ];
        $mockResult1 = $this->createMock(ApiSerializableInterface::class);
        $mockResult1->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult1);

        $mockResult2 = $this->createMock(ApiSerializableInterface::class);
        $mockResult2->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult2);

        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = (new Response($mockTransformersContainer))->setVersion(1)
            ->setResults($mockResult1, $mockResult2);

        $expected = [
            '_meta' => [],
            'data' => [
                $serializedResult1,
                $serializedResult2,
            ]
        ];

        $this->assertinstanceOf(Response::class, $subj);
        $this->assertSame($expected, $subj->serialize());
    }

    public function testMultiResultResponseWithPageIsSerializedCorrectly()
    {
        $version = 1;
        $serializedResult1 = [
            'some_key' => 'some_val',
        ];
        $serializedResult2 = [
            'other_key' => 'other_val',
        ];
        $mockResult1 = $this->createMock(ApiSerializableInterface::class);
        $mockResult1->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult1);

        $mockResult2 = $this->createMock(ApiSerializableInterface::class);
        $mockResult2->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult2);

        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = (new Response($mockTransformersContainer))->setVersion(1)
            ->setPage(10)
            ->setResults($mockResult1, $mockResult2);

        $expected = [
            '_meta' => [
                'pagination' => [
                    'page' => 10
                ]
            ],
            'data' => [
                $serializedResult1,
                $serializedResult2,
            ]
        ];

        $this->assertinstanceOf(Response::class, $subj);
        $this->assertSame($expected, $subj->serialize());
    }

    public function testMultiResultResponseWithPaginationIsSerializedCorrectly()
    {
        $version = 1;
        $serializedResult1 = [
            'some_key' => 'some_val',
        ];
        $serializedResult2 = [
            'other_key' => 'other_val',
        ];
        $mockResult1 = $this->createMock(ApiSerializableInterface::class);
        $mockResult1->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult1);

        $mockResult2 = $this->createMock(ApiSerializableInterface::class);
        $mockResult2->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult2);

        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = (new Response($mockTransformersContainer))->setVersion(1)
            ->setPage(10)
            ->setPageSize(4)
            ->setPageTotal(4)
            ->setTotalPages(12)
            ->setTotalResults(46)
            ->setResults($mockResult1, $mockResult2);

        $expected = [
            '_meta' => [
                'pagination' => [
                    'page' => 10,
                    'page_size' => 4,
                    'page_total' => 4,
                    'total_pages' => 12,
                    'total' => 46,
                ]
            ],
            'data' => [
                $serializedResult1,
                $serializedResult2,
            ]
        ];

        $this->assertinstanceOf(Response::class, $subj);
        $this->assertSame($expected, $subj->serialize());
    }

    public function testThatAnExceptionIsThrownWhenNoResultsAreSet()
    {
        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = (new Response($mockTransformersContainer))->setVersion(1)
            ->setStatusCode(200);
        $this->assertInstanceOf(Response::class, $subj);
        $this->expectException(RuntimeException::class);

        $subj->serialize();
    }

    public function testThatSingularResultIsRemovedWhenCollectionIsSet()
    {
        $version = 1;
        $serializedResult1 = [
            'some_key' => 'some_val',
        ];
        $serializedResult2 = [
            'other_key' => 'other_val',
        ];
        $mockSingleResult = $this->createMock(ApiSerializableInterface::class);
        $mockSingleResult->expects($this->exactly(0))
            ->method('forApi');

        $mockResult1 = $this->createMock(ApiSerializableInterface::class);
        $mockResult1->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult1);

        $mockResult2 = $this->createMock(ApiSerializableInterface::class);
        $mockResult2->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult2);

        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = (new Response($mockTransformersContainer))->setVersion(1)
            ->setStatusCode(200)
            ->setSingularResult($mockSingleResult)
            ->setResults($mockResult1, $mockResult2);

        $this->assertInstanceOf(Response::class, $subj);

        $expected = [
            '_meta' => [],
            'data' => [
                $serializedResult1,
                $serializedResult2,
            ]
        ];

        $this->assertSame($expected, $subj->serialize());
    }

    public function testThatCollectionResultIsRemovedWhenSingularIsSet()
    {
        $version = 1;
        $singleResult = [
            'some_key' => 'some_val',
        ];
        $mockSingleResult = $this->createMock(ApiSerializableInterface::class);
        $mockSingleResult->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($singleResult);

        $mockResult1 = $this->createMock(ApiSerializableInterface::class);
        $mockResult1->expects($this->exactly(0))
            ->method('forApi');

        $mockResult2 = $this->createMock(ApiSerializableInterface::class);
        $mockResult2->expects($this->exactly(0))
            ->method('forApi');

        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $subj = (new Response($mockTransformersContainer))->setVersion(1)
            ->setStatusCode(200)
            ->setResults($mockResult1, $mockResult2)
            ->setSingularResult($mockSingleResult);

        $this->assertInstanceOf(Response::class, $subj);

        $expected = [
            '_meta' => [],
            'data' => $singleResult
        ];

        $this->assertSame($expected, $subj->serialize());
    }

    public function testSingularResultResponseIsTransformed()
    {
        $version = 1;
        $serializedResult = [
            'some_key' => 'some_val',
        ];
        $transformedResult = [
            'some_key' => 'transformed_val'
        ];

        $mockTransformerConfigArray = [
            'config' => [
                'something'
            ]
        ];

        $mockTransformerClass = 'TestTransformer';
        $mockTransformer = $this->createMock(TransformerInterface::class);
        $mockTransformer->expects($this->exactly(1))
            ->method('transform')
            ->with($serializedResult, $mockTransformerConfigArray)
            ->willReturn($transformedResult);

        $mockTransformerConfig = $this->createMock(TransformerConfig::class);
        $mockTransformerConfig->expects($this->exactly(1))
            ->method('getClass')
            ->willReturn($mockTransformerClass);

        $mockTransformerConfig->expects($this->exactly(1))
            ->method('getConfig')
            ->willReturn($mockTransformerConfigArray);

        $mockResult = $this->createMock(ApiSerializableWithTransformersInterface::class);
        $mockResult->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult);

        $mockResult->expects($this->exactly(1))
            ->method('getTransformerConfigs')
            ->willReturn([$mockTransformerConfig]);

        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $mockTransformersContainer->expects($this->exactly(1))
            ->method('get')
            ->with($mockTransformerClass)
            ->willReturn($mockTransformer);

        $subj = (new Response($mockTransformersContainer))->setVersion(1)
            ->setSingularResult($mockResult);

        $expected = [
            '_meta' => [],
            'data' => $transformedResult
        ];

        $this->assertinstanceOf(Response::class, $subj);
        $this->assertSame($expected, $subj->serialize());
    }

    public function testMultiResultResponseIsTransformed()
    {
        $version = 1;
        $serializedResult1 = [
            'some_key' => 'some_val_1',
        ];
        $transformedResult1 = [
            'some_key' => 'transformed_val_1'
        ];

        $serializedResult2 = [
            'some_key' => 'some_val_2',
        ];
        $transformedResult2 = [
            'some_key' => 'transformed_val_2'
        ];

        $mockTransformerConfigArray = [
            'config' => [
                'something'
            ]
        ];

        $mockTransformerClass = 'TestTransformer';
        $mockTransformer = $this->createMock(TransformerInterface::class);
        $mockTransformer->expects($this->exactly(2))
            ->method('transform')
            ->withConsecutive(
                [$serializedResult1, $mockTransformerConfigArray],
                [$serializedResult2, $mockTransformerConfigArray]
            )->willReturnOnConsecutiveCalls(
                $transformedResult1,
                $transformedResult2
            );

        $mockTransformerConfig = $this->createMock(TransformerConfig::class);
        $mockTransformerConfig->expects($this->exactly(2))
            ->method('getClass')
            ->willReturn($mockTransformerClass);

        $mockTransformerConfig->expects($this->exactly(2))
            ->method('getConfig')
            ->willReturn($mockTransformerConfigArray);

        $mockResult1 = $this->createMock(ApiSerializableWithTransformersInterface::class);
        $mockResult1->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult1);

        $mockResult1->expects($this->exactly(1))
            ->method('getTransformerConfigs')
            ->willReturn([$mockTransformerConfig]);

        $mockResult2 = $this->createMock(ApiSerializableWithTransformersInterface::class);
        $mockResult2->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult2);

        $mockResult2->expects($this->exactly(1))
            ->method('getTransformerConfigs')
            ->willReturn([$mockTransformerConfig]);

        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $mockTransformersContainer->expects($this->exactly(2))
            ->method('get')
            ->with($mockTransformerClass)
            ->willReturn($mockTransformer);

        $subj = (new Response($mockTransformersContainer))->setVersion(1)
            ->setResults($mockResult1, $mockResult2);

        $expected = [
            '_meta' => [],
            'data' => [
                $transformedResult1,
                $transformedResult2
            ],
        ];

        $this->assertinstanceOf(Response::class, $subj);
        $this->assertSame($expected, $subj->serialize());
    }

    public function testSingularResultResponseIsTransformedByAllTransformers()
    {
        $version = 1;
        $serializedResult = [
            'some_key' => 'some_val',
        ];

        $transformedResult1 = [
            'some_key' => 'transformed_val'
        ];

        $transformedResult2 = [
            'some_key' => 'transformed_again'
        ];

        $mockTransformerConfigArray1 = [
            'config' => [
                'something'
            ]
        ];

        $mockTransformerConfigArray2 = [
            'config' => [
                'other_thing'
            ]
        ];

        $mockTransformerClass1 = 'TestTransformer';
        $mockTransformerClass2 = 'OtherTestTransformer';

        $mockTransformer1 = $this->createMock(TransformerInterface::class);
        $mockTransformer1->expects($this->exactly(1))
            ->method('transform')
            ->with($serializedResult, $mockTransformerConfigArray1)
            ->willReturn($transformedResult1);

        $mockTransformer2 = $this->createMock(TransformerInterface::class);
        $mockTransformer2->expects($this->exactly(1))
            ->method('transform')
            ->with($transformedResult1, $mockTransformerConfigArray2)
            ->willReturn($transformedResult2);

        $mockTransformerConfig1 = $this->createMock(TransformerConfig::class);
        $mockTransformerConfig1->expects($this->exactly(1))
            ->method('getClass')
            ->willReturn($mockTransformerClass1);

        $mockTransformerConfig1->expects($this->exactly(1))
            ->method('getConfig')
            ->willReturn($mockTransformerConfigArray1);

        $mockTransformerConfig2 = $this->createMock(TransformerConfig::class);
        $mockTransformerConfig2->expects($this->exactly(1))
            ->method('getClass')
            ->willReturn($mockTransformerClass2);

        $mockTransformerConfig2->expects($this->exactly(1))
            ->method('getConfig')
            ->willReturn($mockTransformerConfigArray2);

        $mockResult = $this->createMock(ApiSerializableWithTransformersInterface::class);
        $mockResult->expects($this->exactly(1))
            ->method('forApi')
            ->with($version)
            ->willReturn($serializedResult);

        $mockResult->expects($this->exactly(1))
            ->method('getTransformerConfigs')
            ->willReturn([$mockTransformerConfig1, $mockTransformerConfig2]);

        $mockTransformersContainer = $this->createMock(TransformersContainer::class);
        $mockTransformersContainer->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(
                [$mockTransformerClass1],
                [$mockTransformerClass2]
            )->willReturnOnConsecutiveCalls(
                $mockTransformer1,
                $mockTransformer2
            );

        $subj = (new Response($mockTransformersContainer))->setVersion(1)
            ->setSingularResult($mockResult);

        $expected = [
            '_meta' => [],
            'data' => $transformedResult2
        ];

        $this->assertinstanceOf(Response::class, $subj);
        $this->assertSame($expected, $subj->serialize());
    }
}
